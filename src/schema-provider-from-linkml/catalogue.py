# Auto generated from catalogue.yaml by pythongen.py version: 0.9.0
# Generation date: 2023-04-06T16:24:50
# Schema: catalogue
#
# id: https://w3id.org/gaia-x/Catalogue
# description:
# license: https://creativecommons.org/publicdomain/zero/1.0/

import dataclasses
import sys
import re
from jsonasobj2 import JsonObj, as_dict
from typing import Optional, List, Union, Dict, ClassVar, Any
from dataclasses import dataclass
from linkml_runtime.linkml_model.meta import EnumDefinition, PermissibleValue, PvFormulaOptions

from linkml_runtime.utils.slot import Slot
from linkml_runtime.utils.metamodelcore import empty_list, empty_dict, bnode
from linkml_runtime.utils.yamlutils import YAMLRoot, extended_str, extended_float, extended_int
from linkml_runtime.utils.dataclass_extensions_376 import dataclasses_init_fn_with_kwargs
from linkml_runtime.utils.formatutils import camelcase, underscore, sfx
from linkml_runtime.utils.enumerations import EnumDefinitionImpl
from rdflib import Namespace, URIRef
from linkml_runtime.utils.curienamespace import CurieNamespace
from linkml_runtime.linkml_model.types import String

metamodel_version = "1.7.0"
version = "2210"

# Overwrite dataclasses _init_fn to add **kwargs in __init__
dataclasses._init_fn = dataclasses_init_fn_with_kwargs

# Namespaces
GX = CurieNamespace('gx', 'https://w3id.org/gaia-x/')
LINKML = CurieNamespace('linkml', 'https://w3id.org/linkml/')
SCHEMA = CurieNamespace('schema', 'http://schema.org/')
DEFAULT_ = CurieNamespace('', 'https://w3id.org/gaia-x/Catalogue/')


# Types

# Class references



@dataclass
class Catalogue(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = GX.Catalogue
    class_class_curie: ClassVar[str] = "gx:Catalogue"
    class_name: ClassVar[str] = "Catalogue"
    class_model_uri: ClassVar[URIRef] = URIRef("https://w3id.org/gaia-x/Catalogue/Catalogue")

    providedBy: str = None
    getVerifiableCredentialsIDs: Optional[Union[str, List[str]]] = empty_list()

    def __post_init__(self, *_: List[str], **kwargs: Dict[str, Any]):
        if self._is_empty(self.providedBy):
            self.MissingRequiredField("providedBy")
        if not isinstance(self.providedBy, str):
            self.providedBy = str(self.providedBy)

        if not isinstance(self.getVerifiableCredentialsIDs, list):
            self.getVerifiableCredentialsIDs = [self.getVerifiableCredentialsIDs] if self.getVerifiableCredentialsIDs is not None else []
        self.getVerifiableCredentialsIDs = [v if isinstance(v, str) else str(v) for v in self.getVerifiableCredentialsIDs]

        super().__post_init__(**kwargs)


# Enumerations


# Slots
class slots:
    pass

slots.getVerifiableCredentialsIDs = Slot(uri=GX.getVerifiableCredentialsIDs, name="getVerifiableCredentialsIDs", curie=GX.curie('getVerifiableCredentialsIDs'),
                   model_uri=DEFAULT_.getVerifiableCredentialsIDs, domain=None, range=Optional[Union[str, List[str]]])

slots.providedBy = Slot(uri=GX.providedBy, name="providedBy", curie=GX.curie('providedBy'),
                   model_uri=DEFAULT_.providedBy, domain=None, range=Optional[str])

slots.Catalogue_providedBy = Slot(uri=GX.providedBy, name="Catalogue_providedBy", curie=GX.curie('providedBy'),
                   model_uri=DEFAULT_.Catalogue_providedBy, domain=Catalogue, range=str)
