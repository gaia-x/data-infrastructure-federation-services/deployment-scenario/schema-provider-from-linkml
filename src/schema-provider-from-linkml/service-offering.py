# Auto generated from service-offering.yaml by pythongen.py version: 0.9.0
# Generation date: 2023-04-04T13:57:58
# Schema: service-offering-info
#
# id: https://w3id.org/linkml/examples/personinfo
# description:
# license: https://creativecommons.org/publicdomain/zero/1.0/

import dataclasses
import sys
import re
from jsonasobj2 import JsonObj, as_dict
from typing import Optional, List, Union, Dict, ClassVar, Any
from dataclasses import dataclass
from linkml_runtime.linkml_model.meta import EnumDefinition, PermissibleValue, PvFormulaOptions

from linkml_runtime.utils.slot import Slot
from linkml_runtime.utils.metamodelcore import empty_list, empty_dict, bnode
from linkml_runtime.utils.yamlutils import YAMLRoot, extended_str, extended_float, extended_int
from linkml_runtime.utils.dataclass_extensions_376 import dataclasses_init_fn_with_kwargs
from linkml_runtime.utils.formatutils import camelcase, underscore, sfx
from linkml_runtime.utils.enumerations import EnumDefinitionImpl
from rdflib import Namespace, URIRef
from linkml_runtime.utils.curienamespace import CurieNamespace
from linkml_runtime.linkml_model.types import String

metamodel_version = "1.7.0"
version = None

# Overwrite dataclasses _init_fn to add **kwargs in __init__
dataclasses._init_fn = dataclasses_init_fn_with_kwargs

# Namespaces
ORCID = CurieNamespace('ORCID', 'https://orcid.org/')
DCAT = CurieNamespace('dcat', 'https://www.w3.org/ns/dcat#')
DCT = CurieNamespace('dct', 'https://purl.org/dc/terms/')
GX_TRUST_FRAMEWORK_EXTENDED = CurieNamespace('gx_trust_framework_extended', 'https://w3id.org/linkml/examples/personinfo/')
LINKML = CurieNamespace('linkml', 'https://w3id.org/linkml/')
DEFAULT_ = GX_TRUST_FRAMEWORK_EXTENDED


# Types

# Class references
class ServiceOfferingId(extended_str):
    pass


@dataclass
class ServiceOffering(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = GX_TRUST_FRAMEWORK_EXTENDED.ServiceOffering
    class_class_curie: ClassVar[str] = "gx_trust_framework_extended:ServiceOffering"
    class_name: ClassVar[str] = "ServiceOffering"
    class_model_uri: ClassVar[URIRef] = GX_TRUST_FRAMEWORK_EXTENDED.ServiceOffering

    id: Union[str, ServiceOfferingId] = None
    description: Optional[str] = None
    serviceType: Optional[str] = None
    layer: Optional[str] = None
    providerServiceId: Optional[str] = None
    keyword: Optional[str] = None
    isAvailableOn: Optional[str] = None

    def __post_init__(self, *_: List[str], **kwargs: Dict[str, Any]):
        if self._is_empty(self.id):
            self.MissingRequiredField("id")
        if not isinstance(self.id, ServiceOfferingId):
            self.id = ServiceOfferingId(self.id)

        if self.description is not None and not isinstance(self.description, str):
            self.description = str(self.description)

        if self.serviceType is not None and not isinstance(self.serviceType, str):
            self.serviceType = str(self.serviceType)

        if self.layer is not None and not isinstance(self.layer, str):
            self.layer = str(self.layer)

        if self.providerServiceId is not None and not isinstance(self.providerServiceId, str):
            self.providerServiceId = str(self.providerServiceId)

        if self.keyword is not None and not isinstance(self.keyword, str):
            self.keyword = str(self.keyword)

        if self.isAvailableOn is not None and not isinstance(self.isAvailableOn, str):
            self.isAvailableOn = str(self.isAvailableOn)

        super().__post_init__(**kwargs)


# Enumerations


# Slots
class slots:
    pass

slots.serviceOffering__id = Slot(uri=GX_TRUST_FRAMEWORK_EXTENDED.id, name="serviceOffering__id", curie=GX_TRUST_FRAMEWORK_EXTENDED.curie('id'),
                   model_uri=GX_TRUST_FRAMEWORK_EXTENDED.serviceOffering__id, domain=None, range=URIRef)

slots.serviceOffering__description = Slot(uri=DCT.description, name="serviceOffering__description", curie=DCT.curie('description'),
                   model_uri=GX_TRUST_FRAMEWORK_EXTENDED.serviceOffering__description, domain=None, range=Optional[str])

slots.serviceOffering__serviceType = Slot(uri=GX_TRUST_FRAMEWORK_EXTENDED.serviceType, name="serviceOffering__serviceType", curie=GX_TRUST_FRAMEWORK_EXTENDED.curie('serviceType'),
                   model_uri=GX_TRUST_FRAMEWORK_EXTENDED.serviceOffering__serviceType, domain=None, range=Optional[str])

slots.serviceOffering__layer = Slot(uri=GX_TRUST_FRAMEWORK_EXTENDED.layer, name="serviceOffering__layer", curie=GX_TRUST_FRAMEWORK_EXTENDED.curie('layer'),
                   model_uri=GX_TRUST_FRAMEWORK_EXTENDED.serviceOffering__layer, domain=None, range=Optional[str])

slots.serviceOffering__providerServiceId = Slot(uri=GX_TRUST_FRAMEWORK_EXTENDED.providerServiceId, name="serviceOffering__providerServiceId", curie=GX_TRUST_FRAMEWORK_EXTENDED.curie('providerServiceId'),
                   model_uri=GX_TRUST_FRAMEWORK_EXTENDED.serviceOffering__providerServiceId, domain=None, range=Optional[str])

slots.serviceOffering__keyword = Slot(uri=DCAT.keyword, name="serviceOffering__keyword", curie=DCAT.curie('keyword'),
                   model_uri=GX_TRUST_FRAMEWORK_EXTENDED.serviceOffering__keyword, domain=None, range=Optional[str])

slots.serviceOffering__isAvailableOn = Slot(uri=GX_TRUST_FRAMEWORK_EXTENDED.Location, name="serviceOffering__isAvailableOn", curie=GX_TRUST_FRAMEWORK_EXTENDED.curie('Location'),
                   model_uri=GX_TRUST_FRAMEWORK_EXTENDED.serviceOffering__isAvailableOn, domain=None, range=Optional[str])
