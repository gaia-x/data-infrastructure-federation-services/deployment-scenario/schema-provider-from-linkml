from linkml_runtime.dumpers import json_dumper, rdflib_dumper
from participant import LegalParticipant
from address import Address
from catalogue import Catalogue
from rdflib import Graph
from rdflib.namespace import FOAF, RDF

print("Hello world")

legal_address = Address("France", locality="Paris")
headquarter_address = Address("France", locality="Paris")
legal_participant = LegalParticipant("registrationNumber", legalAddress=legal_address, headquarterAddress=headquarter_address)
catalogue = Catalogue(providedBy="did:web:example123", getVerifiableCredentialsIDs=["123"])

context = {
    "@context": {
        "gx": "https://w3id.org/gaia-x/",
        "participant": "https://w3id.org/gaia-x/Participant/",
        "schema": "http://schema.org/",
        "@vocab": "https://w3id.org/gaia-x/Participant/",
        "country_name": {
            "@id": "schema:name"
        },
        "countryCode": {
            "@id": "schema:addressCountry"
        },
        "gps": {
            "@id": "gx:gps"
        },
        "headquarterAddress": {
            "@type": "@id",
            "@id": "gx:headquarterAddress"
        },
        "legalAddress": {
            "@type": "@id",
            "@id": "gx:legalAddress"
        },
        "locality": {
            "@id": "schema:addressLocality"
        },
        "parentOrganization": {
            "@id": "schema:parentOrganization"
        },
        "postal_code": {
            "@id": "schema:postalCode"
        },
        "registrationNumber": {
            "@id": "gx:registrationNumber"
        },
        "street_address": {
            "@id": "schema:streetAddress"
        },
        "subOrganization": {
            "@id": "schema:subOrganization"
        },
        "Address": {
            "@id": "gx:Address"
        },
        "LegalParticipant": {
            "@id": "gx:LegalParticipant"
        },
        "Participant": {
            "@id": "gx:Participant"
        }
    }
}

context_catalog = {
    "@context": {
        "catalogue": "https://w3id.org/gaia-x/Catalogue/",
        "gx": "https://w3id.org/gaia-x/",
        "linkml": "https://w3id.org/linkml/",
        "schema": "http://schema.org/",
        "@vocab": "https://w3id.org/gaia-x/Catalogue/",
        "getVerifiableCredentialsIDs": {
            "@id": "gx:getVerifiableCredentialsIDs"
        },
        "providedBy": {
            "@id": "gx:providedBy"
        },
        "Catalogue": {
            "@id": "gx:Catalogue"
        }
    }
}








# for k,v in s1.__repr__(self):
#    print(k, v)
#print(json_dumper.dumps(legal_participant, contexts=context))

# pyshacl -s output/shacl/participant.shacl.ttl -df json-ld -f human code/data.jsonld
# WARNING generator ne met pas de @type aux sous-objets... indiqué dans la doc d'ailleurs
with open("data.jsonld", "w") as fichier:
    fichier.write(json_dumper.dumps(legal_participant, contexts=context))

# pyshacl -s output/shacl/address.shacl.ttl -df json-ld -f human code/data-address.jsonld
with open("data-address.jsonld", "w") as fichier:
    fichier.write(json_dumper.dumps(legal_address, contexts=context))

with open("data-catalog.json", "w") as fichier:
    fichier.write(json_dumper.dumps(catalogue, contexts=context_catalog))

# with open("data.ttl", "w") as ttl_file:
#     ttl_file.write(rdflib_dumper.dumps(legal_participant))

# Generate a json-ld object ??


# g = Graph()
#
# g.parse(json_dumper.dumps(legal_participant).__str__())
# g.bind("foaf", FOAF)
# g.bind("rdf", RDF)
#
# print(g.serialize(format="json-ld"))
