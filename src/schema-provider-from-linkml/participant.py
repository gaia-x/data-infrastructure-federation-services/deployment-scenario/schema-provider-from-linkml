# Auto generated from participant.yaml by pythongen.py version: 0.9.0
# Generation date: 2023-04-04T13:57:57
# Schema: participant
#
# id: https://w3id.org/gaia-x/Participant
# description:
# license: https://creativecommons.org/publicdomain/zero/1.0/

import dataclasses
import sys
import re
from jsonasobj2 import JsonObj, as_dict
from typing import Optional, List, Union, Dict, ClassVar, Any
from dataclasses import dataclass
from linkml_runtime.linkml_model.meta import EnumDefinition, PermissibleValue, PvFormulaOptions

from linkml_runtime.utils.slot import Slot
from linkml_runtime.utils.metamodelcore import empty_list, empty_dict, bnode
from linkml_runtime.utils.yamlutils import YAMLRoot, extended_str, extended_float, extended_int
from linkml_runtime.utils.dataclass_extensions_376 import dataclasses_init_fn_with_kwargs
from linkml_runtime.utils.formatutils import camelcase, underscore, sfx
from linkml_runtime.utils.enumerations import EnumDefinitionImpl
from rdflib import Namespace, URIRef
from linkml_runtime.utils.curienamespace import CurieNamespace
from linkml_runtime.linkml_model.types import String

metamodel_version = "1.7.0"
version = "2210"

# Overwrite dataclasses _init_fn to add **kwargs in __init__
dataclasses._init_fn = dataclasses_init_fn_with_kwargs

# Namespaces
GX = CurieNamespace('gx', 'https://w3id.org/gaia-x/')
SCHEMA = CurieNamespace('schema', 'http://schema.org/')
DEFAULT_ = CurieNamespace('', 'https://w3id.org/gaia-x/Participant/')


# Types

# Class references



class Participant(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = GX.Participant
    class_class_curie: ClassVar[str] = "gx:Participant"
    class_name: ClassVar[str] = "Participant"
    class_model_uri: ClassVar[URIRef] = URIRef("https://w3id.org/gaia-x/Participant/Participant")


@dataclass
class LegalParticipant(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = GX.LegalParticipant
    class_class_curie: ClassVar[str] = "gx:LegalParticipant"
    class_name: ClassVar[str] = "LegalParticipant"
    class_model_uri: ClassVar[URIRef] = URIRef("https://w3id.org/gaia-x/Participant/LegalParticipant")

    registrationNumber: str = None
    legalAddress: Union[dict, "Address"] = None
    headquarterAddress: Union[dict, "Address"] = None
    parentOrganization: Optional[Union[str, List[str]]] = empty_list()
    subOrganization: Optional[Union[str, List[str]]] = empty_list()

    def __post_init__(self, *_: List[str], **kwargs: Dict[str, Any]):
        if self._is_empty(self.registrationNumber):
            self.MissingRequiredField("registrationNumber")
        if not isinstance(self.registrationNumber, str):
            self.registrationNumber = str(self.registrationNumber)

        if self._is_empty(self.legalAddress):
            self.MissingRequiredField("legalAddress")
        if not isinstance(self.legalAddress, Address):
            self.legalAddress = Address(**as_dict(self.legalAddress))

        if self._is_empty(self.headquarterAddress):
            self.MissingRequiredField("headquarterAddress")
        if not isinstance(self.headquarterAddress, Address):
            self.headquarterAddress = Address(**as_dict(self.headquarterAddress))

        if not isinstance(self.parentOrganization, list):
            self.parentOrganization = [self.parentOrganization] if self.parentOrganization is not None else []
        self.parentOrganization = [v if isinstance(v, str) else str(v) for v in self.parentOrganization]

        if not isinstance(self.subOrganization, list):
            self.subOrganization = [self.subOrganization] if self.subOrganization is not None else []
        self.subOrganization = [v if isinstance(v, str) else str(v) for v in self.subOrganization]

        super().__post_init__(**kwargs)


@dataclass
class Address(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = GX.Address
    class_class_curie: ClassVar[str] = "gx:Address"
    class_name: ClassVar[str] = "Address"
    class_model_uri: ClassVar[URIRef] = URIRef("https://w3id.org/gaia-x/Participant/Address")

    country_name: str = None
    locality: str = None
    gps: Optional[str] = None
    street_address: Optional[str] = None
    postal_code: Optional[str] = None
    countryCode: Optional[str] = None

    def __post_init__(self, *_: List[str], **kwargs: Dict[str, Any]):
        if self._is_empty(self.country_name):
            self.MissingRequiredField("country_name")
        if not isinstance(self.country_name, str):
            self.country_name = str(self.country_name)

        if self._is_empty(self.locality):
            self.MissingRequiredField("locality")
        if not isinstance(self.locality, str):
            self.locality = str(self.locality)

        if self.gps is not None and not isinstance(self.gps, str):
            self.gps = str(self.gps)

        if self.street_address is not None and not isinstance(self.street_address, str):
            self.street_address = str(self.street_address)

        if self.postal_code is not None and not isinstance(self.postal_code, str):
            self.postal_code = str(self.postal_code)

        if self.countryCode is not None and not isinstance(self.countryCode, str):
            self.countryCode = str(self.countryCode)

        super().__post_init__(**kwargs)


# Enumerations


# Slots
class slots:
    pass

slots.subOrganization = Slot(uri=SCHEMA.subOrganization, name="subOrganization", curie=SCHEMA.curie('subOrganization'),
                   model_uri=DEFAULT_.subOrganization, domain=None, range=Optional[str])

slots.parentOrganization = Slot(uri=SCHEMA.parentOrganization, name="parentOrganization", curie=SCHEMA.curie('parentOrganization'),
                   model_uri=DEFAULT_.parentOrganization, domain=None, range=Optional[str])

slots.headquarterAddress = Slot(uri=GX.headquarterAddress, name="headquarterAddress", curie=GX.curie('headquarterAddress'),
                   model_uri=DEFAULT_.headquarterAddress, domain=None, range=Optional[Union[dict, Address]])

slots.legalAddress = Slot(uri=GX.legalAddress, name="legalAddress", curie=GX.curie('legalAddress'),
                   model_uri=DEFAULT_.legalAddress, domain=None, range=Optional[Union[dict, Address]])

slots.registrationNumber = Slot(uri=GX.registrationNumber, name="registrationNumber", curie=GX.curie('registrationNumber'),
                   model_uri=DEFAULT_.registrationNumber, domain=None, range=Optional[str])

slots.country_name = Slot(uri=SCHEMA.name, name="country-name", curie=SCHEMA.curie('name'),
                   model_uri=DEFAULT_.country_name, domain=None, range=Optional[str])

slots.gps = Slot(uri=GX.gps, name="gps", curie=GX.curie('gps'),
                   model_uri=DEFAULT_.gps, domain=None, range=Optional[str])

slots.street_address = Slot(uri=SCHEMA.streetAddress, name="street-address", curie=SCHEMA.curie('streetAddress'),
                   model_uri=DEFAULT_.street_address, domain=None, range=Optional[str])

slots.postal_code = Slot(uri=SCHEMA.postalCode, name="postal-code", curie=SCHEMA.curie('postalCode'),
                   model_uri=DEFAULT_.postal_code, domain=None, range=Optional[str])

slots.locality = Slot(uri=SCHEMA.addressLocality, name="locality", curie=SCHEMA.curie('addressLocality'),
                   model_uri=DEFAULT_.locality, domain=None, range=Optional[str])

slots.countryCode = Slot(uri=SCHEMA.addressCountry, name="countryCode", curie=SCHEMA.curie('addressCountry'),
                   model_uri=DEFAULT_.countryCode, domain=None, range=Optional[str])

slots.LegalParticipant_registrationNumber = Slot(uri=GX.registrationNumber, name="LegalParticipant_registrationNumber", curie=GX.curie('registrationNumber'),
                   model_uri=DEFAULT_.LegalParticipant_registrationNumber, domain=LegalParticipant, range=str)

slots.LegalParticipant_legalAddress = Slot(uri=GX.legalAddress, name="LegalParticipant_legalAddress", curie=GX.curie('legalAddress'),
                   model_uri=DEFAULT_.LegalParticipant_legalAddress, domain=LegalParticipant, range=Union[dict, "Address"])

slots.LegalParticipant_headquarterAddress = Slot(uri=GX.headquarterAddress, name="LegalParticipant_headquarterAddress", curie=GX.curie('headquarterAddress'),
                   model_uri=DEFAULT_.LegalParticipant_headquarterAddress, domain=LegalParticipant, range=Union[dict, "Address"])

slots.LegalParticipant_parentOrganization = Slot(uri=SCHEMA.parentOrganization, name="LegalParticipant_parentOrganization", curie=SCHEMA.curie('parentOrganization'),
                   model_uri=DEFAULT_.LegalParticipant_parentOrganization, domain=LegalParticipant, range=Optional[Union[str, List[str]]])

slots.LegalParticipant_subOrganization = Slot(uri=SCHEMA.subOrganization, name="LegalParticipant_subOrganization", curie=SCHEMA.curie('subOrganization'),
                   model_uri=DEFAULT_.LegalParticipant_subOrganization, domain=LegalParticipant, range=Optional[Union[str, List[str]]])

slots.Address_country_name = Slot(uri=SCHEMA.name, name="Address_country-name", curie=SCHEMA.curie('name'),
                   model_uri=DEFAULT_.Address_country_name, domain=Address, range=str)

slots.Address_locality = Slot(uri=SCHEMA.addressLocality, name="Address_locality", curie=SCHEMA.curie('addressLocality'),
                   model_uri=DEFAULT_.Address_locality, domain=Address, range=str)
