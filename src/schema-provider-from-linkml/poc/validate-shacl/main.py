from rdflib import Graph, URIRef, Literal
from rdflib.namespace import RDFS, XSD, FOAF


def display_all_graph(graph: Graph):
    for s, p, o in graph:
        print(s, p, o)


def example1():
    g = Graph()
    g.parse('http://dbpedia.org/resource/Semantic_Web')

    for s, p, o in g:
        print(s, p, o)


def example2():
    g = Graph()
    g.parse('http://dbpedia.org/resource/Semantic_Web')

    semweb = URIRef('http://dbpedia.org/resource/Semantic_Web')
    type = g.value(semweb, RDFS.label)
    print(type)


def example3():
    g = Graph()
    #g.bind("foaf", FOAF)
    #g.bind("xsd", XSD)

    g.add((
        URIRef("http://example.com/person/nick"),
        FOAF.givenName,
        Literal("Nick", datatype=XSD.string)
    ))

    print(g.serialize(format="turtle"))


example3()
