from pyshacl import validate
from rdflib import Graph, URIRef

# data_graph is an rdflib Graph object or file path of the graph to be validated
# shacl_graph is an rdflib Graph object or file path or Web URL of the graph containing the SHACL shapes to validate with, or None if the SHACL shapes are included in the data_graph.
# ont_graph is an rdflib Graph object or file path or Web URL a graph containing extra ontological information, or None if not required. RDFS and OWL definitions from this are used to inoculate the DataGraph.
# inference is a Python string value to indicate whether or not to perform OWL inferencing expansion of the data_graph before validation. Options are 'rdfs', 'owlrl', 'both', or 'none'. The default is 'none'.
# abort_on_first (optional) bool value to indicate whether or not the program should abort after encountering the first validation failure or to continue. Default is to continue.
# allow_infos (optional) bool value, Shapes marked with severity of Info will not cause result to be invalid.
# allow_warnings (optional) bool value, Shapes marked with severity of Warning or Info will not cause result to be invalid.
# meta_shacl (optional) bool value to indicate whether or not the program should enable the Meta-SHACL feature. Default is False.
# advanced: (optional) bool value to enable SHACL Advanced Features
# js: (optional) bool value to enable SHACL-JS Features (if pyshacl[js] is installed)
# debug (optional) bool value to indicate whether or not the program should emit debugging output text, including violations that didn't lead to non-conformance overall. So when debug is True don't judge conformance by absense of violation messages. Default is False.
# Some other optional keyword variables available on the validate function:
#
# data_graph_format: Override the format detection for the given data graph source file.
# shacl_graph_format: Override the format detection for the given shacl graph source file.
# ont_graph_format: Override the format detection for the given extra ontology graph source file.
# iterate_rules: Iterate SHACL Rules until steady state is found (only works with advanced mode).
# do_owl_imports: Enable the feature to allow the import of subgraphs using owl:imports for the shapes graph and the ontology graph. Note, you explicitly cannot use this on the target data graph.
# serialize_report_graph: Convert the report results_graph into a serialised representation (for example, 'turtle')
# check_dash_result: Check the validation result against the given expected DASH test suite result.
# check_sht_result: Check the validation result against the given expected SHT test suite result.

participant_graph = Graph().parse("./participant-example.json", format='json-ld')

participant_shacl = Graph().parse("./participant.shacl.ttl", format='turtle')
print(participant_shacl.serialize())

# g.parse(data=json_ld, format="json-ld")

# print(graph.serialize())
# print(graph.serialize(format="json-ld"))

r = validate(participant_graph,
             shacl_graph=participant_shacl,
             ont_graph=None,
             inference='rdfs',
             abort_on_first=False,
             allow_infos=False,
             allow_warnings=False,
             meta_shacl=False,
             advanced=False,
             js=False,
             debug=False)
conforms, results_graph, results_text = r

print(f"Is conform: {conforms}")
print(f"Result: {results_text}")
