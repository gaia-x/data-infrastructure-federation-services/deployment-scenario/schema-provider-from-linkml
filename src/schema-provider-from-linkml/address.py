# Auto generated from address.yaml by pythongen.py version: 0.9.0
# Generation date: 2023-04-04T13:57:56
# Schema: address
#
# id: https://w3id.org/gaia-x/Address
# description:
# license: https://creativecommons.org/publicdomain/zero/1.0/

import dataclasses
import sys
import re
from jsonasobj2 import JsonObj, as_dict
from typing import Optional, List, Union, Dict, ClassVar, Any
from dataclasses import dataclass
from linkml_runtime.linkml_model.meta import EnumDefinition, PermissibleValue, PvFormulaOptions

from linkml_runtime.utils.slot import Slot
from linkml_runtime.utils.metamodelcore import empty_list, empty_dict, bnode
from linkml_runtime.utils.yamlutils import YAMLRoot, extended_str, extended_float, extended_int
from linkml_runtime.utils.dataclass_extensions_376 import dataclasses_init_fn_with_kwargs
from linkml_runtime.utils.formatutils import camelcase, underscore, sfx
from linkml_runtime.utils.enumerations import EnumDefinitionImpl
from rdflib import Namespace, URIRef
from linkml_runtime.utils.curienamespace import CurieNamespace
from linkml_runtime.linkml_model.types import String

metamodel_version = "1.7.0"
version = "2210"

# Overwrite dataclasses _init_fn to add **kwargs in __init__
dataclasses._init_fn = dataclasses_init_fn_with_kwargs

# Namespaces
GX = CurieNamespace('gx', 'https://w3id.org/gaia-x/')
GX_TRUST_FRAMEWORK = CurieNamespace('gx-trust-framework', 'http://w3id.org/gaia-x/gx-trust-framework/')
LINKML = CurieNamespace('linkml', 'https://w3id.org/linkml/')
SCHEMA = CurieNamespace('schema', 'http://schema.org/')
VCARD = CurieNamespace('vcard', 'https://www.w3.org/2006/vcard/ns#')
DEFAULT_ = CurieNamespace('', 'https://w3id.org/gaia-x/Address/')


# Types

# Class references



@dataclass
class Address(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = GX.Address
    class_class_curie: ClassVar[str] = "gx:Address"
    class_name: ClassVar[str] = "Address"
    class_model_uri: ClassVar[URIRef] = URIRef("https://w3id.org/gaia-x/Address/Address")

    country_name: str = None
    locality: str = None
    gps: Optional[str] = None
    street_address: Optional[str] = None
    postal_code: Optional[str] = None
    countryCode: Optional[str] = None

    def __post_init__(self, *_: List[str], **kwargs: Dict[str, Any]):
        if self._is_empty(self.country_name):
            self.MissingRequiredField("country_name")
        if not isinstance(self.country_name, str):
            self.country_name = str(self.country_name)

        if self._is_empty(self.locality):
            self.MissingRequiredField("locality")
        if not isinstance(self.locality, str):
            self.locality = str(self.locality)

        if self.gps is not None and not isinstance(self.gps, str):
            self.gps = str(self.gps)

        if self.street_address is not None and not isinstance(self.street_address, str):
            self.street_address = str(self.street_address)

        if self.postal_code is not None and not isinstance(self.postal_code, str):
            self.postal_code = str(self.postal_code)

        if self.countryCode is not None and not isinstance(self.countryCode, str):
            self.countryCode = str(self.countryCode)

        super().__post_init__(**kwargs)


# Enumerations


# Slots
class slots:
    pass

slots.country_name = Slot(uri=SCHEMA.name, name="country-name", curie=SCHEMA.curie('name'),
                   model_uri=DEFAULT_.country_name, domain=None, range=Optional[str])

slots.gps = Slot(uri=GX.gps, name="gps", curie=GX.curie('gps'),
                   model_uri=DEFAULT_.gps, domain=None, range=Optional[str])

slots.street_address = Slot(uri=SCHEMA.streetAddress, name="street-address", curie=SCHEMA.curie('streetAddress'),
                   model_uri=DEFAULT_.street_address, domain=None, range=Optional[str])

slots.postal_code = Slot(uri=SCHEMA.postalCode, name="postal-code", curie=SCHEMA.curie('postalCode'),
                   model_uri=DEFAULT_.postal_code, domain=None, range=Optional[str])

slots.locality = Slot(uri=SCHEMA.addressLocality, name="locality", curie=SCHEMA.curie('addressLocality'),
                   model_uri=DEFAULT_.locality, domain=None, range=Optional[str])

slots.countryCode = Slot(uri=SCHEMA.addressCountry, name="countryCode", curie=SCHEMA.curie('addressCountry'),
                   model_uri=DEFAULT_.countryCode, domain=None, range=Optional[str])

slots.Address_country_name = Slot(uri=SCHEMA.name, name="Address_country-name", curie=SCHEMA.curie('name'),
                   model_uri=DEFAULT_.Address_country_name, domain=Address, range=str)

slots.Address_locality = Slot(uri=SCHEMA.addressLocality, name="Address_locality", curie=SCHEMA.curie('addressLocality'),
                   model_uri=DEFAULT_.Address_locality, domain=Address, range=str)
