NAME := schema-provider-from-linkml
SRC := src
INSTALL_STAMP := .install.stamp
POETRY := $(shell command -v poetry 2> /dev/null)

.DEFAULT_GOAL := help

BASE_URL := w3id.org

## $(1) BASE_URL
## $(2) Files to browse to generate
## $(3) INPUT_DIR
## $(4) OUTPUT_DIR
define replace_base_url
	$(foreach f,$(2),sed 's/\$$BASE_URL\$$/$(1)/g' $(3)$(f).yaml > $(4)/$(f).yaml;)
endef

## replace base_url SKIP THIS STEP WHEN GENERATE FOR REGISTRY
## $(1) : INPUT_DIR
## $(2) : OUTPUT_DIR
## $(3) : Files to generate
define launch_generation
# replace base_url SKIP THIS STEP WHEN GENERATE FOR REGISTRY
	mkdir -p $(2)/
	$(foreach f,$(3),cat $(1)$(f).yaml > $(2)/$(f).yaml;)
# schema generation
	mkdir -p $(2)/schema
	$(foreach f,$(3),gen-json-schema $(1)$(f).yaml > $(2)/schema/$(f).schema.json;)
# shacl generation
	mkdir -p $(2)/shacl
# $(foreach f,$(3),gen-shacl --no-metadata $(1)$(f).yaml > $(2)/shacl/$(f).shacl.ttl;)
	$(foreach f,$(3),gen-shacl --no-metadata $(2)//$(f).yaml > $(2)/shacl/$(f).shacl.ttl;)
# context generation
	mkdir -p $(2)/context
#gen-jsonld-context --no-metadata $(1)/participant.yaml > $(2)/context/context.json
	$(foreach f,$(3),gen-jsonld-context --no-metadata $(2)//$(f).yaml > $(2)/context/$(f).context.json;)
# python class
	mkdir -p $(2)/python
	$(foreach f,$(3),gen-python $(2)//$(f).yaml > $(2)/python/$(f).py;)
endef


.PHONY: help
help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo ""
	@echo "  install     			install packages and prepare environment"
	@echo "  clean       			remove all temporary files"
	@echo "  lint        			run the code linters"
	@echo "  format      			reformat code"
	@echo "  pre-commit		  		run pre-commit checks"
	@echo "  pre-commit-a	  		run pre-commit checks on all files"
	@echo "  generate-all			generate all schema from linkml definition"
	@echo "  generate-ontology-2210	generate ontology 22.10 from linkml definition"
	@echo "  generate-ontology-23xx	generate ontology 23.xx from linkml definition"
	@echo "  update-gx-doc			update documentation and files to expose about Trust-Framework"
	@echo "  update-conformity-doc	update documentation and files to expose about Conformity"
	@echo "  update-casco-doc		update documentation and files to expose about CASCO conformity"
	@echo "  update-data-doc		update documentation and files to expose about Data Exchange"
	@echo ""
	@echo "Check the Makefile to know exactly what each target is doing."

install: $(INSTALL_STAMP)
$(INSTALL_STAMP): pyproject.toml poetry.lock
	@if [ -z $(POETRY) ]; then echo "Poetry could not be found. See https://python-poetry.org/docs/"; exit 2; fi
	$(POETRY) install
	pre-commit install --install-hooks
	touch $(INSTALL_STAMP)

.PHONY: clean
clean:
	find . -type d -name "__pycache__" | xargs rm -rf {};
	rm -rf $(INSTALL_STAMP) .coverage .mypy_cache .pytest_cache .ruff_cache output

.PHONY: lint
lint: $(INSTALL_STAMP)
	find . -type d -name "__pycache__" | xargs rm -rf {};
	$(POETRY) run isort --settings-path=pyproject.toml --check-only $(SRC)/$(NAME)
	$(POETRY) run black --check $(SRC)/$(NAME) --config=pyproject.toml --diff
	$(POETRY) run ruff --config=pyproject.toml $(SRC)/$(NAME)

.PHONY: format
format: $(INSTALL_STAMP)
	$(POETRY) run isort --settings-path=pyproject.toml $(SRC)/$(NAME)
	$(POETRY) run black $(SRC)/$(NAME) --config=pyproject.toml

.PHONY: pre-commit
pre-commit: $(INSTALL_STAMP)
	pre-commit run

.PHONY: pre-commit-a
pre-commit-a: $(INSTALL_STAMP)
	pre-commit run --all

.PHONY: generate-all
generate-all: generate-ontology-2210 generate-ontology-23xx

22_10_dirs := $(addsuffix /, $(wildcard linkml/22.10/*))
23_xx_dirs := $(addsuffix /, $(wildcard linkml/23.xx/*))

.PHONY: generate-ontology-2210
generate-ontology-2210:
#@echo $(foreach dir,$(22_10_dirs),$(notdir $(basename $(wildcard $(dir)/*))))
	rm -rf output/22.10
	$(foreach dir,$(22_10_dirs), $(call launch_generation,$(dir),output/$(dir),$(notdir $(basename $(wildcard $(dir)/*)))))
	# mv output/linkml/* output/
	# rmdir output/linkml/
# TODO find how to be generic to create documentation
	mkdir -p output/linkml/22.10/gx/docs/
	gen-doc -d output/linkml/22.10/gx/docs/ output/linkml/22.10/gx/gx-ontology.yaml

.PHONY: generate-ontology-23xx
generate-ontology-23xx:
	rm -rf output/23.xx
	$(foreach dir,$(23_xx_dirs), $(call launch_generation,$(dir),output/$(dir),$(notdir $(basename $(wildcard $(dir)/*)))))
	# mv output/linkml/* output/
	# rmdir output/linkml/
	mkdir -p output/linkml/23.xx/abc-data/docs/
	gen-doc -d output/linkml/23.xx/abc-data/docs/ output/linkml/23.xx/abc-data/data-ontology.yaml
	mkdir -p output/linkml/23.xx/abc-conformity/docs/
	gen-doc -d output/linkml/23.xx/abc-conformity/docs/ output/linkml/23.xx/abc-conformity/conformity-ontology.yaml
	mkdir -p output/linkml/23.xx/aster-data/docs/
	gen-doc -d output/linkml/23.xx/aster-data/docs/ output/linkml/23.xx/aster-data/data-ontology.yaml
	mkdir -p output/linkml/23.xx/aster-conformity/docs/
	gen-doc -d output/linkml/23.xx/aster-conformity/docs/ output/linkml/23.xx/aster-conformity/conformity-ontology.yaml
	mkdir -p output/linkml/23.xx/casco-conformity/docs/
	gen-doc -d output/linkml/23.xx/casco-conformity/docs/ output/linkml/23.xx/casco-conformity/casco-conformity-ontology.yaml

update-conformity-doc:
	rm -vf web/docs/23.xx/abc-conformity/*
	cp output/linkml/23.xx/abc-conformity/docs/* web/docs/23.xx/abc-conformity/
	cp output/linkml/23.xx/abc-conformity/context/conformity-ontology.context.json web/docs/files/23.xx/context/abc-conformity/
	cp output/linkml/23.xx/abc-conformity/shacl/conformity-ontology.shacl.ttl web/docs/files/23.xx/shacl/abc-conformity/
	rm -vf web/docs/23.xx/aster-conformity/*
	cp output/linkml/23.xx/aster-conformity/docs/* web/docs/23.xx/aster-conformity/
	cp output/linkml/23.xx/aster-conformity/context/conformity-ontology.context.json web/docs/files/23.xx/context/aster-conformity/
	cp output/linkml/23.xx/aster-conformity/shacl/conformity-ontology.shacl.ttl web/docs/files/23.xx/shacl/aster-conformity/

update-casco-doc:
	rm -vf web/docs/23.xx/casco-conformity/*
	cp output/linkml/23.xx/casco-conformity/docs/* web/docs/23.xx/casco-conformity/
	cp output/linkml/23.xx/casco-conformity/context/casco-conformity-ontology.context.json web/docs/files/23.xx/context/casco-conformity/
	cp output/linkml/23.xx/casco-conformity/shacl/casco-conformity-ontology.shacl.ttl web/docs/files/23.xx/shacl/casco-conformity/

update-data-doc:
	rm -vf web/docs/23.xx/abc-data/*
	cp output/linkml/23.xx/abc-data/docs/* web/docs/23.xx/abc-data/
	cp output/linkml/23.xx/abc-data/context/data-ontology.context.json web/docs/files/23.xx/context/abc-data/
	cp output/linkml/23.xx/abc-data/shacl/data-ontology.shacl.ttl web/docs/files/23.xx/shacl/abc-data/
	rm -vf web/docs/23.xx/aster-data/*
	cp output/linkml/23.xx/aster-data/docs/* web/docs/23.xx/aster-data/
	cp output/linkml/23.xx/aster-data/context/data-ontology.context.json web/docs/files/23.xx/context/aster-data/
	cp output/linkml/23.xx/aster-data/shacl/data-ontology.shacl.ttl web/docs/files/23.xx/shacl/aster-data/

update-gx-doc:
	rm -vf web/docs/22.10/gx/*
	cp output/linkml/22.10/gx/docs/* web/docs/22.10/gx/
	cp output/linkml/22.10/gx/context/gx-ontology.context.json web/docs/files/22.10/context/
	cp output/linkml/22.10/gx/shacl/gx-ontology.shacl.ttl web/docs/files/22.10/shacl/


tests:
	pytest
