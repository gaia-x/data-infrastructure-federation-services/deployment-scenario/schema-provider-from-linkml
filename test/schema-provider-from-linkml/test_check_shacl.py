import os

from pyshacl import validate
from rdflib import Graph
from typing import Final

ROOT_DIR: Final = os.path.realpath(os.path.dirname(__file__))


class TestFirstTest:
    """Test class for init test"""

    def test_init(self):
        """Test description"""
        a = 3
        b = 2

        r = a + b

        assert r is 5

    def test_valid_minimum_data_product_with_shacl(self):
        graph = Graph().parse(os.path.join(ROOT_DIR, "data/minimum_viable_data_product.json"), format='json-ld')
        shacl = Graph().parse(os.path.join(ROOT_DIR, "shacl/data-product.shacl.ttl"), format='turtle')

        r = validate(graph,
                     shacl_graph=shacl,
                     ont_graph=None,
                     inference='rdfs',
                     abort_on_first=False,
                     allow_infos=False,
                     allow_warnings=False,
                     meta_shacl=False,
                     advanced=False,
                     js=False,
                     debug=False)

        conforms, results_graph, results_text = r

        print(f"Is conform: {conforms}")
        print(f"Result: {results_text}")

        assert conforms is True

    def test_should_not_valid_incomplete_data_product_with_shacl(self):
        graph = Graph().parse(os.path.join(ROOT_DIR, "data/incomplete_data_product.json"), format='json-ld')
        shacl = Graph().parse(os.path.join(ROOT_DIR, "shacl/data-product.shacl.ttl"), format='turtle')

        r = validate(graph,
                     shacl_graph=shacl,
                     ont_graph=None,
                     inference='rdfs',
                     abort_on_first=False,
                     allow_infos=False,
                     allow_warnings=False,
                     meta_shacl=False,
                     advanced=False,
                     js=False,
                     debug=False)

        conforms, results_graph, results_text = r

        print(f"Is conform: {conforms}")
        print(f"Result: {results_text}")

        assert conforms is False

    def test_should_valid_full_data_product_with_shacl(self):
        graph = Graph().parse(os.path.join(ROOT_DIR, "data/complete_viable_data_product.json"), format='json-ld')
        shacl = Graph().parse(os.path.join(ROOT_DIR, "shacl/data-product.shacl.ttl"), format='turtle')

        r = validate(graph,
                     shacl_graph=shacl,
                     ont_graph=None,
                     inference='rdfs',
                     abort_on_first=False,
                     allow_infos=False,
                     allow_warnings=False,
                     meta_shacl=False,
                     advanced=False,
                     js=False,
                     debug=False)

        conforms, results_graph, results_text = r

        print(f"Is conform: {conforms}")
        print(f"Result: {results_text}")

        assert conforms is True

## TODO
# Essayez avec une URL de dataset plutôt qu'un objet complet
# Essayez avec @base et enlever les prefix gx-data sur les différents attributs
# Pas d'id dans la distribution sinon ça fail... est-ce que c'est ce qu'on veut ou pas ??
