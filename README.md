# Schema Provider from Linkml

From a single point of truth which is Linkml description, the project will generate schema with different formats.

This tool is not used to expose ontology, so why the generated ontologies are using a $BASE_URL$ tag. This tag need to
be replaced when the ontology is exposed.

From our side, we are using [Schema Registry](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/schema-registry)
to expose our ontologies.

# How can I use this tool ?

## First try with this tool

```bash
$ make generate-ontology-2210
$ make update-gx-doc
$ cd web
$ mkdocs serve
```

Go to url indicated by the script to see your documentation.

## If you want to make your own ontology

### Step 1: describe your ontology with linkml

In the `/linkml` directory you have the description of Ontology with linkml format.

You can create your own ontology in the appropriate version. You can be inspired by existing ontologies and the
documentation https://linkml.io/linkml/

### Step 2: launch the generation

You need to modify Makefile to add your own generation command or enrich existing.

You can see the `generate-ontology-23xx` command which generate all the ontologies from the `23.xx` directory.

### Step 3: add the generated files to the documentation website source

You can do this step thanks to the `update-*-doc` command.

Once again, create your own or enrich an existing one.

This will enrich the `web` directory which contains sources for the documentation website.

Maybe you will need to create missing directory for a first launch.

### Step 4: update website documentation with manually step

To provide access to the generated files, you need to manually update the structure files of the website.

- `/web/mkdocs.yml`: to add navigation link to the index.md of the new ontology
- `/web/docs/index.md`: to add new index.md file inside the list of the ontologies
- `/web/docs/files/[VERSION]/index.md`: to add link to retrieve generated files like jsonld context or SHACL.

### Step 5: expose your ontology

Think to expose your ontology. We make this step with [Schema Registry](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/schema-registry)
if you need inspiration.

# Functional Documentation

## Gaia-X Core ontology

According to the [Trust Framework 22.10](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/intro/),
the Trust Framework context must be published with the machine-executable implementation of the Trust Framework at
[https://w3id.org/gaia-x/core/](https://w3id.org/gaia-x/core/).

The package gx-core is the linkMl representation of the Trust Framework entities.

# Technical Documentation

## Makefile

The makefile is configured to launch generation of following assets :

- Replace BASE_URL with configured url (skip this step if deployed inside gx-registry)
- Json schema (.schema.json)
- SHACL (.shacl.ttl)
- Context /!\ One context by declaration, by default it's participant
- Python class

## Command to launch generation

#### Launch generation of schema declared inside trust-framework document.

```shell
make generate-gx-core
```

#### Launch generation of schema used by GXFSFR.
```shell
make generate-gx-service
```

#### Launch generation of schema for data exchange

[Spec file](https://gitlab.com/gaia-x/technical-committee/federation-services/data-exchange/-/blob/main/specs/dewg.md)

```shell
make generate-gx-data
```

## Tests

Tests are implemented to check SHACL files with data objets. You can launch this test with following command :

```shell
make tests
```

## Deploy schema provider

This step will deploy documentation and will expose generated files.

Steps reminder :

- generate documentation with gen-doc cmd (```make generate-gx-data-doc```)
- cp generated files into files section of the web project
- check if files link are exposed in version file (ex: [22.10 version file](web/docs/files/22.10/index.md))
- ```mkdocs build``` or ```mkdocs serve``` (local test)
- launch CI
