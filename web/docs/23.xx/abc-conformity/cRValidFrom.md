# Slot: cRValidFrom

URI: [abc-conformity:cRValidFrom](https://$BASE_URL$/abc-conformity#cRValidFrom)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [Datetime](Datetime.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: cRValidFrom
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:cRValidFrom
alias: cRValidFrom
domain_of:
- ComplianceReference
range: datetime

```
</details>