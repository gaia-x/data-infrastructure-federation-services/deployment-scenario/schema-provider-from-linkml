# Slot: hasDescription

URI: [abc-conformity:hasDescription](https://$BASE_URL$/abc-conformity#hasDescription)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCriterion](ComplianceCriterion.md) |  |  yes  |
[ComplianceLabel](ComplianceLabel.md) |  |  yes  |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [String](String.md)






## Examples

| Value |
| --- |
| GAIA-X |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasDescription
examples:
- value: GAIA-X
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasDescription
alias: hasDescription
domain_of:
- ComplianceCriterion
- ComplianceLabel
- ComplianceReference
range: string

```
</details>