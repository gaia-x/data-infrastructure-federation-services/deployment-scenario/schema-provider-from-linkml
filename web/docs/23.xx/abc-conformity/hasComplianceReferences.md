# Slot: hasComplianceReferences


_List of Ids of ComplianceReferences (self-description) managed by this ComplianceReferenceManager_



URI: [abc-conformity:hasComplianceReferences](https://$BASE_URL$/abc-conformity#hasComplianceReferences)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReferenceManager](ComplianceReferenceManager.md) |  |  no  |







## Properties

* Range: [ComplianceReference](ComplianceReference.md)

* Multivalued: True

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceReferences
description: List of Ids of ComplianceReferences (self-description) managed by this
  ComplianceReferenceManager
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasComplianceReferences
multivalued: true
alias: hasComplianceReferences
owner: ComplianceReferenceManager
domain_of:
- ComplianceReferenceManager
range: ComplianceReference
required: true

```
</details>