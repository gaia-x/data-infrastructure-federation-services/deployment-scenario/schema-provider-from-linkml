# Slot: hasLevel

URI: [abc-conformity:hasLevel](https://$BASE_URL$/abc-conformity#hasLevel)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCriterion](ComplianceCriterion.md) |  |  yes  |
[ComplianceLabel](ComplianceLabel.md) |  |  yes  |







## Properties

* Range: [Integer](Integer.md)






## Examples

| Value |
| --- |
| 1 |
| 2 |
| 3 |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasLevel
examples:
- value: '1'
- value: '2'
- value: '3'
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasLevel
alias: hasLevel
domain_of:
- ComplianceCriterion
- ComplianceLabel
range: integer

```
</details>