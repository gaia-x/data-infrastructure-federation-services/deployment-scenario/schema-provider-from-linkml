# Slot: hasComplianceReference

URI: [abc-conformity:hasComplianceReference](https://$BASE_URL$/abc-conformity#hasComplianceReference)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCertificationScheme](ComplianceCertificationScheme.md) |  |  yes  |
[ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) |  |  no  |







## Properties

* Range: [ComplianceReference](ComplianceReference.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceReference
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasComplianceReference
alias: hasComplianceReference
domain_of:
- ComplianceCertificationScheme
range: ComplianceReference

```
</details>