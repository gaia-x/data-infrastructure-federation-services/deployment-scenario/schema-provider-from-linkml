# Slot: hasCategory

URI: [abc-conformity:hasCategory](https://$BASE_URL$/abc-conformity#hasCategory)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCriterion](ComplianceCriterion.md) |  |  yes  |







## Properties

* Range: [String](String.md)






## Examples

| Value |
| --- |
| Data Protection |
| Transparency |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasCategory
examples:
- value: Data Protection
- value: Transparency
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasCategory
alias: hasCategory
domain_of:
- ComplianceCriterion
range: string

```
</details>