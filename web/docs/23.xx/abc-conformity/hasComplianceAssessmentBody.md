# Slot: hasComplianceAssessmentBody

URI: [abc-conformity:hasComplianceAssessmentBody](https://$BASE_URL$/abc-conformity#hasComplianceAssessmentBody)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) |  |  yes  |
[ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) |  |  yes  |







## Properties

* Range: [ComplianceAssessmentBody](ComplianceAssessmentBody.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceAssessmentBody
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasComplianceAssessmentBody
alias: hasComplianceAssessmentBody
domain_of:
- ThirdPartyComplianceCertificationScheme
- ThirdPartyComplianceCertificateClaim
range: ComplianceAssessmentBody

```
</details>