# Slot: isValid

URI: [abc-conformity:isValid](https://$BASE_URL$/abc-conformity#isValid)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SelfAssessedComplianceCriteriaCredential](SelfAssessedComplianceCriteriaCredential.md) |  |  yes  |
[ComplianceCertificateCredential](ComplianceCertificateCredential.md) |  |  yes  |
[ThirdPartyComplianceCertificateCredential](ThirdPartyComplianceCertificateCredential.md) |  |  no  |







## Properties

* Range: [Boolean](Boolean.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: isValid
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:isValid
alias: isValid
domain_of:
- SelfAssessedComplianceCriteriaCredential
- ComplianceCertificateCredential
range: boolean

```
</details>