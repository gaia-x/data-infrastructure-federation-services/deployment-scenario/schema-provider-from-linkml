# Class: ComplianceAssessmentBody



URI: [abc-conformity:ComplianceAssessmentBody](https://$BASE_URL$/abc-conformity#ComplianceAssessmentBody)




```mermaid
 classDiagram
    class ComplianceAssessmentBody
      ComplianceAssessmentBody : canCertifyThirdPartyComplianceCertificationScheme
        
          ComplianceAssessmentBody --|> ThirdPartyComplianceCertificationScheme : canCertifyThirdPartyComplianceCertificationScheme
        
      ComplianceAssessmentBody : hasThirdPartyComplianceCertificateClaim
        
          ComplianceAssessmentBody --|> ThirdPartyComplianceCertificateClaim : hasThirdPartyComplianceCertificateClaim
        
      ComplianceAssessmentBody : hasThirdPartyComplianceCredential
        
          ComplianceAssessmentBody --|> ThirdPartyComplianceCertificateCredential : hasThirdPartyComplianceCredential
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [canCertifyThirdPartyComplianceCertificationScheme](canCertifyThirdPartyComplianceCertificationScheme.md) | 1..* <br/> [ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) | Ids of the Third Party Compliance Certification Scheme this Compliance Assess... | direct |
| [hasThirdPartyComplianceCertificateClaim](hasThirdPartyComplianceCertificateClaim.md) | 1..* <br/> [ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) | Ids of Certificate claims issued by this Compliance Assessment Body | direct |
| [hasThirdPartyComplianceCredential](hasThirdPartyComplianceCredential.md) | 1..* <br/> [ThirdPartyComplianceCertificateCredential](ThirdPartyComplianceCertificateCredential.md) | Ids of the Certificate claims VC certified by this Compliance Assessment Body | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) | [hasComplianceAssessmentBody](hasComplianceAssessmentBody.md) | range | [ComplianceAssessmentBody](ComplianceAssessmentBody.md) |
| [ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) | [hasComplianceAssessmentBody](hasComplianceAssessmentBody.md) | range | [ComplianceAssessmentBody](ComplianceAssessmentBody.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | abc-conformity:ComplianceAssessmentBody |
| native | https://$BASE_URL$/abc-conformity/:ComplianceAssessmentBody |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ComplianceAssessmentBody
from_schema: https://$BASE_URL$/abc-conformity
slots:
- canCertifyThirdPartyComplianceCertificationScheme
- hasThirdPartyComplianceCertificateClaim
- hasThirdPartyComplianceCredential
slot_usage:
  canCertifyThirdPartyComplianceCertificationScheme:
    name: canCertifyThirdPartyComplianceCertificationScheme
    description: Ids of the Third Party Compliance Certification Scheme this Compliance
      Assessment Body can certify.
    multivalued: true
    domain_of:
    - ComplianceAssessmentBody
    required: true
  hasThirdPartyComplianceCertificateClaim:
    name: hasThirdPartyComplianceCertificateClaim
    description: Ids of Certificate claims issued by this Compliance Assessment Body.
    multivalued: true
    domain_of:
    - ComplianceAssessmentBody
    required: true
  hasThirdPartyComplianceCredential:
    name: hasThirdPartyComplianceCredential
    description: Ids of the Certificate claims VC certified by this Compliance Assessment
      Body.
    multivalued: true
    domain_of:
    - ComplianceAssessmentBody
    required: true
class_uri: abc-conformity:ComplianceAssessmentBody

```
</details>

### Induced

<details>
```yaml
name: ComplianceAssessmentBody
from_schema: https://$BASE_URL$/abc-conformity
slot_usage:
  canCertifyThirdPartyComplianceCertificationScheme:
    name: canCertifyThirdPartyComplianceCertificationScheme
    description: Ids of the Third Party Compliance Certification Scheme this Compliance
      Assessment Body can certify.
    multivalued: true
    domain_of:
    - ComplianceAssessmentBody
    required: true
  hasThirdPartyComplianceCertificateClaim:
    name: hasThirdPartyComplianceCertificateClaim
    description: Ids of Certificate claims issued by this Compliance Assessment Body.
    multivalued: true
    domain_of:
    - ComplianceAssessmentBody
    required: true
  hasThirdPartyComplianceCredential:
    name: hasThirdPartyComplianceCredential
    description: Ids of the Certificate claims VC certified by this Compliance Assessment
      Body.
    multivalued: true
    domain_of:
    - ComplianceAssessmentBody
    required: true
attributes:
  canCertifyThirdPartyComplianceCertificationScheme:
    name: canCertifyThirdPartyComplianceCertificationScheme
    description: Ids of the Third Party Compliance Certification Scheme this Compliance
      Assessment Body can certify.
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:canCertifyThirdPartyComplianceCertificationScheme
    multivalued: true
    alias: canCertifyThirdPartyComplianceCertificationScheme
    owner: ComplianceAssessmentBody
    domain_of:
    - ComplianceAssessmentBody
    range: ThirdPartyComplianceCertificationScheme
    required: true
  hasThirdPartyComplianceCertificateClaim:
    name: hasThirdPartyComplianceCertificateClaim
    description: Ids of Certificate claims issued by this Compliance Assessment Body.
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasThirdPartyComplianceCertificateClaim
    multivalued: true
    alias: hasThirdPartyComplianceCertificateClaim
    owner: ComplianceAssessmentBody
    domain_of:
    - ComplianceAssessmentBody
    range: ThirdPartyComplianceCertificateClaim
    required: true
  hasThirdPartyComplianceCredential:
    name: hasThirdPartyComplianceCredential
    description: Ids of the Certificate claims VC certified by this Compliance Assessment
      Body.
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasThirdPartyComplianceCredential
    multivalued: true
    alias: hasThirdPartyComplianceCredential
    owner: ComplianceAssessmentBody
    domain_of:
    - ComplianceAssessmentBody
    range: ThirdPartyComplianceCertificateCredential
    required: true
class_uri: abc-conformity:ComplianceAssessmentBody

```
</details>