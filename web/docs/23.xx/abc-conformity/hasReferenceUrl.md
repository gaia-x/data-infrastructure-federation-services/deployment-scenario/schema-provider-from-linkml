# Slot: hasReferenceUrl

URI: [abc-conformity:hasReferenceUrl](https://$BASE_URL$/abc-conformity#hasReferenceUrl)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasReferenceUrl
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasReferenceUrl
alias: hasReferenceUrl
domain_of:
- ComplianceReference
range: string

```
</details>