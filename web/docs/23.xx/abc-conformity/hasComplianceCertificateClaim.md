# Slot: hasComplianceCertificateClaim

URI: [abc-conformity:hasComplianceCertificateClaim](https://$BASE_URL$/abc-conformity#hasComplianceCertificateClaim)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LocatedServiceOffering](LocatedServiceOffering.md) |  |  yes  |







## Properties

* Range: [ComplianceCertificateClaim](ComplianceCertificateClaim.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceCertificateClaim
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasComplianceCertificateClaim
alias: hasComplianceCertificateClaim
domain_of:
- LocatedServiceOffering
range: ComplianceCertificateClaim

```
</details>