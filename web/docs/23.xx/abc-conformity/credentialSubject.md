# Slot: credentialSubject

URI: [abc-conformity:credentialSubject](https://$BASE_URL$/abc-conformity#credentialSubject)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SelfAssessedComplianceCriteriaCredential](SelfAssessedComplianceCriteriaCredential.md) |  |  yes  |
[ComplianceCertificateCredential](ComplianceCertificateCredential.md) |  |  yes  |
[ThirdPartyComplianceCertificateCredential](ThirdPartyComplianceCertificateCredential.md) |  |  yes  |







## Properties

* Range: [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: credentialSubject
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:credentialSubject
alias: credentialSubject
domain_of:
- SelfAssessedComplianceCriteriaCredential
- ComplianceCertificateCredential
range: SelfAssessedComplianceCriteriaClaim

```
</details>