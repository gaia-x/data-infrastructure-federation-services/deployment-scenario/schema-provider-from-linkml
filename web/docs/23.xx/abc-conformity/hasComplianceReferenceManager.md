# Slot: hasComplianceReferenceManager

URI: [abc-conformity:hasComplianceReferenceManager](https://$BASE_URL$/abc-conformity#hasComplianceReferenceManager)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [ComplianceReferenceManager](ComplianceReferenceManager.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceReferenceManager
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasComplianceReferenceManager
alias: hasComplianceReferenceManager
domain_of:
- ComplianceReference
range: ComplianceReferenceManager

```
</details>