# Class: LocatedServiceOffering



URI: [abc-conformity:LocatedServiceOffering](https://$BASE_URL$/abc-conformity#LocatedServiceOffering)




```mermaid
 classDiagram
    class LocatedServiceOffering
      LocatedServiceOffering : hasComplianceCertificateClaim
        
          LocatedServiceOffering --|> ComplianceCertificateClaim : hasComplianceCertificateClaim
        
      LocatedServiceOffering : hasComplianceCertificateCredential
        
          LocatedServiceOffering --|> ComplianceCertificateCredential : hasComplianceCertificateCredential
        
      LocatedServiceOffering : isHostedOn
        
      LocatedServiceOffering : isImplementationOf
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [isImplementationOf](isImplementationOf.md) | 1..1 <br/> [String](String.md) | Id of the Service Offering referenced by this located service | direct |
| [isHostedOn](isHostedOn.md) | 1..1 <br/> [String](String.md) | Id of the Location where this located service is hosted on | direct |
| [hasComplianceCertificateClaim](hasComplianceCertificateClaim.md) | 0..* <br/> [ComplianceCertificateClaim](ComplianceCertificateClaim.md) | Ids of the compliance reference claims claimed by the provider for the locate... | direct |
| [hasComplianceCertificateCredential](hasComplianceCertificateCredential.md) | 0..* <br/> [ComplianceCertificateCredential](ComplianceCertificateCredential.md) | Ids of the compliance reference credential for the located service | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) | [hasLocatedServiceOffering](hasLocatedServiceOffering.md) | range | [LocatedServiceOffering](LocatedServiceOffering.md) |
| [ComplianceCertificateClaim](ComplianceCertificateClaim.md) | [hasLocatedServiceOffering](hasLocatedServiceOffering.md) | range | [LocatedServiceOffering](LocatedServiceOffering.md) |
| [ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) | [hasLocatedServiceOffering](hasLocatedServiceOffering.md) | range | [LocatedServiceOffering](LocatedServiceOffering.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | abc-conformity:LocatedServiceOffering |
| native | https://$BASE_URL$/abc-conformity/:LocatedServiceOffering |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: LocatedServiceOffering
from_schema: https://$BASE_URL$/abc-conformity
slots:
- isImplementationOf
- isHostedOn
- hasComplianceCertificateClaim
- hasComplianceCertificateCredential
slot_usage:
  isImplementationOf:
    name: isImplementationOf
    description: Id of the Service Offering referenced by this located service
    domain_of:
    - LocatedServiceOffering
    required: true
  isHostedOn:
    name: isHostedOn
    description: Id of the Location where this located service is hosted on
    domain_of:
    - LocatedServiceOffering
    required: true
  hasComplianceCertificateClaim:
    name: hasComplianceCertificateClaim
    description: Ids of the compliance reference claims claimed by the provider for
      the located service
    multivalued: true
    domain_of:
    - LocatedServiceOffering
  hasComplianceCertificateCredential:
    name: hasComplianceCertificateCredential
    description: Ids of the compliance reference credential for the located service
    multivalued: true
    domain_of:
    - LocatedServiceOffering
class_uri: abc-conformity:LocatedServiceOffering

```
</details>

### Induced

<details>
```yaml
name: LocatedServiceOffering
from_schema: https://$BASE_URL$/abc-conformity
slot_usage:
  isImplementationOf:
    name: isImplementationOf
    description: Id of the Service Offering referenced by this located service
    domain_of:
    - LocatedServiceOffering
    required: true
  isHostedOn:
    name: isHostedOn
    description: Id of the Location where this located service is hosted on
    domain_of:
    - LocatedServiceOffering
    required: true
  hasComplianceCertificateClaim:
    name: hasComplianceCertificateClaim
    description: Ids of the compliance reference claims claimed by the provider for
      the located service
    multivalued: true
    domain_of:
    - LocatedServiceOffering
  hasComplianceCertificateCredential:
    name: hasComplianceCertificateCredential
    description: Ids of the compliance reference credential for the located service
    multivalued: true
    domain_of:
    - LocatedServiceOffering
attributes:
  isImplementationOf:
    name: isImplementationOf
    description: Id of the Service Offering referenced by this located service
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:isImplementationOf
    alias: isImplementationOf
    owner: LocatedServiceOffering
    domain_of:
    - LocatedServiceOffering
    range: string
    required: true
  isHostedOn:
    name: isHostedOn
    description: Id of the Location where this located service is hosted on
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:isHostedOn
    alias: isHostedOn
    owner: LocatedServiceOffering
    domain_of:
    - LocatedServiceOffering
    range: string
    required: true
  hasComplianceCertificateClaim:
    name: hasComplianceCertificateClaim
    description: Ids of the compliance reference claims claimed by the provider for
      the located service
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasComplianceCertificateClaim
    multivalued: true
    alias: hasComplianceCertificateClaim
    owner: LocatedServiceOffering
    domain_of:
    - LocatedServiceOffering
    range: ComplianceCertificateClaim
  hasComplianceCertificateCredential:
    name: hasComplianceCertificateCredential
    description: Ids of the compliance reference credential for the located service
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasComplianceCertificateCredential
    multivalued: true
    alias: hasComplianceCertificateCredential
    owner: LocatedServiceOffering
    domain_of:
    - LocatedServiceOffering
    range: ComplianceCertificateCredential
class_uri: abc-conformity:LocatedServiceOffering

```
</details>