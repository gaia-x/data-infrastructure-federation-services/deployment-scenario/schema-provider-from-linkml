# Class: ThirdPartyComplianceCertificationScheme



URI: [abc-conformity:ThirdPartyComplianceCertificationScheme](https://$BASE_URL$/abc-conformity#ThirdPartyComplianceCertificationScheme)




```mermaid
 classDiagram
    class ThirdPartyComplianceCertificationScheme
      ComplianceCertificationScheme <|-- ThirdPartyComplianceCertificationScheme
      
      ThirdPartyComplianceCertificationScheme : grantsComplianceCriteria
        
          ThirdPartyComplianceCertificationScheme --|> ComplianceCriterion : grantsComplianceCriteria
        
      ThirdPartyComplianceCertificationScheme : hasComplianceAssessmentBody
        
          ThirdPartyComplianceCertificationScheme --|> ComplianceAssessmentBody : hasComplianceAssessmentBody
        
      ThirdPartyComplianceCertificationScheme : hasComplianceReference
        
          ThirdPartyComplianceCertificationScheme --|> ComplianceReference : hasComplianceReference
        
      
```





## Inheritance
* [ComplianceCertificationScheme](ComplianceCertificationScheme.md)
    * **ThirdPartyComplianceCertificationScheme**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasComplianceAssessmentBody](hasComplianceAssessmentBody.md) | 1..* <br/> [ComplianceAssessmentBody](ComplianceAssessmentBody.md) | List of Ids of ComplianceAssessmentBody (participant) endorsed having a Compl... | direct |
| [hasComplianceReference](hasComplianceReference.md) | 1..1 <br/> [ComplianceReference](ComplianceReference.md) | Id of Compliance Reference (self-description) to be certified by any means de... | [ComplianceCertificationScheme](ComplianceCertificationScheme.md) |
| [grantsComplianceCriteria](grantsComplianceCriteria.md) | 0..* <br/> [ComplianceCriterion](ComplianceCriterion.md) | List of Compliance Criteria granted by the scheme in case of certification | [ComplianceCertificationScheme](ComplianceCertificationScheme.md) |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ComplianceAssessmentBody](ComplianceAssessmentBody.md) | [canCertifyThirdPartyComplianceCertificationScheme](canCertifyThirdPartyComplianceCertificationScheme.md) | range | [ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | abc-conformity:ThirdPartyComplianceCertificationScheme |
| native | https://$BASE_URL$/abc-conformity/:ThirdPartyComplianceCertificationScheme |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ThirdPartyComplianceCertificationScheme
from_schema: https://$BASE_URL$/abc-conformity
is_a: ComplianceCertificationScheme
slots:
- hasComplianceAssessmentBody
slot_usage:
  hasComplianceAssessmentBody:
    name: hasComplianceAssessmentBody
    description: List of Ids of ComplianceAssessmentBody (participant) endorsed having
      a Compliance Assessment Body role by the Compliance Reference. This list is
      managed and validated by the Compliance Reference Manager
    multivalued: true
    domain_of:
    - ThirdPartyComplianceCertificationScheme
    - ThirdPartyComplianceCertificateClaim
    required: true
class_uri: abc-conformity:ThirdPartyComplianceCertificationScheme

```
</details>

### Induced

<details>
```yaml
name: ThirdPartyComplianceCertificationScheme
from_schema: https://$BASE_URL$/abc-conformity
is_a: ComplianceCertificationScheme
slot_usage:
  hasComplianceAssessmentBody:
    name: hasComplianceAssessmentBody
    description: List of Ids of ComplianceAssessmentBody (participant) endorsed having
      a Compliance Assessment Body role by the Compliance Reference. This list is
      managed and validated by the Compliance Reference Manager
    multivalued: true
    domain_of:
    - ThirdPartyComplianceCertificationScheme
    - ThirdPartyComplianceCertificateClaim
    required: true
attributes:
  hasComplianceAssessmentBody:
    name: hasComplianceAssessmentBody
    description: List of Ids of ComplianceAssessmentBody (participant) endorsed having
      a Compliance Assessment Body role by the Compliance Reference. This list is
      managed and validated by the Compliance Reference Manager
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasComplianceAssessmentBody
    multivalued: true
    alias: hasComplianceAssessmentBody
    owner: ThirdPartyComplianceCertificationScheme
    domain_of:
    - ThirdPartyComplianceCertificationScheme
    - ThirdPartyComplianceCertificateClaim
    range: ComplianceAssessmentBody
    required: true
  hasComplianceReference:
    name: hasComplianceReference
    description: Id of Compliance Reference (self-description) to be certified by
      any means defined in the Certification Scheme
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasComplianceReference
    alias: hasComplianceReference
    owner: ThirdPartyComplianceCertificationScheme
    domain_of:
    - ComplianceCertificationScheme
    range: ComplianceReference
    required: true
  grantsComplianceCriteria:
    name: grantsComplianceCriteria
    description: List of Compliance Criteria granted by the scheme in case of certification
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:grantsComplianceCriteria
    multivalued: true
    alias: grantsComplianceCriteria
    owner: ThirdPartyComplianceCertificationScheme
    domain_of:
    - ComplianceCertificationScheme
    range: ComplianceCriterion
class_uri: abc-conformity:ThirdPartyComplianceCertificationScheme

```
</details>