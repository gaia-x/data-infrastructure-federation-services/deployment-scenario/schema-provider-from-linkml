# Slot: canCertifyThirdPartyComplianceCertificationScheme

URI: [abc-conformity:canCertifyThirdPartyComplianceCertificationScheme](https://$BASE_URL$/abc-conformity#canCertifyThirdPartyComplianceCertificationScheme)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceAssessmentBody](ComplianceAssessmentBody.md) |  |  yes  |







## Properties

* Range: [ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: canCertifyThirdPartyComplianceCertificationScheme
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:canCertifyThirdPartyComplianceCertificationScheme
alias: canCertifyThirdPartyComplianceCertificationScheme
domain_of:
- ComplianceAssessmentBody
range: ThirdPartyComplianceCertificationScheme

```
</details>