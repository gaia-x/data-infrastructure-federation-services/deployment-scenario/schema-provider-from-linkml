# Slot: hasThirdPartyComplianceCertificateClaim

URI: [abc-conformity:hasThirdPartyComplianceCertificateClaim](https://$BASE_URL$/abc-conformity#hasThirdPartyComplianceCertificateClaim)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceAssessmentBody](ComplianceAssessmentBody.md) |  |  yes  |







## Properties

* Range: [ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasThirdPartyComplianceCertificateClaim
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasThirdPartyComplianceCertificateClaim
alias: hasThirdPartyComplianceCertificateClaim
domain_of:
- ComplianceAssessmentBody
range: ThirdPartyComplianceCertificateClaim

```
</details>