# Slot: hasComplianceReferenceTitle

URI: [abc-conformity:hasComplianceReferenceTitle](https://$BASE_URL$/abc-conformity#hasComplianceReferenceTitle)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceReferenceTitle
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasComplianceReferenceTitle
alias: hasComplianceReferenceTitle
domain_of:
- ComplianceReference
range: string

```
</details>