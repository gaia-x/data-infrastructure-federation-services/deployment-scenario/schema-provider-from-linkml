# Class: SelfAssessedComplianceCriteriaClaim



URI: [abc-conformity:SelfAssessedComplianceCriteriaClaim](https://$BASE_URL$/abc-conformity#SelfAssessedComplianceCriteriaClaim)




```mermaid
 classDiagram
    class SelfAssessedComplianceCriteriaClaim
      SelfAssessedComplianceCriteriaClaim : hasAssessedComplianceCriteria
        
          SelfAssessedComplianceCriteriaClaim --|> ComplianceCriterion : hasAssessedComplianceCriteria
        
      SelfAssessedComplianceCriteriaClaim : hasLocatedServiceOffering
        
          SelfAssessedComplianceCriteriaClaim --|> LocatedServiceOffering : hasLocatedServiceOffering
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasLocatedServiceOffering](hasLocatedServiceOffering.md) | 1..1 <br/> [LocatedServiceOffering](LocatedServiceOffering.md) | Id of the LocatedServiceOffering (self-description) that claims the complianc... | direct |
| [hasAssessedComplianceCriteria](hasAssessedComplianceCriteria.md) | 0..* <br/> [ComplianceCriterion](ComplianceCriterion.md) | Ids of the Criterion (self-description) that are declared and claim by Locate... | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [SelfAssessedComplianceCriteriaCredential](SelfAssessedComplianceCriteriaCredential.md) | [credentialSubject](credentialSubject.md) | range | [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) |
| [ComplianceCertificateCredential](ComplianceCertificateCredential.md) | [credentialSubject](credentialSubject.md) | range | [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) |
| [ThirdPartyComplianceCertificateCredential](ThirdPartyComplianceCertificateCredential.md) | [credentialSubject](credentialSubject.md) | range | [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | abc-conformity:SelfAssessedComplianceCriteriaClaim |
| native | https://$BASE_URL$/abc-conformity/:SelfAssessedComplianceCriteriaClaim |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: SelfAssessedComplianceCriteriaClaim
from_schema: https://$BASE_URL$/abc-conformity
slots:
- hasLocatedServiceOffering
- hasAssessedComplianceCriteria
slot_usage:
  hasLocatedServiceOffering:
    name: hasLocatedServiceOffering
    description: Id of the LocatedServiceOffering (self-description) that claims the
      compliance criteria
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
    - ComplianceCertificateClaim
    required: true
  hasAssessedComplianceCriteria:
    name: hasAssessedComplianceCriteria
    description: Ids of the Criterion (self-description) that are declared and claim
      by LocatedService
    multivalued: true
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
class_uri: abc-conformity:SelfAssessedComplianceCriteriaClaim

```
</details>

### Induced

<details>
```yaml
name: SelfAssessedComplianceCriteriaClaim
from_schema: https://$BASE_URL$/abc-conformity
slot_usage:
  hasLocatedServiceOffering:
    name: hasLocatedServiceOffering
    description: Id of the LocatedServiceOffering (self-description) that claims the
      compliance criteria
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
    - ComplianceCertificateClaim
    required: true
  hasAssessedComplianceCriteria:
    name: hasAssessedComplianceCriteria
    description: Ids of the Criterion (self-description) that are declared and claim
      by LocatedService
    multivalued: true
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
attributes:
  hasLocatedServiceOffering:
    name: hasLocatedServiceOffering
    description: Id of the LocatedServiceOffering (self-description) that claims the
      compliance criteria
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasServiceOffering
    alias: hasLocatedServiceOffering
    owner: SelfAssessedComplianceCriteriaClaim
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
    - ComplianceCertificateClaim
    range: LocatedServiceOffering
    required: true
  hasAssessedComplianceCriteria:
    name: hasAssessedComplianceCriteria
    description: Ids of the Criterion (self-description) that are declared and claim
      by LocatedService
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasAssessedComplianceCriteria
    multivalued: true
    alias: hasAssessedComplianceCriteria
    owner: SelfAssessedComplianceCriteriaClaim
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
    range: ComplianceCriterion
class_uri: abc-conformity:SelfAssessedComplianceCriteriaClaim

```
</details>