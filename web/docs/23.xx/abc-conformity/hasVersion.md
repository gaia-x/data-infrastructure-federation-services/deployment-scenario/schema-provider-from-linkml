# Slot: hasVersion

URI: [abc-conformity:hasVersion](https://$BASE_URL$/abc-conformity#hasVersion)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasVersion
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasVersion
alias: hasVersion
domain_of:
- ComplianceReference
range: string

```
</details>