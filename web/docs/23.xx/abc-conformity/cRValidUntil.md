# Slot: cRValidUntil

URI: [abc-conformity:cRValidUntil](https://$BASE_URL$/abc-conformity#cRValidUntil)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [Datetime](Datetime.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: cRValidUntil
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:cRValidUntil
alias: cRValidUntil
domain_of:
- ComplianceReference
range: datetime

```
</details>