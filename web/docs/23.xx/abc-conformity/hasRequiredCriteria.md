# Slot: hasRequiredCriteria

URI: [abc-conformity:hasRequiredCriteria](https://$BASE_URL$/abc-conformity#hasRequiredCriteria)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceLabel](ComplianceLabel.md) |  |  yes  |







## Properties

* Range: [ComplianceCriterion](ComplianceCriterion.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasRequiredCriteria
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasRequiredCriteria
alias: hasRequiredCriteria
domain_of:
- ComplianceLabel
range: ComplianceCriterion

```
</details>