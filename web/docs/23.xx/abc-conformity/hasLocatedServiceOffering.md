# Slot: hasLocatedServiceOffering

URI: [abc-conformity:hasServiceOffering](https://$BASE_URL$/abc-conformity#hasServiceOffering)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) |  |  yes  |
[ComplianceCertificateClaim](ComplianceCertificateClaim.md) |  |  yes  |
[ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) |  |  no  |







## Properties

* Range: [LocatedServiceOffering](LocatedServiceOffering.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasLocatedServiceOffering
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasServiceOffering
alias: hasLocatedServiceOffering
domain_of:
- SelfAssessedComplianceCriteriaClaim
- ComplianceCertificateClaim
range: LocatedServiceOffering

```
</details>