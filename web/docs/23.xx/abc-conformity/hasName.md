# Slot: hasName

URI: [abc-conformity:hasName](https://$BASE_URL$/abc-conformity#hasName)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCriterion](ComplianceCriterion.md) |  |  yes  |
[ComplianceLabel](ComplianceLabel.md) |  |  yes  |







## Properties

* Range: [String](String.md)






## Examples

| Value |
| --- |
| GAIA-X C1/L1 |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasName
examples:
- value: GAIA-X C1/L1
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasName
alias: hasName
domain_of:
- ComplianceCriterion
- ComplianceLabel
range: string

```
</details>