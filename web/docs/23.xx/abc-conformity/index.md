# abc-conformity



URI: https://$BASE_URL$/abc-conformity

Name: abc-conformity



## Classes

| Class | Description |
| --- | --- |
| [ComplianceAssessmentBody](ComplianceAssessmentBody.md) | None |
| [ComplianceCertificateClaim](ComplianceCertificateClaim.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) | None |
| [ComplianceCertificateCredential](ComplianceCertificateCredential.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ThirdPartyComplianceCertificateCredential](ThirdPartyComplianceCertificateCredential.md) | None |
| [ComplianceCertificationScheme](ComplianceCertificationScheme.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) | None |
| [ComplianceCriterion](ComplianceCriterion.md) | None |
| [ComplianceLabel](ComplianceLabel.md) | None |
| [ComplianceReference](ComplianceReference.md) | None |
| [ComplianceReferenceManager](ComplianceReferenceManager.md) | None |
| [LocatedServiceOffering](LocatedServiceOffering.md) | None |
| [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) | None |
| [SelfAssessedComplianceCriteriaCredential](SelfAssessedComplianceCriteriaCredential.md) | None |



## Slots

| Slot | Description |
| --- | --- |
| [canBeSelfAssessed](canBeSelfAssessed.md) |  |
| [canCertifyThirdPartyComplianceCertificationScheme](canCertifyThirdPartyComplianceCertificationScheme.md) |  |
| [credentialSubject](credentialSubject.md) |  |
| [cRValidFrom](cRValidFrom.md) |  |
| [cRValidUntil](cRValidUntil.md) |  |
| [grantsComplianceCriteria](grantsComplianceCriteria.md) |  |
| [hasAssessedComplianceCriteria](hasAssessedComplianceCriteria.md) |  |
| [hasCategory](hasCategory.md) |  |
| [hasComplianceAssessmentBody](hasComplianceAssessmentBody.md) |  |
| [hasComplianceCertificateClaim](hasComplianceCertificateClaim.md) |  |
| [hasComplianceCertificateCredential](hasComplianceCertificateCredential.md) |  |
| [hasComplianceCertificationScheme](hasComplianceCertificationScheme.md) |  |
| [hasComplianceReference](hasComplianceReference.md) |  |
| [hasComplianceReferenceManager](hasComplianceReferenceManager.md) |  |
| [hasComplianceReferences](hasComplianceReferences.md) | List of Ids of ComplianceReferences (self-description) managed by this Compli... |
| [hasComplianceReferenceTitle](hasComplianceReferenceTitle.md) |  |
| [hasDescription](hasDescription.md) |  |
| [hasLevel](hasLevel.md) |  |
| [hasLocatedServiceOffering](hasLocatedServiceOffering.md) |  |
| [hasName](hasName.md) |  |
| [hasReferenceUrl](hasReferenceUrl.md) |  |
| [hasRequiredCriteria](hasRequiredCriteria.md) |  |
| [hasSha256](hasSha256.md) |  |
| [hasThirdPartyComplianceCertificateClaim](hasThirdPartyComplianceCertificateClaim.md) |  |
| [hasThirdPartyComplianceCredential](hasThirdPartyComplianceCredential.md) |  |
| [hasVersion](hasVersion.md) |  |
| [isHostedOn](isHostedOn.md) |  |
| [isImplementationOf](isImplementationOf.md) |  |
| [isValid](isValid.md) |  |
| [layer](layer.md) | Layer |
| [referenceType](referenceType.md) |  |


## Enumerations

| Enumeration | Description |
| --- | --- |


## Types

| Type | Description |
| --- | --- |
| [Boolean](Boolean.md) | A binary (true or false) value |
| [Curie](Curie.md) | a compact URI |
| [Date](Date.md) | a date (year, month and day) in an idealized calendar |
| [DateOrDatetime](DateOrDatetime.md) | Either a date or a datetime |
| [Datetime](Datetime.md) | The combination of a date and time |
| [Decimal](Decimal.md) | A real number with arbitrary precision that conforms to the xsd:decimal speci... |
| [Double](Double.md) | A real number that conforms to the xsd:double specification |
| [Float](Float.md) | A real number that conforms to the xsd:float specification |
| [Integer](Integer.md) | An integer |
| [Jsonpath](Jsonpath.md) | A string encoding a JSON Path |
| [Jsonpointer](Jsonpointer.md) | A string encoding a JSON Pointer |
| [Ncname](Ncname.md) | Prefix part of CURIE |
| [Nodeidentifier](Nodeidentifier.md) | A URI, CURIE or BNODE that represents a node in a model |
| [Objectidentifier](Objectidentifier.md) | A URI or CURIE that represents an object in the model |
| [Sparqlpath](Sparqlpath.md) | A string encoding a SPARQL Property Path |
| [String](String.md) | A character string |
| [Time](Time.md) | A time object represents a (local) time of day, independent of any particular... |
| [Uri](Uri.md) | a complete URI |
| [Uriorcurie](Uriorcurie.md) | a URI or a CURIE |


## Subsets

| Subset | Description |
| --- | --- |
