# Class: ComplianceCertificateClaim



URI: [abc-conformity:ComplianceCertificateClaim](https://$BASE_URL$/abc-conformity#ComplianceCertificateClaim)




```mermaid
 classDiagram
    class ComplianceCertificateClaim
      ComplianceCertificateClaim <|-- ThirdPartyComplianceCertificateClaim
      
      ComplianceCertificateClaim : hasComplianceCertificationScheme
        
          ComplianceCertificateClaim --|> ComplianceCertificationScheme : hasComplianceCertificationScheme
        
      ComplianceCertificateClaim : hasLocatedServiceOffering
        
          ComplianceCertificateClaim --|> LocatedServiceOffering : hasLocatedServiceOffering
        
      
```





## Inheritance
* **ComplianceCertificateClaim**
    * [ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasComplianceCertificationScheme](hasComplianceCertificationScheme.md) | 0..1 <br/> [ComplianceCertificationScheme](ComplianceCertificationScheme.md) | Id of the Compliance Certification Scheme (self-description) involved in the ... | direct |
| [hasLocatedServiceOffering](hasLocatedServiceOffering.md) | 1..1 <br/> [LocatedServiceOffering](LocatedServiceOffering.md) | Id of the Service Offering (self-description) certified for the compliance | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [LocatedServiceOffering](LocatedServiceOffering.md) | [hasComplianceCertificateClaim](hasComplianceCertificateClaim.md) | range | [ComplianceCertificateClaim](ComplianceCertificateClaim.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | abc-conformity:ComplianceCertificateClaim |
| native | https://$BASE_URL$/abc-conformity/:ComplianceCertificateClaim |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ComplianceCertificateClaim
from_schema: https://$BASE_URL$/abc-conformity
slots:
- hasComplianceCertificationScheme
- hasLocatedServiceOffering
slot_usage:
  hasComplianceCertificationScheme:
    name: hasComplianceCertificationScheme
    description: Id of the Compliance Certification Scheme (self-description) involved
      in the certification
    domain_of:
    - ComplianceReference
    - ComplianceCertificateClaim
  hasLocatedServiceOffering:
    name: hasLocatedServiceOffering
    description: Id of the Service Offering (self-description) certified for the compliance
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
    - ComplianceCertificateClaim
    required: true
class_uri: abc-conformity:ComplianceCertificateClaim

```
</details>

### Induced

<details>
```yaml
name: ComplianceCertificateClaim
from_schema: https://$BASE_URL$/abc-conformity
slot_usage:
  hasComplianceCertificationScheme:
    name: hasComplianceCertificationScheme
    description: Id of the Compliance Certification Scheme (self-description) involved
      in the certification
    domain_of:
    - ComplianceReference
    - ComplianceCertificateClaim
  hasLocatedServiceOffering:
    name: hasLocatedServiceOffering
    description: Id of the Service Offering (self-description) certified for the compliance
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
    - ComplianceCertificateClaim
    required: true
attributes:
  hasComplianceCertificationScheme:
    name: hasComplianceCertificationScheme
    description: Id of the Compliance Certification Scheme (self-description) involved
      in the certification
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasComplianceCertificationScheme
    alias: hasComplianceCertificationScheme
    owner: ComplianceCertificateClaim
    domain_of:
    - ComplianceReference
    - ComplianceCertificateClaim
    range: ComplianceCertificationScheme
  hasLocatedServiceOffering:
    name: hasLocatedServiceOffering
    description: Id of the Service Offering (self-description) certified for the compliance
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasServiceOffering
    alias: hasLocatedServiceOffering
    owner: ComplianceCertificateClaim
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
    - ComplianceCertificateClaim
    range: LocatedServiceOffering
    required: true
class_uri: abc-conformity:ComplianceCertificateClaim

```
</details>