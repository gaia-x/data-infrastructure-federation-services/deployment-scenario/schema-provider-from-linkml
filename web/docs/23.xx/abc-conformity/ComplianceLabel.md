# Class: ComplianceLabel



URI: [abc-conformity:ComplianceLabel](https://$BASE_URL$/abc-conformity#ComplianceLabel)




```mermaid
 classDiagram
    class ComplianceLabel
      ComplianceLabel : hasDescription
        
      ComplianceLabel : hasLevel
        
      ComplianceLabel : hasName
        
      ComplianceLabel : hasRequiredCriteria
        
          ComplianceLabel --|> ComplianceCriterion : hasRequiredCriteria
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasName](hasName.md) | 1..1 <br/> [String](String.md) | Name of the GAIA-X Label | direct |
| [hasDescription](hasDescription.md) | 0..1 <br/> [String](String.md) | A description in natural language of the GAIA-X Label as defined in TF docume... | direct |
| [hasLevel](hasLevel.md) | 1..1 <br/> [Integer](Integer.md) | A description in natural language of the GAIA-X Label Level as defined in TF ... | direct |
| [hasRequiredCriteria](hasRequiredCriteria.md) | 1..* <br/> [ComplianceCriterion](ComplianceCriterion.md) | List of required compliance criteria IDs | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | abc-conformity:ComplianceLabel |
| native | https://$BASE_URL$/abc-conformity/:ComplianceLabel |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ComplianceLabel
from_schema: https://$BASE_URL$/abc-conformity
slots:
- hasName
- hasDescription
- hasLevel
- hasRequiredCriteria
slot_usage:
  hasName:
    name: hasName
    description: Name of the GAIA-X Label
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the GAIA-X Label as defined
      in TF document
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
  hasLevel:
    name: hasLevel
    description: A description in natural language of the GAIA-X Label Level as defined
      in TF document
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    required: true
  hasRequiredCriteria:
    name: hasRequiredCriteria
    description: List of required compliance criteria IDs
    multivalued: true
    domain_of:
    - ComplianceLabel
    required: true
class_uri: abc-conformity:ComplianceLabel

```
</details>

### Induced

<details>
```yaml
name: ComplianceLabel
from_schema: https://$BASE_URL$/abc-conformity
slot_usage:
  hasName:
    name: hasName
    description: Name of the GAIA-X Label
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the GAIA-X Label as defined
      in TF document
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
  hasLevel:
    name: hasLevel
    description: A description in natural language of the GAIA-X Label Level as defined
      in TF document
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    required: true
  hasRequiredCriteria:
    name: hasRequiredCriteria
    description: List of required compliance criteria IDs
    multivalued: true
    domain_of:
    - ComplianceLabel
    required: true
attributes:
  hasName:
    name: hasName
    description: Name of the GAIA-X Label
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasName
    alias: hasName
    owner: ComplianceLabel
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    range: string
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the GAIA-X Label as defined
      in TF document
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasDescription
    alias: hasDescription
    owner: ComplianceLabel
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
    range: string
  hasLevel:
    name: hasLevel
    description: A description in natural language of the GAIA-X Label Level as defined
      in TF document
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasLevel
    alias: hasLevel
    owner: ComplianceLabel
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    range: integer
    required: true
  hasRequiredCriteria:
    name: hasRequiredCriteria
    description: List of required compliance criteria IDs
    from_schema: https://$BASE_URL$/abc-conformity
    rank: 1000
    slot_uri: abc-conformity:hasRequiredCriteria
    multivalued: true
    alias: hasRequiredCriteria
    owner: ComplianceLabel
    domain_of:
    - ComplianceLabel
    range: ComplianceCriterion
    required: true
class_uri: abc-conformity:ComplianceLabel

```
</details>