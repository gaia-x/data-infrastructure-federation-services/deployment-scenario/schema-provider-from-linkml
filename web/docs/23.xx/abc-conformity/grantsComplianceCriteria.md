# Slot: grantsComplianceCriteria

URI: [abc-conformity:grantsComplianceCriteria](https://$BASE_URL$/abc-conformity#grantsComplianceCriteria)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCertificationScheme](ComplianceCertificationScheme.md) |  |  yes  |
[ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) |  |  no  |







## Properties

* Range: [ComplianceCriterion](ComplianceCriterion.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: grantsComplianceCriteria
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:grantsComplianceCriteria
alias: grantsComplianceCriteria
domain_of:
- ComplianceCertificationScheme
range: ComplianceCriterion

```
</details>