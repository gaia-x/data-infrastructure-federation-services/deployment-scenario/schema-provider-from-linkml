# Slot: hasSha256

URI: [abc-conformity:hasSha256](https://$BASE_URL$/abc-conformity#hasSha256)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasSha256
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasSha256
alias: hasSha256
domain_of:
- ComplianceReference
range: string

```
</details>