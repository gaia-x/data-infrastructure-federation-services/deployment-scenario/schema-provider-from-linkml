# Slot: layer


_Layer_



URI: [abc-conformity:layer](https://$BASE_URL$/abc-conformity#layer)



<!-- no inheritance hierarchy -->







## Properties

* Range: [String](String.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: layer
description: Layer
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:layer
multivalued: true
alias: layer
range: string

```
</details>