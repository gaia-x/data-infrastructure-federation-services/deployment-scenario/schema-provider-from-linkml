# Slot: canBeSelfAssessed

URI: [abc-conformity:canBeSelfAssessed](https://$BASE_URL$/abc-conformity#canBeSelfAssessed)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCriterion](ComplianceCriterion.md) |  |  yes  |







## Properties

* Range: [Boolean](Boolean.md)






## Examples

| Value |
| --- |
| True |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: canBeSelfAssessed
examples:
- value: 'True'
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:canBeSelfAssessed
alias: canBeSelfAssessed
domain_of:
- ComplianceCriterion
range: boolean

```
</details>