# Slot: isImplementationOf

URI: [abc-conformity:isImplementationOf](https://$BASE_URL$/abc-conformity#isImplementationOf)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LocatedServiceOffering](LocatedServiceOffering.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: isImplementationOf
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:isImplementationOf
alias: isImplementationOf
domain_of:
- LocatedServiceOffering
range: string

```
</details>