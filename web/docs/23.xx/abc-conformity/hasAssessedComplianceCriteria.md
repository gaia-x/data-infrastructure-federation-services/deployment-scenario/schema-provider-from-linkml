# Slot: hasAssessedComplianceCriteria

URI: [abc-conformity:hasAssessedComplianceCriteria](https://$BASE_URL$/abc-conformity#hasAssessedComplianceCriteria)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) |  |  yes  |







## Properties

* Range: [ComplianceCriterion](ComplianceCriterion.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasAssessedComplianceCriteria
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasAssessedComplianceCriteria
alias: hasAssessedComplianceCriteria
domain_of:
- SelfAssessedComplianceCriteriaClaim
range: ComplianceCriterion

```
</details>