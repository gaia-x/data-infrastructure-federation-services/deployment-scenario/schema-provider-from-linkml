# Slot: hasComplianceCertificateCredential

URI: [abc-conformity:hasComplianceCertificateCredential](https://$BASE_URL$/abc-conformity#hasComplianceCertificateCredential)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LocatedServiceOffering](LocatedServiceOffering.md) |  |  yes  |







## Properties

* Range: [ComplianceCertificateCredential](ComplianceCertificateCredential.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceCertificateCredential
from_schema: https://$BASE_URL$/abc-conformity
rank: 1000
slot_uri: abc-conformity:hasComplianceCertificateCredential
alias: hasComplianceCertificateCredential
domain_of:
- LocatedServiceOffering
range: ComplianceCertificateCredential

```
</details>