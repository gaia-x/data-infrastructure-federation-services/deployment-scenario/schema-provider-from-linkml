# Slot: vcard_title


_To specify the position or job of the object._



URI: [vcard:title](https://www.w3.org/2006/vcard/ns#title)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Organization](Organization.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: vcard_title
description: To specify the position or job of the object.
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: vcard:title
alias: vcard_title
domain_of:
- Organization
range: string

```
</details>