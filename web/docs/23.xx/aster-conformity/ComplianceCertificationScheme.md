# Class: ComplianceCertificationScheme



URI: [aster-conformity:ComplianceCertificationScheme](https://$BASE_URL$/aster-conformity#ComplianceCertificationScheme)




```mermaid
 classDiagram
    class ComplianceCertificationScheme
      ComplianceCertificationScheme <|-- ThirdPartyComplianceCertificationScheme
      
      ComplianceCertificationScheme : grantsComplianceCriteria
        
          ComplianceCertificationScheme --|> ComplianceCriterion : grantsComplianceCriteria
        
      ComplianceCertificationScheme : hasComplianceReference
        
          ComplianceCertificationScheme --|> ComplianceReference : hasComplianceReference
        
      
```





## Inheritance
* **ComplianceCertificationScheme**
    * [ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasComplianceReference](hasComplianceReference.md) | 1..1 <br/> [ComplianceReference](ComplianceReference.md) | Id of Compliance Reference (self-description) to be certified by any means de... | direct |
| [grantsComplianceCriteria](grantsComplianceCriteria.md) | 0..* <br/> [ComplianceCriterion](ComplianceCriterion.md) | List of Compliance Criteria granted by the scheme in case of certification | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ComplianceReference](ComplianceReference.md) | [hasComplianceCertificationScheme](hasComplianceCertificationScheme.md) | range | [ComplianceCertificationScheme](ComplianceCertificationScheme.md) |
| [ComplianceCertificateClaim](ComplianceCertificateClaim.md) | [hasComplianceCertificationScheme](hasComplianceCertificationScheme.md) | range | [ComplianceCertificationScheme](ComplianceCertificationScheme.md) |
| [ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) | [hasComplianceCertificationScheme](hasComplianceCertificationScheme.md) | range | [ComplianceCertificationScheme](ComplianceCertificationScheme.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:ComplianceCertificationScheme |
| native | https://$BASE_URL$/aster-conformity/:ComplianceCertificationScheme |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ComplianceCertificationScheme
from_schema: https://$BASE_URL$/aster-conformity
slots:
- hasComplianceReference
- grantsComplianceCriteria
slot_usage:
  hasComplianceReference:
    name: hasComplianceReference
    description: Id of Compliance Reference (self-description) to be certified by
      any means defined in the Certification Scheme
    domain_of:
    - ComplianceCertificationScheme
    required: true
  grantsComplianceCriteria:
    name: grantsComplianceCriteria
    description: List of Compliance Criteria granted by the scheme in case of certification
    multivalued: true
    domain_of:
    - ComplianceCertificationScheme
class_uri: aster-conformity:ComplianceCertificationScheme

```
</details>

### Induced

<details>
```yaml
name: ComplianceCertificationScheme
from_schema: https://$BASE_URL$/aster-conformity
slot_usage:
  hasComplianceReference:
    name: hasComplianceReference
    description: Id of Compliance Reference (self-description) to be certified by
      any means defined in the Certification Scheme
    domain_of:
    - ComplianceCertificationScheme
    required: true
  grantsComplianceCriteria:
    name: grantsComplianceCriteria
    description: List of Compliance Criteria granted by the scheme in case of certification
    multivalued: true
    domain_of:
    - ComplianceCertificationScheme
attributes:
  hasComplianceReference:
    name: hasComplianceReference
    description: Id of Compliance Reference (self-description) to be certified by
      any means defined in the Certification Scheme
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasComplianceReference
    alias: hasComplianceReference
    owner: ComplianceCertificationScheme
    domain_of:
    - ComplianceCertificationScheme
    range: ComplianceReference
    required: true
  grantsComplianceCriteria:
    name: grantsComplianceCriteria
    description: List of Compliance Criteria granted by the scheme in case of certification
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:grantsComplianceCriteria
    multivalued: true
    alias: grantsComplianceCriteria
    owner: ComplianceCertificationScheme
    domain_of:
    - ComplianceCertificationScheme
    range: ComplianceCriterion
class_uri: aster-conformity:ComplianceCertificationScheme

```
</details>