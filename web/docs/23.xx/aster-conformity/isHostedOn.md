# Slot: isHostedOn

URI: [aster-conformity:isHostedOn](https://$BASE_URL$/aster-conformity#isHostedOn)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LocatedServiceOffering](LocatedServiceOffering.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: isHostedOn
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:isHostedOn
alias: isHostedOn
domain_of:
- LocatedServiceOffering
range: string

```
</details>