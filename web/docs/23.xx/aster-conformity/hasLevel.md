# Slot: hasLevel

URI: [aster-conformity:hasLevel](https://$BASE_URL$/aster-conformity#hasLevel)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCriterion](ComplianceCriterion.md) |  |  yes  |
[ComplianceLabel](ComplianceLabel.md) |  |  yes  |







## Properties

* Range: [Integer](Integer.md)






## Examples

| Value |
| --- |
| 1 |
| 2 |
| 3 |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasLevel
examples:
- value: '1'
- value: '2'
- value: '3'
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasLevel
alias: hasLevel
domain_of:
- ComplianceCriterion
- ComplianceLabel
range: integer

```
</details>