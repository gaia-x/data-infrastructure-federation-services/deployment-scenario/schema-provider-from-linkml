# Slot: vc

URI: [aster-conformity:vc](https://$BASE_URL$/aster-conformity#vc)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[GrantedLabel](GrantedLabel.md) |  |  yes  |







## Properties

* Range: [LocatedServiceOffering](LocatedServiceOffering.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: vc
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:vc
alias: vc
domain_of:
- GrantedLabel
range: LocatedServiceOffering

```
</details>