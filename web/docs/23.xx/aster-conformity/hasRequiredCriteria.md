# Slot: hasRequiredCriteria

URI: [aster-conformity:hasRequiredCriteria](https://$BASE_URL$/aster-conformity#hasRequiredCriteria)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceLabel](ComplianceLabel.md) |  |  yes  |







## Properties

* Range: [ComplianceCriterion](ComplianceCriterion.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasRequiredCriteria
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasRequiredCriteria
alias: hasRequiredCriteria
domain_of:
- ComplianceLabel
range: ComplianceCriterion

```
</details>