# Class: ComplianceReference



URI: [aster-conformity:ComplianceReference](https://$BASE_URL$/aster-conformity#ComplianceReference)




```mermaid
 classDiagram
    class ComplianceReference
      ComplianceReference : cRValidFrom
        
      ComplianceReference : cRValidUntil
        
      ComplianceReference : hasComplianceCertificationScheme
        
          ComplianceReference --|> ComplianceCertificationScheme : hasComplianceCertificationScheme
        
      ComplianceReference : hasComplianceReferenceManager
        
          ComplianceReference --|> ComplianceReferenceManager : hasComplianceReferenceManager
        
      ComplianceReference : hasComplianceReferenceTitle
        
      ComplianceReference : hasDescription
        
      ComplianceReference : hasReferenceUrl
        
      ComplianceReference : hasSha256
        
      ComplianceReference : hasVersion
        
      ComplianceReference : referenceType
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasReferenceUrl](hasReferenceUrl.md) | 1..1 <br/> [String](String.md) | URI to reference the content of the compliance reference in a single PDF file | direct |
| [referenceType](referenceType.md) | 0..1 <br/> [String](String.md) | Type of Compliance Reference | direct |
| [hasSha256](hasSha256.md) | 1..1 <br/> [String](String.md) | SHA256 of pdf document referenced by hasReferenceUrl | direct |
| [hasComplianceReferenceTitle](hasComplianceReferenceTitle.md) | 1..1 <br/> [String](String.md) | Name of the Compliance Reference | direct |
| [hasDescription](hasDescription.md) | 0..1 <br/> [String](String.md) | A description in natural language | direct |
| [hasComplianceReferenceManager](hasComplianceReferenceManager.md) | 1..1 <br/> [ComplianceReferenceManager](ComplianceReferenceManager.md) | Id of Participant (self-description) in charge of managing this Compliance Re... | direct |
| [hasVersion](hasVersion.md) | 0..1 <br/> [String](String.md) | String designated the version of the reference document | direct |
| [cRValidFrom](cRValidFrom.md) | 0..1 <br/> [Datetime](Datetime.md) | Indicates the first date when the compliance reference goes into effect | direct |
| [cRValidUntil](cRValidUntil.md) | 0..1 <br/> [Datetime](Datetime.md) | Indicates the last date when the compliance reference is no more into effect | direct |
| [hasComplianceCertificationScheme](hasComplianceCertificationScheme.md) | 0..* <br/> [ComplianceCertificationScheme](ComplianceCertificationScheme.md) | List of schemes that grants certification | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ComplianceReferenceManager](ComplianceReferenceManager.md) | [hasComplianceReferences](hasComplianceReferences.md) | range | [ComplianceReference](ComplianceReference.md) |
| [ComplianceCertificationScheme](ComplianceCertificationScheme.md) | [hasComplianceReference](hasComplianceReference.md) | range | [ComplianceReference](ComplianceReference.md) |
| [ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) | [hasComplianceReference](hasComplianceReference.md) | range | [ComplianceReference](ComplianceReference.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:ComplianceReference |
| native | https://$BASE_URL$/aster-conformity/:ComplianceReference |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ComplianceReference
from_schema: https://$BASE_URL$/aster-conformity
slots:
- hasReferenceUrl
- referenceType
- hasSha256
- hasComplianceReferenceTitle
- hasDescription
- hasComplianceReferenceManager
- hasVersion
- cRValidFrom
- cRValidUntil
- hasComplianceCertificationScheme
slot_usage:
  hasReferenceUrl:
    name: hasReferenceUrl
    description: URI to reference the content of the compliance reference in a single
      PDF file
    domain_of:
    - ComplianceReference
    required: true
  referenceType:
    name: referenceType
    description: Type of Compliance Reference
    domain_of:
    - ComplianceReference
  hasSha256:
    name: hasSha256
    description: SHA256 of pdf document referenced by hasReferenceUrl.
    domain_of:
    - ComplianceReference
    required: true
  hasComplianceReferenceTitle:
    name: hasComplianceReferenceTitle
    description: Name of the Compliance Reference
    domain_of:
    - ComplianceReference
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
  hasComplianceReferenceManager:
    name: hasComplianceReferenceManager
    description: Id of Participant (self-description) in charge of managing this Compliance
      Reference
    domain_of:
    - ComplianceReference
    required: true
  hasVersion:
    name: hasVersion
    description: String designated the version of the reference document. Can be a
      date or a build number or X.YY.ZZ.....
    domain_of:
    - ComplianceReference
  cRValidFrom:
    name: cRValidFrom
    description: Indicates the first date when the compliance reference goes into
      effect
    domain_of:
    - ComplianceReference
  cRValidUntil:
    name: cRValidUntil
    description: Indicates the last date when the compliance reference is no more
      into effect
    domain_of:
    - ComplianceReference
  hasComplianceCertificationScheme:
    name: hasComplianceCertificationScheme
    description: List of schemes that grants certification. This list is managed by
      a reference manager.
    multivalued: true
    domain_of:
    - ComplianceReference
    - ComplianceCertificateClaim
class_uri: aster-conformity:ComplianceReference

```
</details>

### Induced

<details>
```yaml
name: ComplianceReference
from_schema: https://$BASE_URL$/aster-conformity
slot_usage:
  hasReferenceUrl:
    name: hasReferenceUrl
    description: URI to reference the content of the compliance reference in a single
      PDF file
    domain_of:
    - ComplianceReference
    required: true
  referenceType:
    name: referenceType
    description: Type of Compliance Reference
    domain_of:
    - ComplianceReference
  hasSha256:
    name: hasSha256
    description: SHA256 of pdf document referenced by hasReferenceUrl.
    domain_of:
    - ComplianceReference
    required: true
  hasComplianceReferenceTitle:
    name: hasComplianceReferenceTitle
    description: Name of the Compliance Reference
    domain_of:
    - ComplianceReference
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
  hasComplianceReferenceManager:
    name: hasComplianceReferenceManager
    description: Id of Participant (self-description) in charge of managing this Compliance
      Reference
    domain_of:
    - ComplianceReference
    required: true
  hasVersion:
    name: hasVersion
    description: String designated the version of the reference document. Can be a
      date or a build number or X.YY.ZZ.....
    domain_of:
    - ComplianceReference
  cRValidFrom:
    name: cRValidFrom
    description: Indicates the first date when the compliance reference goes into
      effect
    domain_of:
    - ComplianceReference
  cRValidUntil:
    name: cRValidUntil
    description: Indicates the last date when the compliance reference is no more
      into effect
    domain_of:
    - ComplianceReference
  hasComplianceCertificationScheme:
    name: hasComplianceCertificationScheme
    description: List of schemes that grants certification. This list is managed by
      a reference manager.
    multivalued: true
    domain_of:
    - ComplianceReference
    - ComplianceCertificateClaim
attributes:
  hasReferenceUrl:
    name: hasReferenceUrl
    description: URI to reference the content of the compliance reference in a single
      PDF file
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasReferenceUrl
    alias: hasReferenceUrl
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    range: string
    required: true
  referenceType:
    name: referenceType
    description: Type of Compliance Reference
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:referenceType
    alias: referenceType
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    range: string
  hasSha256:
    name: hasSha256
    description: SHA256 of pdf document referenced by hasReferenceUrl.
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasSha256
    alias: hasSha256
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    range: string
    required: true
  hasComplianceReferenceTitle:
    name: hasComplianceReferenceTitle
    description: Name of the Compliance Reference
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasComplianceReferenceTitle
    alias: hasComplianceReferenceTitle
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    range: string
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasDescription
    alias: hasDescription
    owner: ComplianceReference
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
    range: string
  hasComplianceReferenceManager:
    name: hasComplianceReferenceManager
    description: Id of Participant (self-description) in charge of managing this Compliance
      Reference
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasComplianceReferenceManager
    alias: hasComplianceReferenceManager
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    range: ComplianceReferenceManager
    required: true
  hasVersion:
    name: hasVersion
    description: String designated the version of the reference document. Can be a
      date or a build number or X.YY.ZZ.....
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasVersion
    alias: hasVersion
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    range: string
  cRValidFrom:
    name: cRValidFrom
    description: Indicates the first date when the compliance reference goes into
      effect
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:cRValidFrom
    alias: cRValidFrom
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    range: datetime
  cRValidUntil:
    name: cRValidUntil
    description: Indicates the last date when the compliance reference is no more
      into effect
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:cRValidUntil
    alias: cRValidUntil
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    range: datetime
  hasComplianceCertificationScheme:
    name: hasComplianceCertificationScheme
    description: List of schemes that grants certification. This list is managed by
      a reference manager.
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasComplianceCertificationScheme
    multivalued: true
    alias: hasComplianceCertificationScheme
    owner: ComplianceReference
    domain_of:
    - ComplianceReference
    - ComplianceCertificateClaim
    range: ComplianceCertificationScheme
class_uri: aster-conformity:ComplianceReference

```
</details>