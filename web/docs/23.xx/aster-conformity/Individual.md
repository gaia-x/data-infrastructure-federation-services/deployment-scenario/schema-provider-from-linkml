# Class: Individual



URI: [vcard:Individual](https://www.w3.org/2006/vcard/ns#Individual)




```mermaid
 classDiagram
    class Individual
      Individual : hasDid
        
      Individual : vcard_fn
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [vcard_fn](vcard_fn.md) | 1..1 <br/> [String](String.md) | The formatted text corresponding to the name of the object | direct |
| [hasDid](hasDid.md) | 1..1 <br/> [String](String.md) | Did to reference identity of the participant | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [Organization](Organization.md) | [vcard_hasMember](vcard_hasMember.md) | range | [Individual](Individual.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | vcard:Individual |
| native | https://$BASE_URL$/aster-conformity/:Individual |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Individual
from_schema: https://$BASE_URL$/aster-conformity
slots:
- vcard_fn
- hasDid
slot_usage:
  vcard_fn:
    name: vcard_fn
    domain_of:
    - Individual
    required: true
  hasDid:
    name: hasDid
    domain_of:
    - Individual
    required: true
class_uri: vcard:Individual

```
</details>

### Induced

<details>
```yaml
name: Individual
from_schema: https://$BASE_URL$/aster-conformity
slot_usage:
  vcard_fn:
    name: vcard_fn
    domain_of:
    - Individual
    required: true
  hasDid:
    name: hasDid
    domain_of:
    - Individual
    required: true
attributes:
  vcard_fn:
    name: vcard_fn
    description: The formatted text corresponding to the name of the object.
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: vcard:fn
    alias: vcard_fn
    owner: Individual
    domain_of:
    - Individual
    range: string
    required: true
  hasDid:
    name: hasDid
    description: Did to reference identity of the participant. did:web is expected.
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasDid
    alias: hasDid
    owner: Individual
    domain_of:
    - Individual
    range: string
    required: true
class_uri: vcard:Individual

```
</details>