# Slot: vcard_fn


_The formatted text corresponding to the name of the object._



URI: [vcard:fn](https://www.w3.org/2006/vcard/ns#fn)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Individual](Individual.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: vcard_fn
description: The formatted text corresponding to the name of the object.
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: vcard:fn
alias: vcard_fn
domain_of:
- Individual
range: string

```
</details>