# Slot: hasComplianceReferenceTitle

URI: [aster-conformity:hasComplianceReferenceTitle](https://$BASE_URL$/aster-conformity#hasComplianceReferenceTitle)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceReferenceTitle
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasComplianceReferenceTitle
alias: hasComplianceReferenceTitle
domain_of:
- ComplianceReference
range: string

```
</details>