# Slot: hasComplianceReference

URI: [aster-conformity:hasComplianceReference](https://$BASE_URL$/aster-conformity#hasComplianceReference)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCertificationScheme](ComplianceCertificationScheme.md) |  |  yes  |
[ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) |  |  no  |







## Properties

* Range: [ComplianceReference](ComplianceReference.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceReference
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasComplianceReference
alias: hasComplianceReference
domain_of:
- ComplianceCertificationScheme
range: ComplianceReference

```
</details>