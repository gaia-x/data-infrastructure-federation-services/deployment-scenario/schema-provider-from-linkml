# Slot: vcard_hasMember


_To include a member in the group this object represents._



URI: [vcard:hasMember](https://www.w3.org/2006/vcard/ns#hasMember)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Organization](Organization.md) |  |  yes  |







## Properties

* Range: [Individual](Individual.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: vcard_hasMember
description: To include a member in the group this object represents.
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: vcard:hasMember
alias: vcard_hasMember
domain_of:
- Organization
range: Individual

```
</details>