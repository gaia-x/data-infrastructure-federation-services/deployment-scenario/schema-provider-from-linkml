# Class: ComplianceCriterion



URI: [aster-conformity:ComplianceCriterion](https://$BASE_URL$/aster-conformity#ComplianceCriterion)




```mermaid
 classDiagram
    class ComplianceCriterion
      ComplianceCriterion : canBeSelfAssessed
        
      ComplianceCriterion : hasCategory
        
      ComplianceCriterion : hasDescription
        
      ComplianceCriterion : hasLevel
        
      ComplianceCriterion : hasName
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasName](hasName.md) | 1..1 <br/> [String](String.md) | Name of the GAIA-X Criterion | direct |
| [hasDescription](hasDescription.md) | 0..1 <br/> [String](String.md) | A description in natural language of the GAIA-X Criterion as defined in TF do... | direct |
| [hasLevel](hasLevel.md) | 1..1 <br/> [Integer](Integer.md) | A description in natural language of the GAIA-X Criterion as defined in TF do... | direct |
| [hasCategory](hasCategory.md) | 1..1 <br/> [String](String.md) | The category of this criterion | direct |
| [canBeSelfAssessed](canBeSelfAssessed.md) | 1..1 <br/> [Boolean](Boolean.md) | Indicate if this criterion be self assessed | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ComplianceLabel](ComplianceLabel.md) | [hasRequiredCriteria](hasRequiredCriteria.md) | range | [ComplianceCriterion](ComplianceCriterion.md) |
| [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) | [hasAssessedComplianceCriteria](hasAssessedComplianceCriteria.md) | range | [ComplianceCriterion](ComplianceCriterion.md) |
| [ComplianceCertificationScheme](ComplianceCertificationScheme.md) | [grantsComplianceCriteria](grantsComplianceCriteria.md) | range | [ComplianceCriterion](ComplianceCriterion.md) |
| [ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) | [grantsComplianceCriteria](grantsComplianceCriteria.md) | range | [ComplianceCriterion](ComplianceCriterion.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:ComplianceCriterion |
| native | https://$BASE_URL$/aster-conformity/:ComplianceCriterion |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ComplianceCriterion
from_schema: https://$BASE_URL$/aster-conformity
slots:
- hasName
- hasDescription
- hasLevel
- hasCategory
- canBeSelfAssessed
slot_usage:
  hasName:
    name: hasName
    description: Name of the GAIA-X Criterion.
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the GAIA-X Criterion as defined
      in TF document
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
  hasLevel:
    name: hasLevel
    description: A description in natural language of the GAIA-X Criterion as defined
      in TF document
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    required: true
  hasCategory:
    name: hasCategory
    description: The category of this criterion
    domain_of:
    - ComplianceCriterion
    required: true
  canBeSelfAssessed:
    name: canBeSelfAssessed
    description: Indicate if this criterion be self assessed
    domain_of:
    - ComplianceCriterion
    required: true
class_uri: aster-conformity:ComplianceCriterion

```
</details>

### Induced

<details>
```yaml
name: ComplianceCriterion
from_schema: https://$BASE_URL$/aster-conformity
slot_usage:
  hasName:
    name: hasName
    description: Name of the GAIA-X Criterion.
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the GAIA-X Criterion as defined
      in TF document
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
  hasLevel:
    name: hasLevel
    description: A description in natural language of the GAIA-X Criterion as defined
      in TF document
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    required: true
  hasCategory:
    name: hasCategory
    description: The category of this criterion
    domain_of:
    - ComplianceCriterion
    required: true
  canBeSelfAssessed:
    name: canBeSelfAssessed
    description: Indicate if this criterion be self assessed
    domain_of:
    - ComplianceCriterion
    required: true
attributes:
  hasName:
    name: hasName
    description: Name of the GAIA-X Criterion.
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasName
    alias: hasName
    owner: ComplianceCriterion
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    range: string
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the GAIA-X Criterion as defined
      in TF document
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasDescription
    alias: hasDescription
    owner: ComplianceCriterion
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    - ComplianceReference
    range: string
  hasLevel:
    name: hasLevel
    description: A description in natural language of the GAIA-X Criterion as defined
      in TF document
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasLevel
    alias: hasLevel
    owner: ComplianceCriterion
    domain_of:
    - ComplianceCriterion
    - ComplianceLabel
    range: integer
    required: true
  hasCategory:
    name: hasCategory
    description: The category of this criterion
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasCategory
    alias: hasCategory
    owner: ComplianceCriterion
    domain_of:
    - ComplianceCriterion
    range: string
    required: true
  canBeSelfAssessed:
    name: canBeSelfAssessed
    description: Indicate if this criterion be self assessed
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:canBeSelfAssessed
    alias: canBeSelfAssessed
    owner: ComplianceCriterion
    domain_of:
    - ComplianceCriterion
    range: boolean
    required: true
class_uri: aster-conformity:ComplianceCriterion

```
</details>