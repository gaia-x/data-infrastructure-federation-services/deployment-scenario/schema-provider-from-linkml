# Slot: hasThirdPartyComplianceCertificateClaim

URI: [aster-conformity:hasThirdPartyComplianceCertificateClaim](https://$BASE_URL$/aster-conformity#hasThirdPartyComplianceCertificateClaim)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceAssessmentBody](ComplianceAssessmentBody.md) |  |  yes  |







## Properties

* Range: [ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasThirdPartyComplianceCertificateClaim
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasThirdPartyComplianceCertificateClaim
alias: hasThirdPartyComplianceCertificateClaim
domain_of:
- ComplianceAssessmentBody
range: ThirdPartyComplianceCertificateClaim

```
</details>