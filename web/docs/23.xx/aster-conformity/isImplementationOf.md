# Slot: isImplementationOf

URI: [aster-conformity:isImplementationOf](https://$BASE_URL$/aster-conformity#isImplementationOf)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LocatedServiceOffering](LocatedServiceOffering.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: isImplementationOf
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:isImplementationOf
alias: isImplementationOf
domain_of:
- LocatedServiceOffering
range: string

```
</details>