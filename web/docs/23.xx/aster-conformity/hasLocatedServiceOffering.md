# Slot: hasLocatedServiceOffering

URI: [aster-conformity:hasServiceOffering](https://$BASE_URL$/aster-conformity#hasServiceOffering)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) |  |  yes  |
[ComplianceCertificateClaim](ComplianceCertificateClaim.md) |  |  yes  |
[ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) |  |  no  |







## Properties

* Range: [LocatedServiceOffering](LocatedServiceOffering.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasLocatedServiceOffering
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasServiceOffering
alias: hasLocatedServiceOffering
domain_of:
- SelfAssessedComplianceCriteriaClaim
- ComplianceCertificateClaim
range: LocatedServiceOffering

```
</details>