# Slot: hasComplianceAssessmentBody

URI: [aster-conformity:hasComplianceAssessmentBody](https://$BASE_URL$/aster-conformity#hasComplianceAssessmentBody)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) |  |  yes  |
[ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) |  |  yes  |







## Properties

* Range: [ComplianceAssessmentBody](ComplianceAssessmentBody.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceAssessmentBody
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasComplianceAssessmentBody
alias: hasComplianceAssessmentBody
domain_of:
- ThirdPartyComplianceCertificationScheme
- ThirdPartyComplianceCertificateClaim
range: ComplianceAssessmentBody

```
</details>