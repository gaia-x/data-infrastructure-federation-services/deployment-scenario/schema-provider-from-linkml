# Slot: label

URI: [aster-conformity:label](https://$BASE_URL$/aster-conformity#label)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[GrantedLabel](GrantedLabel.md) |  |  yes  |







## Properties

* Range: [ComplianceLabel](ComplianceLabel.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: label
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:label
alias: label
domain_of:
- GrantedLabel
range: ComplianceLabel

```
</details>