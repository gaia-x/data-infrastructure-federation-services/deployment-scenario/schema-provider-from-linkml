# Slot: hasThirdPartyComplianceCredential

URI: [aster-conformity:hasThirdPartyComplianceCredential](https://$BASE_URL$/aster-conformity#hasThirdPartyComplianceCredential)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceAssessmentBody](ComplianceAssessmentBody.md) |  |  yes  |







## Properties

* Range: [ThirdPartyComplianceCertificateCredential](ThirdPartyComplianceCertificateCredential.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasThirdPartyComplianceCredential
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasThirdPartyComplianceCredential
alias: hasThirdPartyComplianceCredential
domain_of:
- ComplianceAssessmentBody
range: ThirdPartyComplianceCertificateCredential

```
</details>