# Slot: grantsComplianceCriteria

URI: [aster-conformity:grantsComplianceCriteria](https://$BASE_URL$/aster-conformity#grantsComplianceCriteria)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCertificationScheme](ComplianceCertificationScheme.md) |  |  yes  |
[ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md) |  |  no  |







## Properties

* Range: [ComplianceCriterion](ComplianceCriterion.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: grantsComplianceCriteria
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:grantsComplianceCriteria
alias: grantsComplianceCriteria
domain_of:
- ComplianceCertificationScheme
range: ComplianceCriterion

```
</details>