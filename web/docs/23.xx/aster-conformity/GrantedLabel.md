# Class: GrantedLabel



URI: [aster-conformity:GrantedLabel](https://$BASE_URL$/aster-conformity#GrantedLabel)




```mermaid
 classDiagram
    class GrantedLabel
      GrantedLabel : label
        
          GrantedLabel --|> ComplianceLabel : label
        
      GrantedLabel : vc
        
          GrantedLabel --|> LocatedServiceOffering : vc
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [vc](vc.md) | 1..1 <br/> [LocatedServiceOffering](LocatedServiceOffering.md) | VC granted by the label | direct |
| [label](label.md) | 0..1 <br/> [ComplianceLabel](ComplianceLabel.md) | Description of the granted label | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:GrantedLabel |
| native | https://$BASE_URL$/aster-conformity/:GrantedLabel |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: GrantedLabel
from_schema: https://$BASE_URL$/aster-conformity
slots:
- vc
- label
slot_usage:
  vc:
    name: vc
    description: VC granted by the label
    domain_of:
    - GrantedLabel
    required: true
  label:
    name: label
    description: Description of the granted label
    domain_of:
    - GrantedLabel
class_uri: aster-conformity:GrantedLabel

```
</details>

### Induced

<details>
```yaml
name: GrantedLabel
from_schema: https://$BASE_URL$/aster-conformity
slot_usage:
  vc:
    name: vc
    description: VC granted by the label
    domain_of:
    - GrantedLabel
    required: true
  label:
    name: label
    description: Description of the granted label
    domain_of:
    - GrantedLabel
attributes:
  vc:
    name: vc
    description: VC granted by the label
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:vc
    alias: vc
    owner: GrantedLabel
    domain_of:
    - GrantedLabel
    range: LocatedServiceOffering
    required: true
  label:
    name: label
    description: Description of the granted label
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:label
    alias: label
    owner: GrantedLabel
    domain_of:
    - GrantedLabel
    range: ComplianceLabel
class_uri: aster-conformity:GrantedLabel

```
</details>