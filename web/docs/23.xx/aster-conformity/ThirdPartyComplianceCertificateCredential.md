# Class: ThirdPartyComplianceCertificateCredential



URI: [aster-conformity:ThirdPartyComplianceCertificateCredential](https://$BASE_URL$/aster-conformity#ThirdPartyComplianceCertificateCredential)




```mermaid
 classDiagram
    class ThirdPartyComplianceCertificateCredential
      ComplianceCertificateCredential <|-- ThirdPartyComplianceCertificateCredential
      
      ThirdPartyComplianceCertificateCredential : credentialSubject
        
          ThirdPartyComplianceCertificateCredential --|> SelfAssessedComplianceCriteriaClaim : credentialSubject
        
      ThirdPartyComplianceCertificateCredential : isValid
        
      
```





## Inheritance
* [ComplianceCertificateCredential](ComplianceCertificateCredential.md)
    * **ThirdPartyComplianceCertificateCredential**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [credentialSubject](credentialSubject.md) | 1..1 <br/> [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) | Id of the claim to be signed in a verifiable credential build with all the in... | [ComplianceCertificateCredential](ComplianceCertificateCredential.md) |
| [isValid](isValid.md) | 1..1 <br/> [Boolean](Boolean.md) | Indicate if the associated claim is valid or not | [ComplianceCertificateCredential](ComplianceCertificateCredential.md) |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ComplianceAssessmentBody](ComplianceAssessmentBody.md) | [hasThirdPartyComplianceCredential](hasThirdPartyComplianceCredential.md) | range | [ThirdPartyComplianceCertificateCredential](ThirdPartyComplianceCertificateCredential.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:ThirdPartyComplianceCertificateCredential |
| native | https://$BASE_URL$/aster-conformity/:ThirdPartyComplianceCertificateCredential |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ThirdPartyComplianceCertificateCredential
from_schema: https://$BASE_URL$/aster-conformity
is_a: ComplianceCertificateCredential
slot_usage:
  credentialSubject:
    name: credentialSubject
    description: Id of the claim to be signed in a verifiable credential build with
      all the information that are bound in the claim. In case of third party credential
      the id is the id of the third party
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
class_uri: aster-conformity:ThirdPartyComplianceCertificateCredential

```
</details>

### Induced

<details>
```yaml
name: ThirdPartyComplianceCertificateCredential
from_schema: https://$BASE_URL$/aster-conformity
is_a: ComplianceCertificateCredential
slot_usage:
  credentialSubject:
    name: credentialSubject
    description: Id of the claim to be signed in a verifiable credential build with
      all the information that are bound in the claim. In case of third party credential
      the id is the id of the third party
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
attributes:
  credentialSubject:
    name: credentialSubject
    description: Id of the claim to be signed in a verifiable credential build with
      all the information that are bound in the claim. In case of third party credential
      the id is the id of the third party
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:credentialSubject
    alias: credentialSubject
    owner: ThirdPartyComplianceCertificateCredential
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    range: SelfAssessedComplianceCriteriaClaim
    required: true
  isValid:
    name: isValid
    description: Indicate if the associated claim is valid or not
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:isValid
    alias: isValid
    owner: ThirdPartyComplianceCertificateCredential
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    range: boolean
    required: true
class_uri: aster-conformity:ThirdPartyComplianceCertificateCredential

```
</details>