# Class: ComplianceCertificateCredential



URI: [aster-conformity:ComplianceCertificateCredential](https://$BASE_URL$/aster-conformity#ComplianceCertificateCredential)




```mermaid
 classDiagram
    class ComplianceCertificateCredential
      ComplianceCertificateCredential <|-- ThirdPartyComplianceCertificateCredential
      
      ComplianceCertificateCredential : credentialSubject
        
          ComplianceCertificateCredential --|> SelfAssessedComplianceCriteriaClaim : credentialSubject
        
      ComplianceCertificateCredential : isValid
        
      
```





## Inheritance
* **ComplianceCertificateCredential**
    * [ThirdPartyComplianceCertificateCredential](ThirdPartyComplianceCertificateCredential.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [credentialSubject](credentialSubject.md) | 1..1 <br/> [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) | Id of the claim to be signed in a verifiable credential build with all the in... | direct |
| [isValid](isValid.md) | 1..1 <br/> [Boolean](Boolean.md) | Indicate if the associated claim is valid or not | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [LocatedServiceOffering](LocatedServiceOffering.md) | [hasComplianceCertificateCredential](hasComplianceCertificateCredential.md) | range | [ComplianceCertificateCredential](ComplianceCertificateCredential.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:ComplianceCertificateCredential |
| native | https://$BASE_URL$/aster-conformity/:ComplianceCertificateCredential |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ComplianceCertificateCredential
from_schema: https://$BASE_URL$/aster-conformity
slots:
- credentialSubject
- isValid
slot_usage:
  credentialSubject:
    name: credentialSubject
    description: Id of the claim to be signed in a verifiable credential build with
      all the information that are bound in the claim. In case of third party credential
      the id is the id of the third party
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    required: true
  isValid:
    name: isValid
    description: Indicate if the associated claim is valid or not
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    required: true
class_uri: aster-conformity:ComplianceCertificateCredential

```
</details>

### Induced

<details>
```yaml
name: ComplianceCertificateCredential
from_schema: https://$BASE_URL$/aster-conformity
slot_usage:
  credentialSubject:
    name: credentialSubject
    description: Id of the claim to be signed in a verifiable credential build with
      all the information that are bound in the claim. In case of third party credential
      the id is the id of the third party
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    required: true
  isValid:
    name: isValid
    description: Indicate if the associated claim is valid or not
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    required: true
attributes:
  credentialSubject:
    name: credentialSubject
    description: Id of the claim to be signed in a verifiable credential build with
      all the information that are bound in the claim. In case of third party credential
      the id is the id of the third party
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:credentialSubject
    alias: credentialSubject
    owner: ComplianceCertificateCredential
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    range: SelfAssessedComplianceCriteriaClaim
    required: true
  isValid:
    name: isValid
    description: Indicate if the associated claim is valid or not
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:isValid
    alias: isValid
    owner: ComplianceCertificateCredential
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    range: boolean
    required: true
class_uri: aster-conformity:ComplianceCertificateCredential

```
</details>