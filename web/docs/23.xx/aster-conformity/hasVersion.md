# Slot: hasVersion

URI: [aster-conformity:hasVersion](https://$BASE_URL$/aster-conformity#hasVersion)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasVersion
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasVersion
alias: hasVersion
domain_of:
- ComplianceReference
range: string

```
</details>