# Slot: hasDid


_Did to reference identity of the participant. did:web is expected._



URI: [aster-conformity:hasDid](https://$BASE_URL$/aster-conformity#hasDid)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Individual](Individual.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasDid
description: Did to reference identity of the participant. did:web is expected.
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasDid
alias: hasDid
domain_of:
- Individual
range: string

```
</details>