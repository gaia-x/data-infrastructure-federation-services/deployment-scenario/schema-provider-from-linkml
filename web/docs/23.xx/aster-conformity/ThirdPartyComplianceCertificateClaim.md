# Class: ThirdPartyComplianceCertificateClaim



URI: [aster-conformity:ThirdPartyComplianceCertificateClaim](https://$BASE_URL$/aster-conformity#ThirdPartyComplianceCertificateClaim)




```mermaid
 classDiagram
    class ThirdPartyComplianceCertificateClaim
      ComplianceCertificateClaim <|-- ThirdPartyComplianceCertificateClaim
      
      ThirdPartyComplianceCertificateClaim : hasComplianceAssessmentBody
        
          ThirdPartyComplianceCertificateClaim --|> ComplianceAssessmentBody : hasComplianceAssessmentBody
        
      ThirdPartyComplianceCertificateClaim : hasComplianceCertificationScheme
        
          ThirdPartyComplianceCertificateClaim --|> ComplianceCertificationScheme : hasComplianceCertificationScheme
        
      ThirdPartyComplianceCertificateClaim : hasLocatedServiceOffering
        
          ThirdPartyComplianceCertificateClaim --|> LocatedServiceOffering : hasLocatedServiceOffering
        
      
```





## Inheritance
* [ComplianceCertificateClaim](ComplianceCertificateClaim.md)
    * **ThirdPartyComplianceCertificateClaim**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasComplianceAssessmentBody](hasComplianceAssessmentBody.md) | 1..* <br/> [ComplianceAssessmentBody](ComplianceAssessmentBody.md) | Id of the Participant (self-description) endorsed having a Compliance Assessm... | direct |
| [hasComplianceCertificationScheme](hasComplianceCertificationScheme.md) | 0..1 <br/> [ComplianceCertificationScheme](ComplianceCertificationScheme.md) | Id of the Compliance Certification Scheme (self-description) involved in the ... | [ComplianceCertificateClaim](ComplianceCertificateClaim.md) |
| [hasLocatedServiceOffering](hasLocatedServiceOffering.md) | 1..1 <br/> [LocatedServiceOffering](LocatedServiceOffering.md) | Id of the Service Offering (self-description) certified for the compliance | [ComplianceCertificateClaim](ComplianceCertificateClaim.md) |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ComplianceAssessmentBody](ComplianceAssessmentBody.md) | [hasThirdPartyComplianceCertificateClaim](hasThirdPartyComplianceCertificateClaim.md) | range | [ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:ThirdPartyComplianceCertificateClaim |
| native | https://$BASE_URL$/aster-conformity/:ThirdPartyComplianceCertificateClaim |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ThirdPartyComplianceCertificateClaim
from_schema: https://$BASE_URL$/aster-conformity
is_a: ComplianceCertificateClaim
slots:
- hasComplianceAssessmentBody
slot_usage:
  hasComplianceAssessmentBody:
    name: hasComplianceAssessmentBody
    description: Id of the Participant (self-description) endorsed having a Compliance
      Assessment Body role that claims the compliance
    multivalued: true
    domain_of:
    - ThirdPartyComplianceCertificationScheme
    - ThirdPartyComplianceCertificateClaim
    required: true
class_uri: aster-conformity:ThirdPartyComplianceCertificateClaim

```
</details>

### Induced

<details>
```yaml
name: ThirdPartyComplianceCertificateClaim
from_schema: https://$BASE_URL$/aster-conformity
is_a: ComplianceCertificateClaim
slot_usage:
  hasComplianceAssessmentBody:
    name: hasComplianceAssessmentBody
    description: Id of the Participant (self-description) endorsed having a Compliance
      Assessment Body role that claims the compliance
    multivalued: true
    domain_of:
    - ThirdPartyComplianceCertificationScheme
    - ThirdPartyComplianceCertificateClaim
    required: true
attributes:
  hasComplianceAssessmentBody:
    name: hasComplianceAssessmentBody
    description: Id of the Participant (self-description) endorsed having a Compliance
      Assessment Body role that claims the compliance
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasComplianceAssessmentBody
    multivalued: true
    alias: hasComplianceAssessmentBody
    owner: ThirdPartyComplianceCertificateClaim
    domain_of:
    - ThirdPartyComplianceCertificationScheme
    - ThirdPartyComplianceCertificateClaim
    range: ComplianceAssessmentBody
    required: true
  hasComplianceCertificationScheme:
    name: hasComplianceCertificationScheme
    description: Id of the Compliance Certification Scheme (self-description) involved
      in the certification
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasComplianceCertificationScheme
    alias: hasComplianceCertificationScheme
    owner: ThirdPartyComplianceCertificateClaim
    domain_of:
    - ComplianceReference
    - ComplianceCertificateClaim
    range: ComplianceCertificationScheme
  hasLocatedServiceOffering:
    name: hasLocatedServiceOffering
    description: Id of the Service Offering (self-description) certified for the compliance
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasServiceOffering
    alias: hasLocatedServiceOffering
    owner: ThirdPartyComplianceCertificateClaim
    domain_of:
    - SelfAssessedComplianceCriteriaClaim
    - ComplianceCertificateClaim
    range: LocatedServiceOffering
    required: true
class_uri: aster-conformity:ThirdPartyComplianceCertificateClaim

```
</details>