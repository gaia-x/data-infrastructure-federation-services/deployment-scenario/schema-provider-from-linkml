# Slot: canCertifyThirdPartyComplianceCertificationScheme

URI: [aster-conformity:canCertifyThirdPartyComplianceCertificationScheme](https://$BASE_URL$/aster-conformity#canCertifyThirdPartyComplianceCertificationScheme)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceAssessmentBody](ComplianceAssessmentBody.md) |  |  yes  |







## Properties

* Range: [ThirdPartyComplianceCertificationScheme](ThirdPartyComplianceCertificationScheme.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: canCertifyThirdPartyComplianceCertificationScheme
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:canCertifyThirdPartyComplianceCertificationScheme
alias: canCertifyThirdPartyComplianceCertificationScheme
domain_of:
- ComplianceAssessmentBody
range: ThirdPartyComplianceCertificationScheme

```
</details>