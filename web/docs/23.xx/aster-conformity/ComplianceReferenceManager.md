# Class: ComplianceReferenceManager



URI: [aster-conformity:ComplianceReferenceManager](https://$BASE_URL$/aster-conformity#ComplianceReferenceManager)




```mermaid
 classDiagram
    class ComplianceReferenceManager
      ComplianceReferenceManager : hasComplianceReferences
        
          ComplianceReferenceManager --|> ComplianceReference : hasComplianceReferences
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [hasComplianceReferences](hasComplianceReferences.md) | 1..* <br/> [ComplianceReference](ComplianceReference.md) | List of Ids of ComplianceReferences (self-description) managed by this Compli... | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ComplianceReference](ComplianceReference.md) | [hasComplianceReferenceManager](hasComplianceReferenceManager.md) | range | [ComplianceReferenceManager](ComplianceReferenceManager.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:ComplianceReferenceManager |
| native | https://$BASE_URL$/aster-conformity/:ComplianceReferenceManager |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ComplianceReferenceManager
from_schema: https://$BASE_URL$/aster-conformity
attributes:
  hasComplianceReferences:
    name: hasComplianceReferences
    description: List of Ids of ComplianceReferences (self-description) managed by
      this ComplianceReferenceManager
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasComplianceReferences
    multivalued: true
    range: ComplianceReference
    required: true
class_uri: aster-conformity:ComplianceReferenceManager

```
</details>

### Induced

<details>
```yaml
name: ComplianceReferenceManager
from_schema: https://$BASE_URL$/aster-conformity
attributes:
  hasComplianceReferences:
    name: hasComplianceReferences
    description: List of Ids of ComplianceReferences (self-description) managed by
      this ComplianceReferenceManager
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:hasComplianceReferences
    multivalued: true
    alias: hasComplianceReferences
    owner: ComplianceReferenceManager
    domain_of:
    - ComplianceReferenceManager
    range: ComplianceReference
    required: true
class_uri: aster-conformity:ComplianceReferenceManager

```
</details>