# Slot: hasCategory

URI: [aster-conformity:hasCategory](https://$BASE_URL$/aster-conformity#hasCategory)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCriterion](ComplianceCriterion.md) |  |  yes  |







## Properties

* Range: [String](String.md)






## Examples

| Value |
| --- |
| Data Protection |
| Transparency |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasCategory
examples:
- value: Data Protection
- value: Transparency
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasCategory
alias: hasCategory
domain_of:
- ComplianceCriterion
range: string

```
</details>