# Class: SelfAssessedComplianceCriteriaCredential



URI: [aster-conformity:SelfAssessedComplianceCriteriaCredential](https://$BASE_URL$/aster-conformity#SelfAssessedComplianceCriteriaCredential)




```mermaid
 classDiagram
    class SelfAssessedComplianceCriteriaCredential
      SelfAssessedComplianceCriteriaCredential : credentialSubject
        
          SelfAssessedComplianceCriteriaCredential --|> SelfAssessedComplianceCriteriaClaim : credentialSubject
        
      SelfAssessedComplianceCriteriaCredential : isValid
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [credentialSubject](credentialSubject.md) | 1..1 <br/> [SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) | Id of the Claim referencing the GAIA-X Criteria and the relevant LocatedServi... | direct |
| [isValid](isValid.md) | 1..1 <br/> [Boolean](Boolean.md) | Indicate if the associated claim is valid or not | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-conformity:SelfAssessedComplianceCriteriaCredential |
| native | https://$BASE_URL$/aster-conformity/:SelfAssessedComplianceCriteriaCredential |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: SelfAssessedComplianceCriteriaCredential
from_schema: https://$BASE_URL$/aster-conformity
slots:
- credentialSubject
- isValid
slot_usage:
  credentialSubject:
    name: credentialSubject
    description: Id of the Claim referencing the GAIA-X Criteria and the relevant
      LocatedServiceOffering
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    required: true
  isValid:
    name: isValid
    description: Indicate if the associated claim is valid or not
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    required: true
class_uri: aster-conformity:SelfAssessedComplianceCriteriaCredential

```
</details>

### Induced

<details>
```yaml
name: SelfAssessedComplianceCriteriaCredential
from_schema: https://$BASE_URL$/aster-conformity
slot_usage:
  credentialSubject:
    name: credentialSubject
    description: Id of the Claim referencing the GAIA-X Criteria and the relevant
      LocatedServiceOffering
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    required: true
  isValid:
    name: isValid
    description: Indicate if the associated claim is valid or not
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    required: true
attributes:
  credentialSubject:
    name: credentialSubject
    description: Id of the Claim referencing the GAIA-X Criteria and the relevant
      LocatedServiceOffering
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:credentialSubject
    alias: credentialSubject
    owner: SelfAssessedComplianceCriteriaCredential
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    range: SelfAssessedComplianceCriteriaClaim
    required: true
  isValid:
    name: isValid
    description: Indicate if the associated claim is valid or not
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: aster-conformity:isValid
    alias: isValid
    owner: SelfAssessedComplianceCriteriaCredential
    domain_of:
    - SelfAssessedComplianceCriteriaCredential
    - ComplianceCertificateCredential
    range: boolean
    required: true
class_uri: aster-conformity:SelfAssessedComplianceCriteriaCredential

```
</details>