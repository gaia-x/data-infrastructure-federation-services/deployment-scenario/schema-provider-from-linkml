# Slot: cRValidFrom

URI: [aster-conformity:cRValidFrom](https://$BASE_URL$/aster-conformity#cRValidFrom)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [Datetime](Datetime.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: cRValidFrom
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:cRValidFrom
alias: cRValidFrom
domain_of:
- ComplianceReference
range: datetime

```
</details>