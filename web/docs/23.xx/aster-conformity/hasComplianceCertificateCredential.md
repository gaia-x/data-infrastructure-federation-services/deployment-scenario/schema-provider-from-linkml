# Slot: hasComplianceCertificateCredential

URI: [aster-conformity:hasComplianceCertificateCredential](https://$BASE_URL$/aster-conformity#hasComplianceCertificateCredential)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LocatedServiceOffering](LocatedServiceOffering.md) |  |  yes  |







## Properties

* Range: [ComplianceCertificateCredential](ComplianceCertificateCredential.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceCertificateCredential
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasComplianceCertificateCredential
alias: hasComplianceCertificateCredential
domain_of:
- LocatedServiceOffering
range: ComplianceCertificateCredential

```
</details>