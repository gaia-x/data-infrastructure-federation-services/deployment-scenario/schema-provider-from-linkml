# Slot: hasAssessedComplianceCriteria

URI: [aster-conformity:hasAssessedComplianceCriteria](https://$BASE_URL$/aster-conformity#hasAssessedComplianceCriteria)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SelfAssessedComplianceCriteriaClaim](SelfAssessedComplianceCriteriaClaim.md) |  |  yes  |







## Properties

* Range: [ComplianceCriterion](ComplianceCriterion.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasAssessedComplianceCriteria
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasAssessedComplianceCriteria
alias: hasAssessedComplianceCriteria
domain_of:
- SelfAssessedComplianceCriteriaClaim
range: ComplianceCriterion

```
</details>