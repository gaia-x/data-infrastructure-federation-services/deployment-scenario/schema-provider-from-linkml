# Slot: hasName

URI: [aster-conformity:hasName](https://$BASE_URL$/aster-conformity#hasName)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceCriterion](ComplianceCriterion.md) |  |  yes  |
[ComplianceLabel](ComplianceLabel.md) |  |  yes  |







## Properties

* Range: [String](String.md)






## Examples

| Value |
| --- |
| GAIA-X C1/L1 |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasName
examples:
- value: GAIA-X C1/L1
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasName
alias: hasName
domain_of:
- ComplianceCriterion
- ComplianceLabel
range: string

```
</details>