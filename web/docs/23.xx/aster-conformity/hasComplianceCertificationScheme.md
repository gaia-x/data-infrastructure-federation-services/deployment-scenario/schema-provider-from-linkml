# Slot: hasComplianceCertificationScheme

URI: [aster-conformity:hasComplianceCertificationScheme](https://$BASE_URL$/aster-conformity#hasComplianceCertificationScheme)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |
[ComplianceCertificateClaim](ComplianceCertificateClaim.md) |  |  yes  |
[ThirdPartyComplianceCertificateClaim](ThirdPartyComplianceCertificateClaim.md) |  |  no  |







## Properties

* Range: [ComplianceCertificationScheme](ComplianceCertificationScheme.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceCertificationScheme
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasComplianceCertificationScheme
alias: hasComplianceCertificationScheme
domain_of:
- ComplianceReference
- ComplianceCertificateClaim
range: ComplianceCertificationScheme

```
</details>