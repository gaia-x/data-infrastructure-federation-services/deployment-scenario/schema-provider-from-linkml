# Slot: hasSha256

URI: [aster-conformity:hasSha256](https://$BASE_URL$/aster-conformity#hasSha256)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ComplianceReference](ComplianceReference.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasSha256
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasSha256
alias: hasSha256
domain_of:
- ComplianceReference
range: string

```
</details>