# Slot: hasComplianceCertificateClaim

URI: [aster-conformity:hasComplianceCertificateClaim](https://$BASE_URL$/aster-conformity#hasComplianceCertificateClaim)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LocatedServiceOffering](LocatedServiceOffering.md) |  |  yes  |







## Properties

* Range: [ComplianceCertificateClaim](ComplianceCertificateClaim.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: hasComplianceCertificateClaim
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
slot_uri: aster-conformity:hasComplianceCertificateClaim
alias: hasComplianceCertificateClaim
domain_of:
- LocatedServiceOffering
range: ComplianceCertificateClaim

```
</details>