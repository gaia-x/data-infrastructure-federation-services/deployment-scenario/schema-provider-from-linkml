# Class: Organization



URI: [vcard:Organization](https://www.w3.org/2006/vcard/ns#Organization)




```mermaid
 classDiagram
    class Organization
      Organization : vcard_hasMember
        
          Organization --|> Individual : vcard_hasMember
        
      Organization : vcard_title
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [vcard_title](vcard_title.md) | 1..1 <br/> [String](String.md) | To specify the position or job of the object | direct |
| [vcard_hasMember](vcard_hasMember.md) | 1..1 <br/> [Individual](Individual.md) | To include a member in the group this object represents | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | vcard:Organization |
| native | https://$BASE_URL$/aster-conformity/:Organization |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Organization
from_schema: https://$BASE_URL$/aster-conformity
slots:
- vcard_title
- vcard_hasMember
slot_usage:
  vcard_title:
    name: vcard_title
    domain_of:
    - Organization
    required: true
  vcard_hasMember:
    name: vcard_hasMember
    domain_of:
    - Organization
    required: true
class_uri: vcard:Organization

```
</details>

### Induced

<details>
```yaml
name: Organization
from_schema: https://$BASE_URL$/aster-conformity
slot_usage:
  vcard_title:
    name: vcard_title
    domain_of:
    - Organization
    required: true
  vcard_hasMember:
    name: vcard_hasMember
    domain_of:
    - Organization
    required: true
attributes:
  vcard_title:
    name: vcard_title
    description: To specify the position or job of the object.
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: vcard:title
    alias: vcard_title
    owner: Organization
    domain_of:
    - Organization
    range: string
    required: true
  vcard_hasMember:
    name: vcard_hasMember
    description: To include a member in the group this object represents.
    from_schema: https://$BASE_URL$/aster-conformity
    rank: 1000
    slot_uri: vcard:hasMember
    alias: vcard_hasMember
    owner: Organization
    domain_of:
    - Organization
    range: Individual
    required: true
class_uri: vcard:Organization

```
</details>