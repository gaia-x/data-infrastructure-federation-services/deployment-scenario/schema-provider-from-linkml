# Slot: hasReferenceUrl


_ReferenceURL_



URI: [https://$BASE_URL$/casco-conformity/:hasReferenceUrl](https://$BASE_URL$/casco-conformity/:hasReferenceUrl)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasReferenceUrl
description: ReferenceURL
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasReferenceUrl
domain_of:
- ConformityAssessmentScheme
range: string

```
</details>