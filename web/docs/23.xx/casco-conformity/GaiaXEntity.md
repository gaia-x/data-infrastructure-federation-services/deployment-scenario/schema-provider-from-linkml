# Class: GaiaXEntity


_Root class for Gaia-X entities._




* __NOTE__: this is an abstract class and should not be instantiated directly


URI: [https://$BASE_URL$/casco-conformity/:GaiaXEntity](https://$BASE_URL$/casco-conformity/:GaiaXEntity)




```mermaid
 classDiagram
    class GaiaXEntity
      GaiaXEntity <|-- Issuer
      GaiaXEntity <|-- Participant
      GaiaXEntity <|-- Resource
      GaiaXEntity <|-- ServiceOffering
      
      
```





## Inheritance
* **GaiaXEntity**
    * [Issuer](Issuer.md)
    * [Participant](Participant.md)
    * [Resource](Resource.md)
    * [ServiceOffering](ServiceOffering.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/casco-conformity/:GaiaXEntity |
| native | https://$BASE_URL$/casco-conformity/:GaiaXEntity |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: GaiaXEntity
description: Root class for Gaia-X entities.
from_schema: https://$BASE_URL$/casco-conformity
abstract: true

```
</details>

### Induced

<details>
```yaml
name: GaiaXEntity
description: Root class for Gaia-X entities.
from_schema: https://$BASE_URL$/casco-conformity
abstract: true

```
</details>