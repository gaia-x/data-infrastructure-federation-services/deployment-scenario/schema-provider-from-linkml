# Slot: hasConformityAssessmentSystemTitle

URI: [casco-conformity:hasConformityAssessmentSystemTitle](https://$BASE_URL$/casco-conformity#hasConformityAssessmentSystemTitle)



<!-- no inheritance hierarchy -->







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasConformityAssessmentSystemTitle
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:hasConformityAssessmentSystemTitle
alias: hasConformityAssessmentSystemTitle
range: string

```
</details>