# Slot: isRecognized


_TODO_



URI: [https://$BASE_URL$/aster-conformity/:isRecognized](https://$BASE_URL$/aster-conformity/:isRecognized)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Requirement](Requirement.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-conformity




## LinkML Source

<details>
```yaml
name: isRecognized
description: TODO
from_schema: https://$BASE_URL$/aster-conformity
rank: 1000
alias: isRecognized
domain_of:
- Requirement
range: string

```
</details>