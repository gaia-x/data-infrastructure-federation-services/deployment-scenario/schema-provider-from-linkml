# Slot: hasVersion

URI: [casco-conformity:hasVersion](https://$BASE_URL$/casco-conformity#hasVersion)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasVersion
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:hasVersion
alias: hasVersion
domain_of:
- ConformityAssessmentScheme
range: string

```
</details>