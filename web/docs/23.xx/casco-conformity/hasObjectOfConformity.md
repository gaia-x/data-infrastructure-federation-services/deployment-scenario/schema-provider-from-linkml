# Slot: hasObjectOfConformity


_IRI type of Object targeted by the Requirement._



URI: [https://$BASE_URL$/casco-conformity/:hasObjectOfConformity](https://$BASE_URL$/casco-conformity/:hasObjectOfConformity)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Requirement](Requirement.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasObjectOfConformity
description: IRI type of Object targeted by the Requirement.
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasObjectOfConformity
domain_of:
- Requirement
range: string

```
</details>