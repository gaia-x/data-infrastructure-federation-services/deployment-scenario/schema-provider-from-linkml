# Slot: hasRequirements


_requirements_



URI: [https://$BASE_URL$/casco-conformity/:hasRequirements](https://$BASE_URL$/casco-conformity/:hasRequirements)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasRequirements
description: requirements
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasRequirements
domain_of:
- ConformityAssessmentScheme
range: string

```
</details>