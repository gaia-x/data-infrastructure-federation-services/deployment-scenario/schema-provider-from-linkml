# Slot: hasConformityAssessmentSchemeRegistry


_URL of a registry which will return list of ConformityAssessmentScheme._



URI: [https://$BASE_URL$/casco-conformity/:hasConformityAssessmentSchemeRegistry](https://$BASE_URL$/casco-conformity/:hasConformityAssessmentSchemeRegistry)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentSystem](ConformityAssessmentSystem.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasConformityAssessmentSchemeRegistry
description: URL of a registry which will return list of ConformityAssessmentScheme.
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasConformityAssessmentSchemeRegistry
domain_of:
- ConformityAssessmentSystem
range: string

```
</details>