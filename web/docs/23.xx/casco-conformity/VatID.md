# Class: VatID



URI: [https://$BASE_URL$/casco-conformity/:VatID](https://$BASE_URL$/casco-conformity/:VatID)




```mermaid
 classDiagram
    class VatID
      RegistrationNumber <|-- VatID
      
      VatID : value
        
      
```





## Inheritance
* [RegistrationNumber](RegistrationNumber.md)
    * **VatID**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [value](value.md) | 1..1 <br/> [String](String.md) | The VAT identification number | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/casco-conformity/:VatID |
| native | https://$BASE_URL$/casco-conformity/:VatID |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: VatID
from_schema: https://$BASE_URL$/casco-conformity
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: The VAT identification number.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    identifier: true
    range: string
    required: true

```
</details>

### Induced

<details>
```yaml
name: VatID
from_schema: https://$BASE_URL$/casco-conformity
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: The VAT identification number.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    identifier: true
    alias: value
    owner: VatID
    domain_of:
    - LocalRegistrationNumber
    - VatID
    - LeiCode
    - EORI
    - EUID
    range: string
    required: true

```
</details>