# Slot: proof


_Proof_



URI: [https://$BASE_URL$/casco-conformity/:proof](https://$BASE_URL$/casco-conformity/:proof)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[RequirementClaim](RequirementClaim.md) |  |  yes  |
[StatementOfConformity](StatementOfConformity.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: proof
description: Proof
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: proof
domain_of:
- RequirementClaim
- StatementOfConformity
range: string

```
</details>