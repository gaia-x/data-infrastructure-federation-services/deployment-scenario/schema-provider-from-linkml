# Enum: PersonalDataProtectionRegime



URI: [PersonalDataProtectionRegime](PersonalDataProtectionRegime)

## Permissible Values

| Value | Meaning | Description |
| --- | --- | --- |
| GDPR2016 | None | General Data Protection Regulation / EEA |
| LGPD2019 | None | General Personal Data Protection Law (Lei Geral de Proteção de Dados Pessoais... |
| PDPA2012 | None | Personal Data Protection Act 2012 / SGP |
| CCPA2018 | None | California Consumer Privacy Act / US-CA |
| VCDPA2021 | None | Virginia Consumer Data Protection Act / US-VA |




## Slots

| Name | Description |
| ---  | --- |
| [dataProtectionRegime](dataProtectionRegime.md) | One or more data protection regimes |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: PersonalDataProtectionRegime
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
permissible_values:
  GDPR2016:
    text: GDPR2016
    description: General Data Protection Regulation / EEA.
  LGPD2019:
    text: LGPD2019
    description: General Personal Data Protection Law (Lei Geral de Proteção de Dados
      Pessoais) / BRA.
  PDPA2012:
    text: PDPA2012
    description: Personal Data Protection Act 2012 / SGP.
  CCPA2018:
    text: CCPA2018
    description: California Consumer Privacy Act / US-CA.
  VCDPA2021:
    text: VCDPA2021
    description: Virginia Consumer Data Protection Act / US-VA.

```
</details>
