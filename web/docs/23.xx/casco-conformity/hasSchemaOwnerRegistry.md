# Slot: hasSchemaOwnerRegistry


_URL of a SchemaOwnerRegistry which will return list of SchemaOwner_



URI: [https://$BASE_URL$/casco-conformity/:hasSchemaOwnerRegistry](https://$BASE_URL$/casco-conformity/:hasSchemaOwnerRegistry)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentSystem](ConformityAssessmentSystem.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasSchemaOwnerRegistry
description: URL of a SchemaOwnerRegistry which will return list of SchemaOwner
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasSchemaOwnerRegistry
domain_of:
- ConformityAssessmentSystem
range: string

```
</details>