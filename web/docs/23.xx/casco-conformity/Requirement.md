# Class: Requirement



URI: [casco-conformity:Requirement](https://$BASE_URL$/casco-conformity#Requirement)




```mermaid
 classDiagram
    class Requirement
      Requirement : hasCategory
        
      Requirement : hasDescription
        
      Requirement : hasName
        
      Requirement : hasObjectOfConformity
        
      Requirement : hasProcedure
        
          Requirement --|> ProcedureType : hasProcedure
        
      Requirement : hasType
        
          Requirement --|> RequirementType : hasType
        
      Requirement : isAccepted
        
          Requirement --|> ConformityAssessmentScheme : isAccepted
        
      Requirement : issuer
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [issuer](issuer.md) | 0..1 <br/> [String](String.md) | Issuer of the object | direct |
| [hasName](hasName.md) | 1..1 <br/> [String](String.md) | Name of the Requirement | direct |
| [hasDescription](hasDescription.md) | 0..1 <br/> [String](String.md) | A description in natural language of the Requirement | direct |
| [hasType](hasType.md) | 0..1 <br/> [RequirementType](RequirementType.md) | Type of the Requirement | direct |
| [hasProcedure](hasProcedure.md) | 0..1 <br/> [ProcedureType](ProcedureType.md) | Value of Testing / Audit / Validation / Verification / Inspection (cf | direct |
| [hasCategory](hasCategory.md) | 1..1 <br/> [String](String.md) | The category of this criterion | direct |
| [hasObjectOfConformity](hasObjectOfConformity.md) | 0..1 <br/> [String](String.md) | IRI type of Object targeted by the Requirement | direct |
| [isAccepted](isAccepted.md) | 0..* <br/> [ConformityAssessmentScheme](ConformityAssessmentScheme.md) | List of ConformityAssessmentScheme accepted for this Requirement | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ConformityAssessmentScheme](ConformityAssessmentScheme.md) | [hasRequirements](hasRequirements.md) | range | [Requirement](Requirement.md) |
| [RequirementClaim](RequirementClaim.md) | [hasReference](hasReference.md) | range | [Requirement](Requirement.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:Requirement |
| native | https://$BASE_URL$/casco-conformity/:Requirement |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Requirement
from_schema: https://$BASE_URL$/casco-conformity
slots:
- issuer
- hasName
- hasDescription
- hasType
- hasProcedure
- hasCategory
- hasObjectOfConformity
- isAccepted
slot_usage:
  hasName:
    name: hasName
    description: Name of the Requirement.
    domain_of:
    - ConformityAssessmentSystem
    - Requirement
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the Requirement
    domain_of:
    - ConformityAssessmentScheme
    - Requirement
  hasType:
    name: hasType
    description: Type of the Requirement
    domain_of:
    - Requirement
    - RequirementClaim
    range: RequirementType
  hasCategory:
    name: hasCategory
    description: The category of this criterion
    domain_of:
    - Requirement
    required: true
class_uri: casco-conformity:Requirement

```
</details>

### Induced

<details>
```yaml
name: Requirement
from_schema: https://$BASE_URL$/casco-conformity
slot_usage:
  hasName:
    name: hasName
    description: Name of the Requirement.
    domain_of:
    - ConformityAssessmentSystem
    - Requirement
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the Requirement
    domain_of:
    - ConformityAssessmentScheme
    - Requirement
  hasType:
    name: hasType
    description: Type of the Requirement
    domain_of:
    - Requirement
    - RequirementClaim
    range: RequirementType
  hasCategory:
    name: hasCategory
    description: The category of this criterion
    domain_of:
    - Requirement
    required: true
attributes:
  issuer:
    name: issuer
    description: Issuer of the object
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: issuer
    owner: Requirement
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: string
  hasName:
    name: hasName
    description: Name of the Requirement.
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasName
    alias: hasName
    owner: Requirement
    domain_of:
    - ConformityAssessmentSystem
    - Requirement
    range: string
    required: true
  hasDescription:
    name: hasDescription
    description: A description in natural language of the Requirement
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasDescription
    alias: hasDescription
    owner: Requirement
    domain_of:
    - ConformityAssessmentScheme
    - Requirement
    range: string
  hasType:
    name: hasType
    description: Type of the Requirement
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasType
    owner: Requirement
    domain_of:
    - Requirement
    - RequirementClaim
    range: RequirementType
  hasProcedure:
    name: hasProcedure
    description: Value of Testing / Audit / Validation / Verification / Inspection
      (cf. section 6 of CASCO)
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasProcedure
    owner: Requirement
    domain_of:
    - Requirement
    range: ProcedureType
  hasCategory:
    name: hasCategory
    description: The category of this criterion
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasCategory
    alias: hasCategory
    owner: Requirement
    domain_of:
    - Requirement
    range: string
    required: true
  hasObjectOfConformity:
    name: hasObjectOfConformity
    description: IRI type of Object targeted by the Requirement.
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasObjectOfConformity
    owner: Requirement
    domain_of:
    - Requirement
    range: string
  isAccepted:
    name: isAccepted
    description: List of ConformityAssessmentScheme accepted for this Requirement
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    multivalued: true
    alias: isAccepted
    owner: Requirement
    domain_of:
    - Requirement
    range: ConformityAssessmentScheme
class_uri: casco-conformity:Requirement

```
</details>