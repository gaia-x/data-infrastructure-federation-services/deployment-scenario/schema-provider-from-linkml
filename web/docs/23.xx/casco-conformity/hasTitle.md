# Slot: hasTitle


_title_



URI: [https://$BASE_URL$/casco-conformity/:hasTitle](https://$BASE_URL$/casco-conformity/:hasTitle)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasTitle
description: title
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasTitle
domain_of:
- ConformityAssessmentScheme
range: string

```
</details>