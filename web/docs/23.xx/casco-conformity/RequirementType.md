# Enum: RequirementType



URI: [RequirementType](RequirementType)

## Permissible Values

| Value | Meaning | Description |
| --- | --- | --- |
| DECLARATION | None |  |
| NOTARIZE DECLARATION | None |  |
| CERTIFICATION | None |  |




## Slots

| Name | Description |
| ---  | --- |
| [hasType](hasType.md) | Type of the Requirement |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: RequirementType
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
permissible_values:
  DECLARATION:
    text: DECLARATION
  NOTARIZE DECLARATION:
    text: NOTARIZE DECLARATION
  CERTIFICATION:
    text: CERTIFICATION

```
</details>
