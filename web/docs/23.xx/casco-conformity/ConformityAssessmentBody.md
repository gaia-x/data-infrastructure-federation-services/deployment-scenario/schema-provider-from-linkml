# Class: ConformityAssessmentBody



URI: [casco-conformity:ConformityAssessmentBody](https://$BASE_URL$/casco-conformity#ConformityAssessmentBody)




```mermaid
 classDiagram
    class ConformityAssessmentBody
      Participant <|-- ConformityAssessmentBody
      
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * [Participant](Participant.md)
        * **ConformityAssessmentBody**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [Accreditation](Accreditation.md) | [subject](subject.md) | range | [ConformityAssessmentBody](ConformityAssessmentBody.md) |
| [StatementOfConformity](StatementOfConformity.md) | [issuer](issuer.md) | range | [ConformityAssessmentBody](ConformityAssessmentBody.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:ConformityAssessmentBody |
| native | https://$BASE_URL$/casco-conformity/:ConformityAssessmentBody |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ConformityAssessmentBody
from_schema: https://$BASE_URL$/casco-conformity
is_a: Participant
class_uri: casco-conformity:ConformityAssessmentBody

```
</details>

### Induced

<details>
```yaml
name: ConformityAssessmentBody
from_schema: https://$BASE_URL$/casco-conformity
is_a: Participant
class_uri: casco-conformity:ConformityAssessmentBody

```
</details>