# Slot: hasReference


_Reference_



URI: [https://$BASE_URL$/casco-conformity/:hasReference](https://$BASE_URL$/casco-conformity/:hasReference)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[RequirementClaim](RequirementClaim.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasReference
description: Reference
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasReference
domain_of:
- RequirementClaim
range: string

```
</details>