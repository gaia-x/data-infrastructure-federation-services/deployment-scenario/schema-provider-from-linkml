# Slot: requestType


_The mean to request data retrieval: API, email, webform, unregisteredLetter, registeredLetter, supportCenter._



URI: [https://$BASE_URL$/casco-conformity/:requestType](https://$BASE_URL$/casco-conformity/:requestType)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataAccountExport](DataAccountExport.md) | List of methods to export data from your account out of the service |  no  |







## Properties

* Range: [RequestTypeMeans](RequestTypeMeans.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: requestType
description: 'The mean to request data retrieval: API, email, webform, unregisteredLetter,
  registeredLetter, supportCenter.'
title: request type
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: requestType
owner: DataAccountExport
domain_of:
- DataAccountExport
range: RequestTypeMeans
required: true

```
</details>