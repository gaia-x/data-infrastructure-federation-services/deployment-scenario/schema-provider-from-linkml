# Class: StatementOfConformity



URI: [casco-conformity:StatementOfConformity](https://$BASE_URL$/casco-conformity#StatementOfConformity)




```mermaid
 classDiagram
    class StatementOfConformity
      StatementOfConformity : hasAccreditation
        
          StatementOfConformity --|> Accreditation : hasAccreditation
        
      StatementOfConformity : hasConformityAssessmentScheme
        
          StatementOfConformity --|> ConformityAssessmentScheme : hasConformityAssessmentScheme
        
      StatementOfConformity : hash
        
      StatementOfConformity : issuer
        
          StatementOfConformity --|> ConformityAssessmentBody : issuer
        
      StatementOfConformity : proof
        
      StatementOfConformity : subject
        
      StatementOfConformity : vc
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [subject](subject.md) | 0..1 <br/> [String](String.md) | Object of Conformity | direct |
| [issuer](issuer.md) | 0..1 <br/> [ConformityAssessmentBody](ConformityAssessmentBody.md) | ConformityAssessmentBody ability to issue scheme used | direct |
| [proof](proof.md) | 0..1 <br/> [String](String.md) | proof | direct |
| [hasConformityAssessmentScheme](hasConformityAssessmentScheme.md) | 0..1 <br/> [ConformityAssessmentScheme](ConformityAssessmentScheme.md) | Scheme used to check StatementOfConformity | direct |
| [hasAccreditation](hasAccreditation.md) | 0..1 <br/> [Accreditation](Accreditation.md) | Accreditation to proof ConformityAssessmentBody is accredited to issue the St... | direct |
| [vc](vc.md) | 0..1 <br/> [String](String.md) |  | direct |
| [hash](hash.md) | 0..1 <br/> [String](String.md) | hash | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:StatementOfConformity |
| native | https://$BASE_URL$/casco-conformity/:StatementOfConformity |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: StatementOfConformity
from_schema: https://$BASE_URL$/casco-conformity
slots:
- subject
- issuer
- proof
- hasConformityAssessmentScheme
- hasAccreditation
- vc
- hash
slot_usage:
  issuer:
    name: issuer
    description: ConformityAssessmentBody ability to issue scheme used
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: ConformityAssessmentBody
  subject:
    name: subject
    description: Object of Conformity
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
  proof:
    name: proof
    description: proof
    domain_of:
    - RequirementClaim
    - StatementOfConformity
  hasConformityAssessmentScheme:
    name: hasConformityAssessmentScheme
    description: Scheme used to check StatementOfConformity
    domain_of:
    - StatementOfConformity
    range: ConformityAssessmentScheme
  hasAccreditation:
    name: hasAccreditation
    description: Accreditation to proof ConformityAssessmentBody is accredited to
      issue the StatementOfConformity
    domain_of:
    - StatementOfConformity
    range: Accreditation
  cRValidFrom:
    name: cRValidFrom
    description: Starting date of the StatementOfConformity.
  cRValidUntil:
    name: cRValidUntil
    description: Ending date of the StatementOfConformity.
class_uri: casco-conformity:StatementOfConformity

```
</details>

### Induced

<details>
```yaml
name: StatementOfConformity
from_schema: https://$BASE_URL$/casco-conformity
slot_usage:
  issuer:
    name: issuer
    description: ConformityAssessmentBody ability to issue scheme used
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: ConformityAssessmentBody
  subject:
    name: subject
    description: Object of Conformity
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
  proof:
    name: proof
    description: proof
    domain_of:
    - RequirementClaim
    - StatementOfConformity
  hasConformityAssessmentScheme:
    name: hasConformityAssessmentScheme
    description: Scheme used to check StatementOfConformity
    domain_of:
    - StatementOfConformity
    range: ConformityAssessmentScheme
  hasAccreditation:
    name: hasAccreditation
    description: Accreditation to proof ConformityAssessmentBody is accredited to
      issue the StatementOfConformity
    domain_of:
    - StatementOfConformity
    range: Accreditation
  cRValidFrom:
    name: cRValidFrom
    description: Starting date of the StatementOfConformity.
  cRValidUntil:
    name: cRValidUntil
    description: Ending date of the StatementOfConformity.
attributes:
  subject:
    name: subject
    description: Object of Conformity
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: subject
    owner: StatementOfConformity
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: string
  issuer:
    name: issuer
    description: ConformityAssessmentBody ability to issue scheme used
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: issuer
    owner: StatementOfConformity
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: ConformityAssessmentBody
  proof:
    name: proof
    description: proof
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: proof
    owner: StatementOfConformity
    domain_of:
    - RequirementClaim
    - StatementOfConformity
    range: string
  hasConformityAssessmentScheme:
    name: hasConformityAssessmentScheme
    description: Scheme used to check StatementOfConformity
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasConformityAssessmentScheme
    owner: StatementOfConformity
    domain_of:
    - StatementOfConformity
    range: ConformityAssessmentScheme
  hasAccreditation:
    name: hasAccreditation
    description: Accreditation to proof ConformityAssessmentBody is accredited to
      issue the StatementOfConformity
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasAccreditation
    owner: StatementOfConformity
    domain_of:
    - StatementOfConformity
    range: Accreditation
  vc:
    name: vc
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:vc
    alias: vc
    owner: StatementOfConformity
    domain_of:
    - StatementOfConformity
    range: string
  hash:
    name: hash
    description: hash
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hash
    owner: StatementOfConformity
    domain_of:
    - StatementOfConformity
    - TermsAndConditions
    range: string
class_uri: casco-conformity:StatementOfConformity

```
</details>