# Slot: headquarterAddress


_Full physical location of the headquarter of the organization._



URI: [https://$BASE_URL$/casco-conformity/:headquarterAddress](https://$BASE_URL$/casco-conformity/:headquarterAddress)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number |  no  |







## Properties

* Range: [HeadquarterAddress](HeadquarterAddress.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: headquarterAddress
description: Full physical location of the headquarter of the organization.
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: headquarterAddress
owner: LegalPerson
domain_of:
- LegalPerson
range: HeadquarterAddress
required: true

```
</details>