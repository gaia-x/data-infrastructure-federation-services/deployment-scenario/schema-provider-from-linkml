# Slot: hasAccreditationBodyRegistry


_URL of a registry which will return list of AccreditationBody._



URI: [https://$BASE_URL$/casco-conformity/:hasAccreditationBodyRegistry](https://$BASE_URL$/casco-conformity/:hasAccreditationBodyRegistry)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentSystem](ConformityAssessmentSystem.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasAccreditationBodyRegistry
description: URL of a registry which will return list of AccreditationBody.
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasAccreditationBodyRegistry
domain_of:
- ConformityAssessmentSystem
range: string

```
</details>