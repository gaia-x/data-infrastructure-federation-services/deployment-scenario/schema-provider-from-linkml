# Slot: cRValidFrom

URI: [casco-conformity:cRValidFrom](https://$BASE_URL$/casco-conformity#cRValidFrom)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Accreditation](Accreditation.md) |  |  yes  |







## Properties

* Range: [Datetime](Datetime.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: cRValidFrom
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:cRValidFrom
alias: cRValidFrom
domain_of:
- Accreditation
range: datetime

```
</details>