# Slot: canCertifyConformityAssessmentScheme


_Subject is accredited to certify object of conformity based on this ConformityAssessmentScheme_



URI: [https://$BASE_URL$/casco-conformity/:canCertifyConformityAssessmentScheme](https://$BASE_URL$/casco-conformity/:canCertifyConformityAssessmentScheme)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Accreditation](Accreditation.md) |  |  no  |







## Properties

* Range: [ConformityAssessmentScheme](ConformityAssessmentScheme.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: canCertifyConformityAssessmentScheme
description: Subject is accredited to certify object of conformity based on this ConformityAssessmentScheme
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: canCertifyConformityAssessmentScheme
domain_of:
- Accreditation
range: ConformityAssessmentScheme
required: true

```
</details>