# Slot: locality


_The v:locality property specifies the locality (e.g., city) of a postal address._



URI: [https://$BASE_URL$/casco-conformity/:locality](https://$BASE_URL$/casco-conformity/:locality)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Address](Address.md) |  |  no  |
[LegalAddress](LegalAddress.md) |  |  no  |
[HeadquarterAddress](HeadquarterAddress.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: locality
description: The v:locality property specifies the locality (e.g., city) of a postal
  address.
title: locality
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: locality
owner: Address
domain_of:
- Address
range: string
required: false

```
</details>