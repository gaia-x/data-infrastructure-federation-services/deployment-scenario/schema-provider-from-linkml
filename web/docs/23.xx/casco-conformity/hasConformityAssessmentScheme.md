# Slot: hasConformityAssessmentScheme


_ConformityAssessmentScheme_



URI: [https://$BASE_URL$/casco-conformity/:hasConformityAssessmentScheme](https://$BASE_URL$/casco-conformity/:hasConformityAssessmentScheme)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[StatementOfConformity](StatementOfConformity.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasConformityAssessmentScheme
description: ConformityAssessmentScheme
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasConformityAssessmentScheme
domain_of:
- StatementOfConformity
range: string

```
</details>