# Slot: hasAccreditationRegistry


_URL of a registry which will return list of Accreditation._



URI: [https://$BASE_URL$/casco-conformity/:hasAccreditationRegistry](https://$BASE_URL$/casco-conformity/:hasAccreditationRegistry)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentSystem](ConformityAssessmentSystem.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasAccreditationRegistry
description: URL of a registry which will return list of Accreditation.
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasAccreditationRegistry
domain_of:
- ConformityAssessmentSystem
range: string

```
</details>