# Class: ConformityAssessmentScheme



URI: [casco-conformity:ConformityAssessmentScheme](https://$BASE_URL$/casco-conformity#ConformityAssessmentScheme)




```mermaid
 classDiagram
    class ConformityAssessmentScheme
      ConformityAssessmentScheme : hasConformityAssessmentSystem
        
          ConformityAssessmentScheme --|> ConformityAssessmentSystem : hasConformityAssessmentSystem
        
      ConformityAssessmentScheme : hasDescription
        
      ConformityAssessmentScheme : hasMethodology
        
      ConformityAssessmentScheme : hasReferenceUrl
        
      ConformityAssessmentScheme : hasRequirements
        
          ConformityAssessmentScheme --|> Requirement : hasRequirements
        
      ConformityAssessmentScheme : hasScope
        
      ConformityAssessmentScheme : hasTitle
        
      ConformityAssessmentScheme : hasVersion
        
      ConformityAssessmentScheme : issuer
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [issuer](issuer.md) | 0..1 <br/> [String](String.md) | Issuer of the object | direct |
| [hasRequirements](hasRequirements.md) | 0..* <br/> [Requirement](Requirement.md) | List of Requirement | direct |
| [hasScope](hasScope.md) | 0..1 <br/> [String](String.md) | Object type defined by the Conformity | direct |
| [hasMethodology](hasMethodology.md) | 0..1 <br/> [String](String.md) | Method used by the scheme | direct |
| [hasReferenceUrl](hasReferenceUrl.md) | 1..1 <br/> [String](String.md) | URI to reference the content of the compliance reference in a single PDF file | direct |
| [hasVersion](hasVersion.md) | 0..1 <br/> [String](String.md) | String designated the version of the reference document | direct |
| [hasConformityAssessmentSystem](hasConformityAssessmentSystem.md) | 0..1 <br/> [ConformityAssessmentSystem](ConformityAssessmentSystem.md) |  | direct |
| [hasTitle](hasTitle.md) | 0..1 <br/> [String](String.md) | ConformityAssessmentScheme type | direct |
| [hasDescription](hasDescription.md) | 0..1 <br/> [String](String.md) | A description in natural language | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [Requirement](Requirement.md) | [isAccepted](isAccepted.md) | range | [ConformityAssessmentScheme](ConformityAssessmentScheme.md) |
| [Accreditation](Accreditation.md) | [canCertifyConformityAssessmentScheme](canCertifyConformityAssessmentScheme.md) | range | [ConformityAssessmentScheme](ConformityAssessmentScheme.md) |
| [StatementOfConformity](StatementOfConformity.md) | [hasConformityAssessmentScheme](hasConformityAssessmentScheme.md) | range | [ConformityAssessmentScheme](ConformityAssessmentScheme.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:ConformityAssessmentScheme |
| native | https://$BASE_URL$/casco-conformity/:ConformityAssessmentScheme |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ConformityAssessmentScheme
from_schema: https://$BASE_URL$/casco-conformity
slots:
- issuer
- hasRequirements
- hasScope
- hasMethodology
- hasReferenceUrl
- hasVersion
- hasConformityAssessmentSystem
- hasTitle
- hasDescription
slot_usage:
  hasRequirements:
    name: hasRequirements
    description: List of Requirement
    multivalued: true
    domain_of:
    - ConformityAssessmentScheme
    range: Requirement
  hasScope:
    name: hasScope
    description: Object type defined by the Conformity
    domain_of:
    - ConformityAssessmentScheme
  hasMethodology:
    name: hasMethodology
    description: Method used by the scheme
    domain_of:
    - ConformityAssessmentScheme
  hasReferenceUrl:
    name: hasReferenceUrl
    description: URI to reference the content of the compliance reference in a single
      PDF file
    domain_of:
    - ConformityAssessmentScheme
    required: true
  hasVersion:
    name: hasVersion
    description: String designated the version of the reference document. Can be a
      date or a build number or X.YY.ZZ.....
    domain_of:
    - ConformityAssessmentScheme
  hasTitle:
    name: hasTitle
    description: ConformityAssessmentScheme type
    domain_of:
    - ConformityAssessmentScheme
  hasDescription:
    name: hasDescription
    description: A description in natural language
    domain_of:
    - ConformityAssessmentScheme
    - Requirement
class_uri: casco-conformity:ConformityAssessmentScheme

```
</details>

### Induced

<details>
```yaml
name: ConformityAssessmentScheme
from_schema: https://$BASE_URL$/casco-conformity
slot_usage:
  hasRequirements:
    name: hasRequirements
    description: List of Requirement
    multivalued: true
    domain_of:
    - ConformityAssessmentScheme
    range: Requirement
  hasScope:
    name: hasScope
    description: Object type defined by the Conformity
    domain_of:
    - ConformityAssessmentScheme
  hasMethodology:
    name: hasMethodology
    description: Method used by the scheme
    domain_of:
    - ConformityAssessmentScheme
  hasReferenceUrl:
    name: hasReferenceUrl
    description: URI to reference the content of the compliance reference in a single
      PDF file
    domain_of:
    - ConformityAssessmentScheme
    required: true
  hasVersion:
    name: hasVersion
    description: String designated the version of the reference document. Can be a
      date or a build number or X.YY.ZZ.....
    domain_of:
    - ConformityAssessmentScheme
  hasTitle:
    name: hasTitle
    description: ConformityAssessmentScheme type
    domain_of:
    - ConformityAssessmentScheme
  hasDescription:
    name: hasDescription
    description: A description in natural language
    domain_of:
    - ConformityAssessmentScheme
    - Requirement
attributes:
  issuer:
    name: issuer
    description: Issuer of the object
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: issuer
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: string
  hasRequirements:
    name: hasRequirements
    description: List of Requirement
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    multivalued: true
    alias: hasRequirements
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentScheme
    range: Requirement
  hasScope:
    name: hasScope
    description: Object type defined by the Conformity
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasScope
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentScheme
    range: string
  hasMethodology:
    name: hasMethodology
    description: Method used by the scheme
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasMethodology
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentScheme
    range: string
  hasReferenceUrl:
    name: hasReferenceUrl
    description: URI to reference the content of the compliance reference in a single
      PDF file
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasReferenceUrl
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentScheme
    range: string
    required: true
  hasVersion:
    name: hasVersion
    description: String designated the version of the reference document. Can be a
      date or a build number or X.YY.ZZ.....
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasVersion
    alias: hasVersion
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentScheme
    range: string
  hasConformityAssessmentSystem:
    name: hasConformityAssessmentSystem
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasConformityAssessmentSystem
    alias: hasConformityAssessmentSystem
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentScheme
    - Accreditation
    range: ConformityAssessmentSystem
  hasTitle:
    name: hasTitle
    description: ConformityAssessmentScheme type
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasTitle
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentScheme
    range: string
  hasDescription:
    name: hasDescription
    description: A description in natural language
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasDescription
    alias: hasDescription
    owner: ConformityAssessmentScheme
    domain_of:
    - ConformityAssessmentScheme
    - Requirement
    range: string
class_uri: casco-conformity:ConformityAssessmentScheme

```
</details>