# Class: GaiaXTermsAndConditions


_Gaia-X terms and conditions, each issuer has to aggree._





URI: [gx:GaiaXTermsAndConditions](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#GaiaXTermsAndConditions)




```mermaid
 classDiagram
    class GaiaXTermsAndConditions
      GaiaXTermsAndConditions : termsAndConditions
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [termsAndConditions](termsAndConditions.md) | 1..1 <br/> [String](String.md) |  | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [Issuer](Issuer.md) | [termsAndConditions](termsAndConditions.md) | range | [GaiaXTermsAndConditions](GaiaXTermsAndConditions.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:GaiaXTermsAndConditions |
| native | https://$BASE_URL$/casco-conformity/:GaiaXTermsAndConditions |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: GaiaXTermsAndConditions
description: Gaia-X terms and conditions, each issuer has to aggree.
from_schema: https://$BASE_URL$/casco-conformity
attributes:
  termsAndConditions:
    name: termsAndConditions
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: true
    equals_string: The PARTICIPANT signing the Self-Description agrees as follows:\n-
      to update its descriptions about any changes, be it technical, organizational,
      or legal - especially but not limited to contractual in regards to the indicated
      attributes present in the descriptions.\n\nThe keypair used to sign Verifiable
      Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate
      statements in regards to the claims which result in a non-compliance with the
      Trust Framework and policy rules defined in the Policy Rules and Labelling Document
      (PRLD).
class_uri: gx:GaiaXTermsAndConditions

```
</details>

### Induced

<details>
```yaml
name: GaiaXTermsAndConditions
description: Gaia-X terms and conditions, each issuer has to aggree.
from_schema: https://$BASE_URL$/casco-conformity
attributes:
  termsAndConditions:
    name: termsAndConditions
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: termsAndConditions
    owner: GaiaXTermsAndConditions
    domain_of:
    - GaiaXTermsAndConditions
    - Issuer
    - ServiceOffering
    range: string
    required: true
    equals_string: The PARTICIPANT signing the Self-Description agrees as follows:\n-
      to update its descriptions about any changes, be it technical, organizational,
      or legal - especially but not limited to contractual in regards to the indicated
      attributes present in the descriptions.\n\nThe keypair used to sign Verifiable
      Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate
      statements in regards to the claims which result in a non-compliance with the
      Trust Framework and policy rules defined in the Policy Rules and Labelling Document
      (PRLD).
class_uri: gx:GaiaXTermsAndConditions

```
</details>