# Slot: hash


_hash_



URI: [https://$BASE_URL$/casco-conformity/:hash](https://$BASE_URL$/casco-conformity/:hash)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[StatementOfConformity](StatementOfConformity.md) |  |  no  |
[TermsAndConditions](TermsAndConditions.md) | Terms and Conditions applying to a service offering |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hash
description: hash
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hash
domain_of:
- StatementOfConformity
- TermsAndConditions
range: string

```
</details>