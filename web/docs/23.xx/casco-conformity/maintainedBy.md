# Slot: maintainedBy


_Participant maintaining the resource in operational condition and thus have physical access to it._



URI: [https://$BASE_URL$/casco-conformity/:maintainedBy](https://$BASE_URL$/casco-conformity/:maintainedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: maintainedBy
description: Participant maintaining the resource in operational condition and thus
  have physical access to it.
title: maintained by
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: maintainedBy
owner: PhysicalResource
domain_of:
- PhysicalResource
range: LegalPerson
required: true

```
</details>