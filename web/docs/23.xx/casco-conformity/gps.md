# Slot: gps


_GPS in ISO 6709:2008/Cor 1:2009 format._



URI: [https://$BASE_URL$/casco-conformity/:gps](https://$BASE_URL$/casco-conformity/:gps)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Address](Address.md) |  |  no  |
[LegalAddress](LegalAddress.md) |  |  no  |
[HeadquarterAddress](HeadquarterAddress.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: gps
description: GPS in ISO 6709:2008/Cor 1:2009 format.
title: gps
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: gps
owner: Address
domain_of:
- Address
range: string
required: false

```
</details>