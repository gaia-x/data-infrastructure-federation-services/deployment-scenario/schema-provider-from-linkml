# Class: RequirementClaim



URI: [casco-conformity:RequirementClaim](https://$BASE_URL$/casco-conformity#RequirementClaim)




```mermaid
 classDiagram
    class RequirementClaim
      RequirementClaim : hasReference
        
          RequirementClaim --|> Requirement : hasReference
        
      RequirementClaim : hasType
        
      RequirementClaim : issuer
        
      RequirementClaim : proof
        
      RequirementClaim : subject
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [issuer](issuer.md) | 0..1 <br/> [String](String.md) | Issuer of the object | direct |
| [hasType](hasType.md) | 0..1 <br/> [String](String.md) | Type of the RequirementClaim | direct |
| [subject](subject.md) | 0..1 <br/> [String](String.md) | Object of Conformity | direct |
| [hasReference](hasReference.md) | 0..1 <br/> [Requirement](Requirement.md) | Reference | direct |
| [proof](proof.md) | 0..1 <br/> [String](String.md) | proof | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:RequirementClaim |
| native | https://$BASE_URL$/casco-conformity/:RequirementClaim |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: RequirementClaim
from_schema: https://$BASE_URL$/casco-conformity
slots:
- issuer
- hasType
- subject
- hasReference
- proof
slot_usage:
  hasType:
    name: hasType
    description: Type of the RequirementClaim
    domain_of:
    - Requirement
    - RequirementClaim
  subject:
    name: subject
    description: Object of Conformity
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
  hasReference:
    name: hasReference
    description: Reference
    domain_of:
    - RequirementClaim
    range: Requirement
  proof:
    name: proof
    description: proof
    domain_of:
    - RequirementClaim
    - StatementOfConformity
class_uri: casco-conformity:RequirementClaim

```
</details>

### Induced

<details>
```yaml
name: RequirementClaim
from_schema: https://$BASE_URL$/casco-conformity
slot_usage:
  hasType:
    name: hasType
    description: Type of the RequirementClaim
    domain_of:
    - Requirement
    - RequirementClaim
  subject:
    name: subject
    description: Object of Conformity
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
  hasReference:
    name: hasReference
    description: Reference
    domain_of:
    - RequirementClaim
    range: Requirement
  proof:
    name: proof
    description: proof
    domain_of:
    - RequirementClaim
    - StatementOfConformity
attributes:
  issuer:
    name: issuer
    description: Issuer of the object
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: issuer
    owner: RequirementClaim
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: string
  hasType:
    name: hasType
    description: Type of the RequirementClaim
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasType
    owner: RequirementClaim
    domain_of:
    - Requirement
    - RequirementClaim
    range: string
  subject:
    name: subject
    description: Object of Conformity
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: subject
    owner: RequirementClaim
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: string
  hasReference:
    name: hasReference
    description: Reference
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasReference
    owner: RequirementClaim
    domain_of:
    - RequirementClaim
    range: Requirement
  proof:
    name: proof
    description: proof
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: proof
    owner: RequirementClaim
    domain_of:
    - RequirementClaim
    - StatementOfConformity
    range: string
class_uri: casco-conformity:RequirementClaim

```
</details>