# casco-conformity



URI: https://$BASE_URL$/casco-conformity

Name: casco-conformity



## Classes

| Class | Description |
| --- | --- |
| [Accreditation](Accreditation.md) | None |
| [Address](Address.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[HeadquarterAddress](HeadquarterAddress.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[LegalAddress](LegalAddress.md) | None |
| [ConformityAssessmentScheme](ConformityAssessmentScheme.md) | None |
| [ConformityAssessmentSystem](ConformityAssessmentSystem.md) | None |
| [DataAccountExport](DataAccountExport.md) | List of methods to export data from your account out of the service. |
| [Endpoint](Endpoint.md) | An endpoint is a mean to access and interact with a service or a resource. |
| [GaiaXEntity](GaiaXEntity.md) | Root class for Gaia-X entities. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Issuer](Issuer.md) | Each issuer shall issue a GaiaXTermsAndCondition verifiable credential. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Participant](Participant.md) | An legal or natural person that is onboarded to Gaia-X and offers, consumes services or operates resources. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[AccreditationBody](AccreditationBody.md) | Has authority from Schema Owner to issue Accreditations to Conformity Assessment Body |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ConformityAssessmentBody](ConformityAssessmentBody.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[SchemaOwner](SchemaOwner.md) | Develops and maintains ConformityAssessmentScheme |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[SystemOwner](SystemOwner.md) | Develops and maintains ConformityAssessmentSystem |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Resource](Resource.md) | A resource that may be aggregated in a Service Offering or exist independently of it. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal service, a warehouse, a plant. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, filter services and resources. |
| [GaiaXTermsAndConditions](GaiaXTermsAndConditions.md) | Gaia-X terms and conditions, each issuer has to aggree. |
| [RegistrationNumber](RegistrationNumber.md) | Country's registration number, which identifies one specific entity. Allowed entries are Local, VatID, lei code, EODI, and EUID. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[EORI](EORI.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[EUID](EUID.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[LeiCode](LeiCode.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[LocalRegistrationNumber](LocalRegistrationNumber.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[VatID](VatID.md) | None |
| [Relationship](Relationship.md) | A relationship between two organisations. |
| [Requirement](Requirement.md) | None |
| [RequirementClaim](RequirementClaim.md) | None |
| [StandardConformity](StandardConformity.md) | Details about standard applied to entities. |
| [StatementOfConformity](StatementOfConformity.md) | None |
| [TermsAndConditions](TermsAndConditions.md) | Terms and Conditions applying to a service offering. |



## Slots

| Slot | Description |
| --- | --- |
| [accessType](accessType.md) | Type of data support: digital, physical |
| [aggregationOf](aggregationOf.md) | Resources related to the resource and that can exist independently of it |
| [canCertifyConformityAssessmentScheme](canCertifyConformityAssessmentScheme.md) | Subject is accredited to certify object of conformity based on this Conformit... |
| [country_name](country_name.md) | Physical location of head quarter in ISO 3166-1 alpha2, alpha-3 or numeric fo... |
| [cRValidFrom](cRValidFrom.md) |  |
| [cRValidUntil](cRValidUntil.md) |  |
| [dataAccountExport](dataAccountExport.md) | One or more  methods to export data out of the service |
| [dataProtectionRegime](dataProtectionRegime.md) | One or more data protection regimes |
| [dependsOn](dependsOn.md) | A resolvable link to the service offering self-description related to the ser... |
| [description](description.md) | A free text description of the data resource |
| [endpoint](endpoint.md) | Endpoint through which the Service Offering can be accessed |
| [endpointDescription](endpointDescription.md) | The Description (e |
| [endpointURL](endpointURL.md) | The URL of the endpoint where it can be accessed |
| [formatType](formatType.md) | Type of Media Types (formerly known as MIME types) as defined by the IANA |
| [getVerifiableCredentialsIDs](getVerifiableCredentialsIDs.md) | a route used to synchronize catalogues and retrieve the list of Verifiable Cr... |
| [gps](gps.md) | GPS in ISO 6709:2008/Cor 1:2009 format |
| [hasAccreditation](hasAccreditation.md) | Accreditation |
| [hasAccreditationBodyRegistry](hasAccreditationBodyRegistry.md) | URL of a registry which will return list of AccreditationBody |
| [hasAccreditationRegistry](hasAccreditationRegistry.md) | URL of a registry which will return list of Accreditation |
| [hasCategory](hasCategory.md) |  |
| [hasConformityAssessmentBody](hasConformityAssessmentBody.md) |  |
| [hasConformityAssessmentScheme](hasConformityAssessmentScheme.md) | ConformityAssessmentScheme |
| [hasConformityAssessmentSchemeRegistry](hasConformityAssessmentSchemeRegistry.md) | URL of a registry which will return list of ConformityAssessmentScheme |
| [hasConformityAssessmentSystem](hasConformityAssessmentSystem.md) |  |
| [hasConformityAssessmentSystemTitle](hasConformityAssessmentSystemTitle.md) |  |
| [hasDescription](hasDescription.md) |  |
| [hash](hash.md) | hash |
| [hasMethodology](hasMethodology.md) | Methodology |
| [hasName](hasName.md) |  |
| [hasObjectOfConformity](hasObjectOfConformity.md) | IRI type of Object targeted by the Requirement |
| [hasProcedure](hasProcedure.md) | Value of Testing / Audit / Validation / Verification / Inspection (cf |
| [hasReference](hasReference.md) | Reference |
| [hasReferenceUrl](hasReferenceUrl.md) | ReferenceURL |
| [hasRequirementClaim](hasRequirementClaim.md) | RequirementClaim |
| [hasRequirements](hasRequirements.md) | requirements |
| [hasSchemaOwnerRegistry](hasSchemaOwnerRegistry.md) | URL of a SchemaOwnerRegistry which will return list of SchemaOwner |
| [hasScope](hasScope.md) | scope |
| [hasSha256](hasSha256.md) |  |
| [hasStatementOfConformity](hasStatementOfConformity.md) | StatementOfConformity |
| [hasSystem](hasSystem.md) | System |
| [hasTitle](hasTitle.md) | title |
| [hasType](hasType.md) | type |
| [hasVersion](hasVersion.md) |  |
| [headquarterAddress](headquarterAddress.md) | Full physical location of the headquarter of the organization |
| [hostedOn](hostedOn.md) | List of Resource references where service is hosted and can be instantiated |
| [isAccepted](isAccepted.md) | List of ConformityAssessmentScheme accepted for this Requirement |
| [issuer](issuer.md) | Issuer of the object |
| [keyword](keyword.md) | Keywords that describe / tag the service |
| [legalAddress](legalAddress.md) | The full legal address of the organization |
| [locality](locality.md) | The v:locality property specifies the locality (e |
| [locationAddress](locationAddress.md) | A vcard:Address object that contains the physical location in ISO 3166-1 alph... |
| [locationGPS](locationGPS.md) | A list of physical GPS in ISO 6709:2008/Cor 1:2009 format |
| [maintainedBy](maintainedBy.md) | Participant maintaining the resource in operational condition and thus have p... |
| [manufacturedBy](manufacturedBy.md) | Participant manufacturing the resource |
| [name](name.md) | A human readable name of the data resource |
| [ownedBy](ownedBy.md) | Participant owning the resource |
| [parentOrganizationOf](parentOrganizationOf.md) | A list of direct participant that this entity is a subOrganization of, if any |
| [policy](policy.md) | One or more policies expressed using a DSL (e |
| [postal_code](postal_code.md) | String of a street-address |
| [proof](proof.md) | Proof |
| [providedBy](providedBy.md) | A resolvable link to the participant self-description providing the service |
| [provisionType](provisionType.md) | Provision type of the service |
| [publisher](publisher.md) | Publisher of the standard |
| [registrationNumber](registrationNumber.md) | Country's registration number, which identifies one specific entity |
| [relatedOrganizations](relatedOrganizations.md) | A list of related organization, either as sub or parent organization, if any |
| [requestType](requestType.md) | The mean to request data retrieval: API, email, webform, unregisteredLetter, ... |
| [standardConformity](standardConformity.md) | Provides information about applied standards |
| [standardReference](standardReference.md) | Provides a link to schemas or details about applied standards |
| [street_address](street_address.md) | The v:street-address property specifies the street address of a postal addres... |
| [subject](subject.md) | Subject |
| [subOrganisationOf](subOrganisationOf.md) | A direct participant with a legal mandate on this entity, e |
| [termsAndConditions](termsAndConditions.md) |  |
| [title](title.md) | Name of the standard |
| [URL](URL.md) | A resolvable link to document |
| [value](value.md) | The state issued company number |
| [vc](vc.md) |  |


## Enumerations

| Enumeration | Description |
| --- | --- |
| [AccessTypeMeans](AccessTypeMeans.md) |  |
| [PersonalDataProtectionRegime](PersonalDataProtectionRegime.md) |  |
| [ProcedureType](ProcedureType.md) |  |
| [RequestTypeMeans](RequestTypeMeans.md) |  |
| [RequirementType](RequirementType.md) |  |


## Types

| Type | Description |
| --- | --- |
| [Boolean](Boolean.md) | A binary (true or false) value |
| [Curie](Curie.md) | a compact URI |
| [Date](Date.md) | a date (year, month and day) in an idealized calendar |
| [DateOrDatetime](DateOrDatetime.md) | Either a date or a datetime |
| [Datetime](Datetime.md) | The combination of a date and time |
| [Decimal](Decimal.md) | A real number with arbitrary precision that conforms to the xsd:decimal speci... |
| [Double](Double.md) | A real number that conforms to the xsd:double specification |
| [Float](Float.md) | A real number that conforms to the xsd:float specification |
| [Integer](Integer.md) | An integer |
| [Jsonpath](Jsonpath.md) | A string encoding a JSON Path |
| [Jsonpointer](Jsonpointer.md) | A string encoding a JSON Pointer |
| [Ncname](Ncname.md) | Prefix part of CURIE |
| [Nodeidentifier](Nodeidentifier.md) | A URI, CURIE or BNODE that represents a node in a model |
| [Objectidentifier](Objectidentifier.md) | A URI or CURIE that represents an object in the model |
| [Sparqlpath](Sparqlpath.md) | A string encoding a SPARQL Property Path |
| [String](String.md) | A character string |
| [Time](Time.md) | A time object represents a (local) time of day, independent of any particular... |
| [Uri](Uri.md) | a complete URI |
| [Uriorcurie](Uriorcurie.md) | a URI or a CURIE |


## Subsets

| Subset | Description |
| --- | --- |
