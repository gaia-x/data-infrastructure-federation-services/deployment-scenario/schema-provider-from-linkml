# Slot: subject


_Subject_



URI: [https://$BASE_URL$/casco-conformity/:subject](https://$BASE_URL$/casco-conformity/:subject)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Accreditation](Accreditation.md) |  |  yes  |
[RequirementClaim](RequirementClaim.md) |  |  yes  |
[StatementOfConformity](StatementOfConformity.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: subject
description: Subject
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: subject
domain_of:
- Accreditation
- RequirementClaim
- StatementOfConformity
range: string

```
</details>