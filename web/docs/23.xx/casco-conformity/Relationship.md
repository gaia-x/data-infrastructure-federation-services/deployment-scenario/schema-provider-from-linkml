# Class: Relationship


_A relationship between two organisations._





URI: [https://$BASE_URL$/casco-conformity/:Relationship](https://$BASE_URL$/casco-conformity/:Relationship)




```mermaid
 classDiagram
    class Relationship
      Relationship : parentOrganizationOf
        
          Relationship --|> LegalPerson : parentOrganizationOf
        
      Relationship : subOrganisationOf
        
          Relationship --|> LegalPerson : subOrganisationOf
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [parentOrganizationOf](parentOrganizationOf.md) | 0..1 <br/> [LegalPerson](LegalPerson.md) | A list of direct participant that this entity is a subOrganization of, if any | direct |
| [subOrganisationOf](subOrganisationOf.md) | 0..1 <br/> [LegalPerson](LegalPerson.md) | A direct participant with a legal mandate on this entity, e | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [LegalPerson](LegalPerson.md) | [relatedOrganizations](relatedOrganizations.md) | range | [Relationship](Relationship.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/casco-conformity/:Relationship |
| native | https://$BASE_URL$/casco-conformity/:Relationship |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Relationship
description: A relationship between two organisations.
from_schema: https://$BASE_URL$/casco-conformity
attributes:
  parentOrganizationOf:
    name: parentOrganizationOf
    description: A list of direct participant that this entity is a subOrganization
      of, if any.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: LegalPerson
  subOrganisationOf:
    name: subOrganisationOf
    description: A direct participant with a legal mandate on this entity, e.g., as
      a subsidiary.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: LegalPerson

```
</details>

### Induced

<details>
```yaml
name: Relationship
description: A relationship between two organisations.
from_schema: https://$BASE_URL$/casco-conformity
attributes:
  parentOrganizationOf:
    name: parentOrganizationOf
    description: A list of direct participant that this entity is a subOrganization
      of, if any.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: parentOrganizationOf
    owner: Relationship
    domain_of:
    - Relationship
    range: LegalPerson
  subOrganisationOf:
    name: subOrganisationOf
    description: A direct participant with a legal mandate on this entity, e.g., as
      a subsidiary.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: subOrganisationOf
    owner: Relationship
    domain_of:
    - Relationship
    range: LegalPerson

```
</details>