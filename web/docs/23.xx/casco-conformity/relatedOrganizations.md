# Slot: relatedOrganizations


_A list of related organization, either as sub or parent organization, if any._



URI: [https://$BASE_URL$/casco-conformity/:relatedOrganizations](https://$BASE_URL$/casco-conformity/:relatedOrganizations)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number |  no  |







## Properties

* Range: [Relationship](Relationship.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: relatedOrganizations
description: A list of related organization, either as sub or parent organization,
  if any.
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
multivalued: true
alias: relatedOrganizations
owner: LegalPerson
domain_of:
- LegalPerson
range: Relationship

```
</details>