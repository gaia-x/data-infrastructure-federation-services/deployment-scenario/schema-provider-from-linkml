# Slot: providedBy


_A resolvable link to the participant self-description providing the service._



URI: [https://$BASE_URL$/casco-conformity/:providedBy](https://$BASE_URL$/casco-conformity/:providedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: providedBy
description: A resolvable link to the participant self-description providing the service.
title: provided by
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: providedBy
owner: ServiceOffering
domain_of:
- ServiceOffering
range: LegalPerson
required: true

```
</details>