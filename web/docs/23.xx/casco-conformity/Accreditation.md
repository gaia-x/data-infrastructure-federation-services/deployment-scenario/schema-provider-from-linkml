# Class: Accreditation



URI: [casco-conformity:Accreditation](https://$BASE_URL$/casco-conformity#Accreditation)




```mermaid
 classDiagram
    class Accreditation
      Accreditation : canCertifyConformityAssessmentScheme
        
          Accreditation --|> ConformityAssessmentScheme : canCertifyConformityAssessmentScheme
        
      Accreditation : cRValidFrom
        
      Accreditation : cRValidUntil
        
      Accreditation : hasConformityAssessmentSystem
        
          Accreditation --|> ConformityAssessmentSystem : hasConformityAssessmentSystem
        
      Accreditation : hasSha256
        
      Accreditation : issuer
        
          Accreditation --|> AccreditationBody : issuer
        
      Accreditation : subject
        
          Accreditation --|> ConformityAssessmentBody : subject
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [issuer](issuer.md) | 0..1 <br/> [AccreditationBody](AccreditationBody.md) | Issuer of the object | direct |
| [subject](subject.md) | 1..1 <br/> [ConformityAssessmentBody](ConformityAssessmentBody.md) | ConformityAssessmentBody accredited by issuer | direct |
| [canCertifyConformityAssessmentScheme](canCertifyConformityAssessmentScheme.md) | 1..1 <br/> [ConformityAssessmentScheme](ConformityAssessmentScheme.md) | Subject is accredited to certify object of conformity based on this Conformit... | direct |
| [hasConformityAssessmentSystem](hasConformityAssessmentSystem.md) | 0..1 <br/> [ConformityAssessmentSystem](ConformityAssessmentSystem.md) | ConformityAssessmentSystem associated | direct |
| [hasSha256](hasSha256.md) | 0..1 <br/> [String](String.md) | Sha256 | direct |
| [cRValidFrom](cRValidFrom.md) | 0..1 <br/> [Datetime](Datetime.md) | Starting date of the accreditation | direct |
| [cRValidUntil](cRValidUntil.md) | 0..1 <br/> [Datetime](Datetime.md) | Ending date of the accreditation | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [StatementOfConformity](StatementOfConformity.md) | [hasAccreditation](hasAccreditation.md) | range | [Accreditation](Accreditation.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:Accreditation |
| native | https://$BASE_URL$/casco-conformity/:Accreditation |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Accreditation
from_schema: https://$BASE_URL$/casco-conformity
slots:
- issuer
- subject
- canCertifyConformityAssessmentScheme
- hasConformityAssessmentSystem
- hasSha256
- cRValidFrom
- cRValidUntil
slot_usage:
  issuer:
    name: issuer
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: AccreditationBody
  subject:
    name: subject
    description: ConformityAssessmentBody accredited by issuer
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: ConformityAssessmentBody
    required: true
  hasConformityAssessmentSystem:
    name: hasConformityAssessmentSystem
    description: ConformityAssessmentSystem associated
    domain_of:
    - ConformityAssessmentScheme
    - Accreditation
  hasSha256:
    name: hasSha256
    description: Sha256
    domain_of:
    - Accreditation
  cRValidFrom:
    name: cRValidFrom
    description: Starting date of the accreditation.
    domain_of:
    - Accreditation
  cRValidUntil:
    name: cRValidUntil
    description: Ending date of the accreditation.
    domain_of:
    - Accreditation
class_uri: casco-conformity:Accreditation

```
</details>

### Induced

<details>
```yaml
name: Accreditation
from_schema: https://$BASE_URL$/casco-conformity
slot_usage:
  issuer:
    name: issuer
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: AccreditationBody
  subject:
    name: subject
    description: ConformityAssessmentBody accredited by issuer
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: ConformityAssessmentBody
    required: true
  hasConformityAssessmentSystem:
    name: hasConformityAssessmentSystem
    description: ConformityAssessmentSystem associated
    domain_of:
    - ConformityAssessmentScheme
    - Accreditation
  hasSha256:
    name: hasSha256
    description: Sha256
    domain_of:
    - Accreditation
  cRValidFrom:
    name: cRValidFrom
    description: Starting date of the accreditation.
    domain_of:
    - Accreditation
  cRValidUntil:
    name: cRValidUntil
    description: Ending date of the accreditation.
    domain_of:
    - Accreditation
attributes:
  issuer:
    name: issuer
    description: Issuer of the object
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: issuer
    owner: Accreditation
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: AccreditationBody
  subject:
    name: subject
    description: ConformityAssessmentBody accredited by issuer
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: subject
    owner: Accreditation
    domain_of:
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: ConformityAssessmentBody
    required: true
  canCertifyConformityAssessmentScheme:
    name: canCertifyConformityAssessmentScheme
    description: Subject is accredited to certify object of conformity based on this
      ConformityAssessmentScheme
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: canCertifyConformityAssessmentScheme
    owner: Accreditation
    domain_of:
    - Accreditation
    range: ConformityAssessmentScheme
    required: true
  hasConformityAssessmentSystem:
    name: hasConformityAssessmentSystem
    description: ConformityAssessmentSystem associated
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasConformityAssessmentSystem
    alias: hasConformityAssessmentSystem
    owner: Accreditation
    domain_of:
    - ConformityAssessmentScheme
    - Accreditation
    range: ConformityAssessmentSystem
  hasSha256:
    name: hasSha256
    description: Sha256
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasSha256
    alias: hasSha256
    owner: Accreditation
    domain_of:
    - Accreditation
    range: string
  cRValidFrom:
    name: cRValidFrom
    description: Starting date of the accreditation.
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:cRValidFrom
    alias: cRValidFrom
    owner: Accreditation
    domain_of:
    - Accreditation
    range: datetime
  cRValidUntil:
    name: cRValidUntil
    description: Ending date of the accreditation.
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:cRValidUntil
    alias: cRValidUntil
    owner: Accreditation
    domain_of:
    - Accreditation
    range: datetime
class_uri: casco-conformity:Accreditation

```
</details>