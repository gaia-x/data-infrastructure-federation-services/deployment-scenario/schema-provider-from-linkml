# Slot: hasAccreditation


_Accreditation_



URI: [https://$BASE_URL$/casco-conformity/:hasAccreditation](https://$BASE_URL$/casco-conformity/:hasAccreditation)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[StatementOfConformity](StatementOfConformity.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasAccreditation
description: Accreditation
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasAccreditation
domain_of:
- StatementOfConformity
range: string

```
</details>