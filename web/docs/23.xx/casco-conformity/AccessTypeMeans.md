# Enum: AccessTypeMeans



URI: [AccessTypeMeans](AccessTypeMeans)

## Permissible Values

| Value | Meaning | Description |
| --- | --- | --- |
| digital | None |  |
| physical | None |  |




## Slots

| Name | Description |
| ---  | --- |
| [accessType](accessType.md) | Type of data support: digital, physical |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: AccessTypeMeans
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
permissible_values:
  digital:
    text: digital
  physical:
    text: physical

```
</details>
