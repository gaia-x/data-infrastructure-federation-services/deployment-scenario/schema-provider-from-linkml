# Class: SystemOwner


_Develops and maintains ConformityAssessmentSystem_





URI: [casco-conformity:SystemOwner](https://$BASE_URL$/casco-conformity#SystemOwner)




```mermaid
 classDiagram
    class SystemOwner
      Participant <|-- SystemOwner
      
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * [Participant](Participant.md)
        * **SystemOwner**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:SystemOwner |
| native | https://$BASE_URL$/casco-conformity/:SystemOwner |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: SystemOwner
description: Develops and maintains ConformityAssessmentSystem
from_schema: https://$BASE_URL$/casco-conformity
is_a: Participant
class_uri: casco-conformity:SystemOwner

```
</details>

### Induced

<details>
```yaml
name: SystemOwner
description: Develops and maintains ConformityAssessmentSystem
from_schema: https://$BASE_URL$/casco-conformity
is_a: Participant
class_uri: casco-conformity:SystemOwner

```
</details>