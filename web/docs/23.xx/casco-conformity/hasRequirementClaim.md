# Slot: hasRequirementClaim


_RequirementClaim_



URI: [https://$BASE_URL$/casco-conformity/:hasRequirementClaim](https://$BASE_URL$/casco-conformity/:hasRequirementClaim)



<!-- no inheritance hierarchy -->







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasRequirementClaim
description: RequirementClaim
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasRequirementClaim
range: string

```
</details>