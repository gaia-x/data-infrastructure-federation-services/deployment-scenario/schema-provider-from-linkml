# Slot: policy


_One or more policies expressed using a DSL (e.g., Rego or ODRL)._



URI: [https://$BASE_URL$/casco-conformity/:policy](https://$BASE_URL$/casco-conformity/:policy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: policy
description: One or more policies expressed using a DSL (e.g., Rego or ODRL).
title: policy
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
multivalued: true
alias: policy
owner: ServiceOffering
domain_of:
- ServiceOffering
range: string
required: true

```
</details>