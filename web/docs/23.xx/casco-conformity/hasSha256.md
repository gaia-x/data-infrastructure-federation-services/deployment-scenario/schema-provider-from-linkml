# Slot: hasSha256

URI: [casco-conformity:hasSha256](https://$BASE_URL$/casco-conformity#hasSha256)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Accreditation](Accreditation.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasSha256
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:hasSha256
alias: hasSha256
domain_of:
- Accreditation
range: string

```
</details>