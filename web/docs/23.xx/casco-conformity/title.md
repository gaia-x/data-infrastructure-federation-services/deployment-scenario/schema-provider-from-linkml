# Slot: title


_Name of the standard._



URI: [https://$BASE_URL$/casco-conformity/:title](https://$BASE_URL$/casco-conformity/:title)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[StandardConformity](StandardConformity.md) | Details about standard applied to entities |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: title
description: Name of the standard.
title: title
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
multivalued: true
alias: title
owner: StandardConformity
domain_of:
- StandardConformity
range: string
required: true
maximum_cardinality: 1

```
</details>