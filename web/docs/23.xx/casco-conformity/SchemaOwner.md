# Class: SchemaOwner


_Develops and maintains ConformityAssessmentScheme_





URI: [casco-conformity:SchemaOwner](https://$BASE_URL$/casco-conformity#SchemaOwner)




```mermaid
 classDiagram
    class SchemaOwner
      Participant <|-- SchemaOwner
      
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * [Participant](Participant.md)
        * **SchemaOwner**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:SchemaOwner |
| native | https://$BASE_URL$/casco-conformity/:SchemaOwner |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: SchemaOwner
description: Develops and maintains ConformityAssessmentScheme
from_schema: https://$BASE_URL$/casco-conformity
is_a: Participant
class_uri: casco-conformity:SchemaOwner

```
</details>

### Induced

<details>
```yaml
name: SchemaOwner
description: Develops and maintains ConformityAssessmentScheme
from_schema: https://$BASE_URL$/casco-conformity
is_a: Participant
class_uri: casco-conformity:SchemaOwner

```
</details>