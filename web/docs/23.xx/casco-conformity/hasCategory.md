# Slot: hasCategory

URI: [casco-conformity:hasCategory](https://$BASE_URL$/casco-conformity#hasCategory)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Requirement](Requirement.md) |  |  yes  |







## Properties

* Range: [String](String.md)






## Examples

| Value |
| --- |
| Data Protection |
| Transparency |

## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasCategory
examples:
- value: Data Protection
- value: Transparency
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:hasCategory
alias: hasCategory
domain_of:
- Requirement
range: string

```
</details>