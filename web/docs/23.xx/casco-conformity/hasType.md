# Slot: hasType


_type_



URI: [https://$BASE_URL$/casco-conformity/:hasType](https://$BASE_URL$/casco-conformity/:hasType)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Requirement](Requirement.md) |  |  yes  |
[RequirementClaim](RequirementClaim.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasType
description: type
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasType
domain_of:
- Requirement
- RequirementClaim
range: string

```
</details>