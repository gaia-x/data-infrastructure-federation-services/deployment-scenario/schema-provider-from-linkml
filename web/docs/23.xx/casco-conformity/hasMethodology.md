# Slot: hasMethodology


_Methodology_



URI: [https://$BASE_URL$/casco-conformity/:hasMethodology](https://$BASE_URL$/casco-conformity/:hasMethodology)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasMethodology
description: Methodology
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasMethodology
domain_of:
- ConformityAssessmentScheme
range: string

```
</details>