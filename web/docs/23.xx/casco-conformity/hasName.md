# Slot: hasName

URI: [casco-conformity:hasName](https://$BASE_URL$/casco-conformity#hasName)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentSystem](ConformityAssessmentSystem.md) |  |  yes  |
[Requirement](Requirement.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasName
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:hasName
alias: hasName
domain_of:
- ConformityAssessmentSystem
- Requirement
range: string

```
</details>