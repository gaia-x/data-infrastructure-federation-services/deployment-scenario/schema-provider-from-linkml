# Class: AccreditationBody


_Has authority from Schema Owner to issue Accreditations to Conformity Assessment Body_





URI: [casco-conformity:AccreditationBody](https://$BASE_URL$/casco-conformity#AccreditationBody)




```mermaid
 classDiagram
    class AccreditationBody
      Participant <|-- AccreditationBody
      
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * [Participant](Participant.md)
        * **AccreditationBody**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [Accreditation](Accreditation.md) | [issuer](issuer.md) | range | [AccreditationBody](AccreditationBody.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:AccreditationBody |
| native | https://$BASE_URL$/casco-conformity/:AccreditationBody |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: AccreditationBody
description: Has authority from Schema Owner to issue Accreditations to Conformity
  Assessment Body
from_schema: https://$BASE_URL$/casco-conformity
is_a: Participant
class_uri: casco-conformity:AccreditationBody

```
</details>

### Induced

<details>
```yaml
name: AccreditationBody
description: Has authority from Schema Owner to issue Accreditations to Conformity
  Assessment Body
from_schema: https://$BASE_URL$/casco-conformity
is_a: Participant
class_uri: casco-conformity:AccreditationBody

```
</details>