# Slot: hasStatementOfConformity


_StatementOfConformity_



URI: [https://$BASE_URL$/casco-conformity/:hasStatementOfConformity](https://$BASE_URL$/casco-conformity/:hasStatementOfConformity)



<!-- no inheritance hierarchy -->







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasStatementOfConformity
description: StatementOfConformity
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasStatementOfConformity
range: string

```
</details>