# Slot: dataProtectionRegime


_One or more data protection regimes._



URI: [https://$BASE_URL$/casco-conformity/:dataProtectionRegime](https://$BASE_URL$/casco-conformity/:dataProtectionRegime)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [PersonalDataProtectionRegime](PersonalDataProtectionRegime.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: dataProtectionRegime
description: One or more data protection regimes.
title: data protection regime
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
multivalued: true
alias: dataProtectionRegime
owner: ServiceOffering
domain_of:
- ServiceOffering
range: PersonalDataProtectionRegime
required: false

```
</details>