# Slot: vc

URI: [casco-conformity:vc](https://$BASE_URL$/casco-conformity#vc)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[StatementOfConformity](StatementOfConformity.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: vc
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:vc
alias: vc
domain_of:
- StatementOfConformity
range: string

```
</details>