# Slot: hasScope


_scope_



URI: [https://$BASE_URL$/casco-conformity/:hasScope](https://$BASE_URL$/casco-conformity/:hasScope)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasScope
description: scope
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasScope
domain_of:
- ConformityAssessmentScheme
range: string

```
</details>