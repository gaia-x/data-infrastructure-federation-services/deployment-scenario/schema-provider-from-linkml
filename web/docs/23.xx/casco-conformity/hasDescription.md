# Slot: hasDescription

URI: [casco-conformity:hasDescription](https://$BASE_URL$/casco-conformity#hasDescription)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  yes  |
[Requirement](Requirement.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasDescription
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:hasDescription
alias: hasDescription
domain_of:
- ConformityAssessmentScheme
- Requirement
range: string

```
</details>