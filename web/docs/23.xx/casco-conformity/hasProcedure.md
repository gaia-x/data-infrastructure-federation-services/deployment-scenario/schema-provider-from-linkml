# Slot: hasProcedure


_Value of Testing / Audit / Validation / Verification / Inspection (cf. section 6 of CASCO)_



URI: [https://$BASE_URL$/casco-conformity/:hasProcedure](https://$BASE_URL$/casco-conformity/:hasProcedure)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Requirement](Requirement.md) |  |  no  |







## Properties

* Range: [ProcedureType](ProcedureType.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasProcedure
description: Value of Testing / Audit / Validation / Verification / Inspection (cf.
  section 6 of CASCO)
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: hasProcedure
domain_of:
- Requirement
range: ProcedureType

```
</details>