# Slot: formatType


_Type of Media Types (formerly known as MIME types) as defined by the IANA._



URI: [https://$BASE_URL$/casco-conformity/:formatType](https://$BASE_URL$/casco-conformity/:formatType)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataAccountExport](DataAccountExport.md) | List of methods to export data from your account out of the service |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: formatType
description: Type of Media Types (formerly known as MIME types) as defined by the
  IANA.
title: format type
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: formatType
owner: DataAccountExport
domain_of:
- DataAccountExport
range: string
required: true

```
</details>