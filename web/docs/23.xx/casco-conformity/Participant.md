# Class: Participant


_An legal or natural person that is onboarded to Gaia-X and offers, consumes services or operates resources._




* __NOTE__: this is an abstract class and should not be instantiated directly


URI: [https://$BASE_URL$/casco-conformity/:Participant](https://$BASE_URL$/casco-conformity/:Participant)




```mermaid
 classDiagram
    class Participant
      GaiaXEntity <|-- Participant
      

      Participant <|-- SystemOwner
      Participant <|-- SchemaOwner
      Participant <|-- AccreditationBody
      Participant <|-- ConformityAssessmentBody
      Participant <|-- LegalPerson
      
      
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **Participant**
        * [SystemOwner](SystemOwner.md)
        * [SchemaOwner](SchemaOwner.md)
        * [AccreditationBody](AccreditationBody.md)
        * [ConformityAssessmentBody](ConformityAssessmentBody.md)
        * [LegalPerson](LegalPerson.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/casco-conformity/:Participant |
| native | https://$BASE_URL$/casco-conformity/:Participant |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Participant
description: An legal or natural person that is onboarded to Gaia-X and offers, consumes
  services or operates resources.
from_schema: https://$BASE_URL$/casco-conformity
is_a: GaiaXEntity
abstract: true

```
</details>

### Induced

<details>
```yaml
name: Participant
description: An legal or natural person that is onboarded to Gaia-X and offers, consumes
  services or operates resources.
from_schema: https://$BASE_URL$/casco-conformity
is_a: GaiaXEntity
abstract: true

```
</details>