# Slot: hasConformityAssessmentBody

URI: [casco-conformity:hasConformityAssessmentBody](https://$BASE_URL$/casco-conformity#hasConformityAssessmentBody)



<!-- no inheritance hierarchy -->







## Properties

* Range: [ConformityAssessmentBody](ConformityAssessmentBody.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasConformityAssessmentBody
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:hasConformityAssessmentBody
alias: hasConformityAssessmentBody
range: ConformityAssessmentBody

```
</details>