# Slot: keyword


_Keywords that describe / tag the service._



URI: [https://$BASE_URL$/casco-conformity/:keyword](https://$BASE_URL$/casco-conformity/:keyword)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: keyword
description: Keywords that describe / tag the service.
title: keyword
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
multivalued: true
alias: keyword
owner: ServiceOffering
domain_of:
- ServiceOffering
range: string
required: false

```
</details>