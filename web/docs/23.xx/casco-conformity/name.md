# Slot: name

URI: [https://$BASE_URL$/casco-conformity/:name](https://$BASE_URL$/casco-conformity/:name)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |
[Resource](Resource.md) | A resource that may be aggregated in a Service Offering or exist independentl... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information








## LinkML Source

<details>
```yaml
name: name
alias: name
domain_of:
- Resource
- ServiceOffering
range: string

```
</details>