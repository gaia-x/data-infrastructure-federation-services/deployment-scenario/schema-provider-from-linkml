# Class: ConformityAssessmentSystem



URI: [casco-conformity:ConformityAssessmentSystem](https://$BASE_URL$/casco-conformity#ConformityAssessmentSystem)




```mermaid
 classDiagram
    class ConformityAssessmentSystem
      ConformityAssessmentSystem : hasAccreditationBodyRegistry
        
      ConformityAssessmentSystem : hasAccreditationRegistry
        
      ConformityAssessmentSystem : hasConformityAssessmentSchemeRegistry
        
      ConformityAssessmentSystem : hasName
        
      ConformityAssessmentSystem : hasSchemaOwnerRegistry
        
      ConformityAssessmentSystem : issuer
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [issuer](issuer.md) | 0..1 <br/> [String](String.md) | Issuer of the object | direct |
| [hasName](hasName.md) | 0..1 <br/> [String](String.md) | Name of the ConformityAssessmentSystem | direct |
| [hasSchemaOwnerRegistry](hasSchemaOwnerRegistry.md) | 0..1 <br/> [String](String.md) | URL of a SchemaOwnerRegistry which will return list of SchemaOwner | direct |
| [hasConformityAssessmentSchemeRegistry](hasConformityAssessmentSchemeRegistry.md) | 0..1 <br/> [String](String.md) | URL of a registry which will return list of ConformityAssessmentScheme | direct |
| [hasAccreditationBodyRegistry](hasAccreditationBodyRegistry.md) | 1..1 <br/> [String](String.md) | URL of a registry which will return list of AccreditationBody | direct |
| [hasAccreditationRegistry](hasAccreditationRegistry.md) | 0..1 <br/> [String](String.md) | URL of a registry which will return list of Accreditation | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [ConformityAssessmentScheme](ConformityAssessmentScheme.md) | [hasConformityAssessmentSystem](hasConformityAssessmentSystem.md) | range | [ConformityAssessmentSystem](ConformityAssessmentSystem.md) |
| [Accreditation](Accreditation.md) | [hasConformityAssessmentSystem](hasConformityAssessmentSystem.md) | range | [ConformityAssessmentSystem](ConformityAssessmentSystem.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | casco-conformity:ConformityAssessmentSystem |
| native | https://$BASE_URL$/casco-conformity/:ConformityAssessmentSystem |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ConformityAssessmentSystem
from_schema: https://$BASE_URL$/casco-conformity
slots:
- issuer
- hasName
- hasSchemaOwnerRegistry
- hasConformityAssessmentSchemeRegistry
- hasAccreditationBodyRegistry
- hasAccreditationRegistry
slot_usage:
  hasName:
    name: hasName
    description: Name of the ConformityAssessmentSystem
    domain_of:
    - ConformityAssessmentSystem
    - Requirement
  hasSchemaOwnerRegistry:
    name: hasSchemaOwnerRegistry
    description: URL of a SchemaOwnerRegistry which will return list of SchemaOwner
    domain_of:
    - ConformityAssessmentSystem
  hasConformityAssessmentSchemeRegistry:
    name: hasConformityAssessmentSchemeRegistry
    description: URL of a registry which will return list of ConformityAssessmentScheme.
    domain_of:
    - ConformityAssessmentSystem
  hasAccreditationBodyRegistry:
    name: hasAccreditationBodyRegistry
    description: URL of a registry which will return list of AccreditationBody.
    domain_of:
    - ConformityAssessmentSystem
    required: true
  hasAccreditationRegistry:
    name: hasAccreditationRegistry
    description: URL of a registry which will return list of Accreditation.
    domain_of:
    - ConformityAssessmentSystem
class_uri: casco-conformity:ConformityAssessmentSystem

```
</details>

### Induced

<details>
```yaml
name: ConformityAssessmentSystem
from_schema: https://$BASE_URL$/casco-conformity
slot_usage:
  hasName:
    name: hasName
    description: Name of the ConformityAssessmentSystem
    domain_of:
    - ConformityAssessmentSystem
    - Requirement
  hasSchemaOwnerRegistry:
    name: hasSchemaOwnerRegistry
    description: URL of a SchemaOwnerRegistry which will return list of SchemaOwner
    domain_of:
    - ConformityAssessmentSystem
  hasConformityAssessmentSchemeRegistry:
    name: hasConformityAssessmentSchemeRegistry
    description: URL of a registry which will return list of ConformityAssessmentScheme.
    domain_of:
    - ConformityAssessmentSystem
  hasAccreditationBodyRegistry:
    name: hasAccreditationBodyRegistry
    description: URL of a registry which will return list of AccreditationBody.
    domain_of:
    - ConformityAssessmentSystem
    required: true
  hasAccreditationRegistry:
    name: hasAccreditationRegistry
    description: URL of a registry which will return list of Accreditation.
    domain_of:
    - ConformityAssessmentSystem
attributes:
  issuer:
    name: issuer
    description: Issuer of the object
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: issuer
    owner: ConformityAssessmentSystem
    domain_of:
    - ConformityAssessmentSystem
    - ConformityAssessmentScheme
    - Requirement
    - Accreditation
    - RequirementClaim
    - StatementOfConformity
    range: string
  hasName:
    name: hasName
    description: Name of the ConformityAssessmentSystem
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    slot_uri: casco-conformity:hasName
    alias: hasName
    owner: ConformityAssessmentSystem
    domain_of:
    - ConformityAssessmentSystem
    - Requirement
    range: string
  hasSchemaOwnerRegistry:
    name: hasSchemaOwnerRegistry
    description: URL of a SchemaOwnerRegistry which will return list of SchemaOwner
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasSchemaOwnerRegistry
    owner: ConformityAssessmentSystem
    domain_of:
    - ConformityAssessmentSystem
    range: string
  hasConformityAssessmentSchemeRegistry:
    name: hasConformityAssessmentSchemeRegistry
    description: URL of a registry which will return list of ConformityAssessmentScheme.
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasConformityAssessmentSchemeRegistry
    owner: ConformityAssessmentSystem
    domain_of:
    - ConformityAssessmentSystem
    range: string
  hasAccreditationBodyRegistry:
    name: hasAccreditationBodyRegistry
    description: URL of a registry which will return list of AccreditationBody.
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasAccreditationBodyRegistry
    owner: ConformityAssessmentSystem
    domain_of:
    - ConformityAssessmentSystem
    range: string
    required: true
  hasAccreditationRegistry:
    name: hasAccreditationRegistry
    description: URL of a registry which will return list of Accreditation.
    from_schema: https://$BASE_URL$/casco-conformity
    rank: 1000
    alias: hasAccreditationRegistry
    owner: ConformityAssessmentSystem
    domain_of:
    - ConformityAssessmentSystem
    range: string
class_uri: casco-conformity:ConformityAssessmentSystem

```
</details>