# Slot: dependsOn


_A resolvable link to the service offering self-description related to the service and that can exist independently of it._



URI: [https://$BASE_URL$/casco-conformity/:dependsOn](https://$BASE_URL$/casco-conformity/:dependsOn)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [ServiceOffering](ServiceOffering.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: dependsOn
description: A resolvable link to the service offering self-description related to
  the service and that can exist independently of it.
title: depends on
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
multivalued: true
alias: dependsOn
owner: ServiceOffering
domain_of:
- ServiceOffering
range: ServiceOffering
required: false

```
</details>