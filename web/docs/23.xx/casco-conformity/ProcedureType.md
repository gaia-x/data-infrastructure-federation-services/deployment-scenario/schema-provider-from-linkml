# Enum: ProcedureType



URI: [ProcedureType](ProcedureType)

## Permissible Values

| Value | Meaning | Description |
| --- | --- | --- |
| TESTING | None |  |
| AUDIT | None |  |
| VALIDATION | None |  |
| VERIFICATION | None |  |




## Slots

| Name | Description |
| ---  | --- |
| [hasProcedure](hasProcedure.md) | Value of Testing / Audit / Validation / Verification / Inspection (cf |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: ProcedureType
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
permissible_values:
  TESTING:
    text: TESTING
  AUDIT:
    text: AUDIT
  VALIDATION:
    text: VALIDATION
  VERIFICATION:
    text: VERIFICATION

```
</details>
