# Slot: hasConformityAssessmentSystem

URI: [casco-conformity:hasConformityAssessmentSystem](https://$BASE_URL$/casco-conformity#hasConformityAssessmentSystem)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  no  |
[Accreditation](Accreditation.md) |  |  yes  |







## Properties

* Range: [ConformityAssessmentSystem](ConformityAssessmentSystem.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: hasConformityAssessmentSystem
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
slot_uri: casco-conformity:hasConformityAssessmentSystem
alias: hasConformityAssessmentSystem
domain_of:
- ConformityAssessmentScheme
- Accreditation
range: ConformityAssessmentSystem

```
</details>