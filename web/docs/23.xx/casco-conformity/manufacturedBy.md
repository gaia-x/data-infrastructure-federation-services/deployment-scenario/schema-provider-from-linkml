# Slot: manufacturedBy


_Participant manufacturing the resource._



URI: [https://$BASE_URL$/casco-conformity/:manufacturedBy](https://$BASE_URL$/casco-conformity/:manufacturedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: manufacturedBy
description: Participant manufacturing the resource.
title: manufactured by
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: manufacturedBy
owner: PhysicalResource
domain_of:
- PhysicalResource
range: LegalPerson
required: false

```
</details>