# Slot: isAccepted


_List of ConformityAssessmentScheme accepted for this Requirement_



URI: [https://$BASE_URL$/casco-conformity/:isAccepted](https://$BASE_URL$/casco-conformity/:isAccepted)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Requirement](Requirement.md) |  |  no  |







## Properties

* Range: [ConformityAssessmentScheme](ConformityAssessmentScheme.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: isAccepted
description: List of ConformityAssessmentScheme accepted for this Requirement
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
multivalued: true
alias: isAccepted
domain_of:
- Requirement
range: ConformityAssessmentScheme

```
</details>