# Slot: issuer


_Issuer of the object_



URI: [https://$BASE_URL$/casco-conformity/:issuer](https://$BASE_URL$/casco-conformity/:issuer)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[ConformityAssessmentSystem](ConformityAssessmentSystem.md) |  |  no  |
[ConformityAssessmentScheme](ConformityAssessmentScheme.md) |  |  no  |
[Requirement](Requirement.md) |  |  no  |
[Accreditation](Accreditation.md) |  |  yes  |
[RequirementClaim](RequirementClaim.md) |  |  no  |
[StatementOfConformity](StatementOfConformity.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: issuer
description: Issuer of the object
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: issuer
domain_of:
- ConformityAssessmentSystem
- ConformityAssessmentScheme
- Requirement
- Accreditation
- RequirementClaim
- StatementOfConformity
range: string

```
</details>