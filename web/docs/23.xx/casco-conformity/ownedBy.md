# Slot: ownedBy


_Participant owning the resource._



URI: [https://$BASE_URL$/casco-conformity/:ownedBy](https://$BASE_URL$/casco-conformity/:ownedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/casco-conformity




## LinkML Source

<details>
```yaml
name: ownedBy
description: Participant owning the resource.
title: owned by
from_schema: https://$BASE_URL$/casco-conformity
rank: 1000
alias: ownedBy
owner: PhysicalResource
domain_of:
- PhysicalResource
range: LegalPerson
required: false

```
</details>