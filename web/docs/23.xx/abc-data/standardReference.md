# Slot: standardReference


_Provides a link to schemas or details about applied standards._



URI: [https://$BASE_URL$/abc-data/:standardReference](https://$BASE_URL$/abc-data/:standardReference)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[StandardConformity](StandardConformity.md) | Details about standard applied to entities |  no  |







## Properties

* Range: [Uri](Uri.md)

* Multivalued: True

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: standardReference
description: Provides a link to schemas or details about applied standards.
title: standard reference
from_schema: https://$BASE_URL$/abc-data
rank: 1000
multivalued: true
alias: standardReference
owner: StandardConformity
domain_of:
- StandardConformity
range: uri
required: true

```
</details>