# Slot: standardConformity


_Provides information about applied standards._



URI: [https://$BASE_URL$/abc-data/:standardConformity](https://$BASE_URL$/abc-data/:standardConformity)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Endpoint](Endpoint.md) | An endpoint is a mean to access and interact with a service or a resource |  no  |







## Properties

* Range: [StandardConformity](StandardConformity.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: standardConformity
description: Provides information about applied standards.
title: standard conformity
from_schema: https://$BASE_URL$/abc-data
rank: 1000
multivalued: true
alias: standardConformity
owner: Endpoint
domain_of:
- Endpoint
range: StandardConformity
required: false

```
</details>