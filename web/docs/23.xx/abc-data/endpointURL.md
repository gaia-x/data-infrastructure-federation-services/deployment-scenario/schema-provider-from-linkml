# Slot: endpointURL


_The URL of the endpoint where it can be accessed._



URI: [https://$BASE_URL$/abc-data/:endpointURL](https://$BASE_URL$/abc-data/:endpointURL)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Endpoint](Endpoint.md) | An endpoint is a mean to access and interact with a service or a resource |  no  |







## Properties

* Range: [Uri](Uri.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: endpointURL
description: The URL of the endpoint where it can be accessed.
title: endpoint URL
from_schema: https://$BASE_URL$/abc-data
rank: 1000
multivalued: true
alias: endpointURL
owner: Endpoint
domain_of:
- Endpoint
range: uri
required: false

```
</details>