# Class: DataConsumer


_is equivalent to Gaia-X Consumer_





URI: [https://$BASE_URL$/abc-data/:DataConsumer](https://$BASE_URL$/abc-data/:DataConsumer)




```mermaid
 classDiagram
    class DataConsumer
      LegalPerson <|-- DataConsumer
      
      DataConsumer : headquarterAddress
        
          DataConsumer --|> HeadquarterAddress : headquarterAddress
        
      DataConsumer : legalAddress
        
          DataConsumer --|> LegalAddress : legalAddress
        
      DataConsumer : registrationNumber
        
      DataConsumer : relatedOrganizations
        
          DataConsumer --|> Relationship : relatedOrganizations
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * [Participant](Participant.md)
        * [LegalPerson](LegalPerson.md)
            * **DataConsumer**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [registrationNumber](registrationNumber.md) | 1..* <br/> [String](String.md) | Country's registration number, which identifies one specific entity | [LegalPerson](LegalPerson.md) |
| [legalAddress](legalAddress.md) | 1..1 <br/> [LegalAddress](LegalAddress.md) | The full legal address of the organization | [LegalPerson](LegalPerson.md) |
| [headquarterAddress](headquarterAddress.md) | 1..1 <br/> [HeadquarterAddress](HeadquarterAddress.md) | Full physical location of the headquarter of the organization | [LegalPerson](LegalPerson.md) |
| [relatedOrganizations](relatedOrganizations.md) | 0..* <br/> [Relationship](Relationship.md) | A list of related organization, either as sub or parent organization, if any | [LegalPerson](LegalPerson.md) |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [DataProductUsageContract](DataProductUsageContract.md) | [consumedBy](consumedBy.md) | range | [DataConsumer](DataConsumer.md) |
| [DataProductUsageContract](DataProductUsageContract.md) | [dataProduct](dataProduct.md) | range | [DataConsumer](DataConsumer.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/abc-data/:DataConsumer |
| native | https://$BASE_URL$/abc-data/:DataConsumer |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: DataConsumer
description: is equivalent to Gaia-X Consumer
from_schema: https://$BASE_URL$/abc-data
is_a: LegalPerson
tree_root: true

```
</details>

### Induced

<details>
```yaml
name: DataConsumer
description: is equivalent to Gaia-X Consumer
from_schema: https://$BASE_URL$/abc-data
is_a: LegalPerson
attributes:
  registrationNumber:
    name: registrationNumber
    description: Country's registration number, which identifies one specific entity.
      Valid formats are local, EUID, EORI, vatID, leiCode.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: registrationNumber
    owner: DataConsumer
    domain_of:
    - LegalPerson
    range: string
    required: true
    inlined: true
    inlined_as_list: true
    any_of:
    - range: RegistrationNumber
    - range: LocalRegistrationNumber
    - range: VatID
    - range: LeiCode
    - range: EORI
    - range: EUID
  legalAddress:
    name: legalAddress
    description: The full legal address of the organization.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: legalAddress
    owner: DataConsumer
    domain_of:
    - LegalPerson
    range: LegalAddress
    required: true
  headquarterAddress:
    name: headquarterAddress
    description: Full physical location of the headquarter of the organization.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: headquarterAddress
    owner: DataConsumer
    domain_of:
    - LegalPerson
    range: HeadquarterAddress
    required: true
  relatedOrganizations:
    name: relatedOrganizations
    description: A list of related organization, either as sub or parent organization,
      if any.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: relatedOrganizations
    owner: DataConsumer
    domain_of:
    - LegalPerson
    range: Relationship
tree_root: true

```
</details>