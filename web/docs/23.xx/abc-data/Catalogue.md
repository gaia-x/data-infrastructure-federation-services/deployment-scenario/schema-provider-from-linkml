# Class: Catalogue


_A catalogue service is a subclass of serviceOffering used to browse, search, filter services and resources._





URI: [https://$BASE_URL$/abc-data/:Catalogue](https://$BASE_URL$/abc-data/:Catalogue)




```mermaid
 classDiagram
    class Catalogue
      ServiceOffering <|-- Catalogue
      
      Catalogue : aggregationOf
        
      Catalogue : dataAccountExport
        
          Catalogue --|> DataAccountExport : dataAccountExport
        
      Catalogue : dataProtectionRegime
        
          Catalogue --|> PersonalDataProtectionRegime : dataProtectionRegime
        
      Catalogue : dependsOn
        
          Catalogue --|> ServiceOffering : dependsOn
        
      Catalogue : description
        
      Catalogue : endpoint
        
          Catalogue --|> Endpoint : endpoint
        
      Catalogue : getVerifiableCredentialsIDs
        
      Catalogue : hostedOn
        
      Catalogue : keyword
        
      Catalogue : name
        
      Catalogue : policy
        
      Catalogue : providedBy
        
          Catalogue --|> LegalPerson : providedBy
        
      Catalogue : provisionType
        
      Catalogue : termsAndConditions
        
          Catalogue --|> TermsAndConditions : termsAndConditions
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * [ServiceOffering](ServiceOffering.md)
        * **Catalogue**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [getVerifiableCredentialsIDs](getVerifiableCredentialsIDs.md) | 1..* <br/> [String](String.md) | a route used to synchronize catalogues and retrieve the list of Verifiable Cr... | direct |
| [providedBy](providedBy.md) | 1..1 <br/> [LegalPerson](LegalPerson.md) | A resolvable link to the participant self-description providing the service | [ServiceOffering](ServiceOffering.md) |
| [name](name.md) | 1..1 <br/> [String](String.md) | Human readable name of the service offering | [ServiceOffering](ServiceOffering.md) |
| [termsAndConditions](termsAndConditions.md) | 1..* <br/> [TermsAndConditions](TermsAndConditions.md) | A resolvable link to the Terms and Conditions applying to that service | [ServiceOffering](ServiceOffering.md) |
| [policy](policy.md) | 1..* <br/> [String](String.md) | One or more policies expressed using a DSL (e | [ServiceOffering](ServiceOffering.md) |
| [dataProtectionRegime](dataProtectionRegime.md) | 0..* <br/> [PersonalDataProtectionRegime](PersonalDataProtectionRegime.md) | One or more data protection regimes | [ServiceOffering](ServiceOffering.md) |
| [dataAccountExport](dataAccountExport.md) | 1..* <br/> [DataAccountExport](DataAccountExport.md) | One or more  methods to export data out of the service | [ServiceOffering](ServiceOffering.md) |
| [description](description.md) | 0..1 <br/> [String](String.md) | A description in natural language | [ServiceOffering](ServiceOffering.md) |
| [keyword](keyword.md) | 0..* <br/> [String](String.md) | Keywords that describe / tag the service | [ServiceOffering](ServiceOffering.md) |
| [provisionType](provisionType.md) | 0..1 <br/> [String](String.md) | Provision type of the service | [ServiceOffering](ServiceOffering.md) |
| [endpoint](endpoint.md) | 0..1 <br/> [Endpoint](Endpoint.md) | Endpoint through which the Service Offering can be accessed | [ServiceOffering](ServiceOffering.md) |
| [hostedOn](hostedOn.md) | 0..* <br/> [String](String.md) | List of Resource references where service is hosted and can be instantiated | [ServiceOffering](ServiceOffering.md) |
| [dependsOn](dependsOn.md) | 0..* <br/> [ServiceOffering](ServiceOffering.md) | A resolvable link to the service offering self-description related to the ser... | [ServiceOffering](ServiceOffering.md) |
| [aggregationOf](aggregationOf.md) | 0..* <br/> [String](String.md) | A resolvable link to the resources self-description related to the service an... | [ServiceOffering](ServiceOffering.md) |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/abc-data/:Catalogue |
| native | https://$BASE_URL$/abc-data/:Catalogue |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Catalogue
description: A catalogue service is a subclass of serviceOffering used to browse,
  search, filter services and resources.
from_schema: https://$BASE_URL$/abc-data
is_a: ServiceOffering
attributes:
  getVerifiableCredentialsIDs:
    name: getVerifiableCredentialsIDs
    description: a route used to synchronize catalogues and retrieve the list of Verifiable
      Credentials (issuer, id).
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    required: true
    inlined: true
    inlined_as_list: true
tree_root: true

```
</details>

### Induced

<details>
```yaml
name: Catalogue
description: A catalogue service is a subclass of serviceOffering used to browse,
  search, filter services and resources.
from_schema: https://$BASE_URL$/abc-data
is_a: ServiceOffering
attributes:
  getVerifiableCredentialsIDs:
    name: getVerifiableCredentialsIDs
    description: a route used to synchronize catalogues and retrieve the list of Verifiable
      Credentials (issuer, id).
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: getVerifiableCredentialsIDs
    owner: Catalogue
    domain_of:
    - Catalogue
    range: string
    required: true
    inlined: true
    inlined_as_list: true
  providedBy:
    name: providedBy
    description: A resolvable link to the participant self-description providing the
      service.
    title: provided by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    alias: providedBy
    owner: Catalogue
    domain_of:
    - DataProduct
    - DataProductUsageContract
    - DataUsageAgreement
    - ServiceOffering
    range: LegalPerson
    required: true
  name:
    name: name
    description: Human readable name of the service offering.
    title: name
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    alias: name
    owner: Catalogue
    domain_of:
    - Resource
    - ServiceOffering
    range: string
    required: true
  termsAndConditions:
    name: termsAndConditions
    description: A resolvable link to the Terms and Conditions applying to that service.
    title: terms and conditions
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    multivalued: true
    alias: termsAndConditions
    owner: Catalogue
    domain_of:
    - DataProduct
    - GaiaXTermsAndConditions
    - Issuer
    - ServiceOffering
    range: TermsAndConditions
    required: true
  policy:
    name: policy
    description: One or more policies expressed using a DSL (e.g., Rego or ODRL).
    title: policy
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: policy
    owner: Catalogue
    domain_of:
    - ServiceOffering
    range: string
    required: true
  dataProtectionRegime:
    name: dataProtectionRegime
    description: One or more data protection regimes.
    title: data protection regime
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: dataProtectionRegime
    owner: Catalogue
    domain_of:
    - ServiceOffering
    range: PersonalDataProtectionRegime
    required: false
  dataAccountExport:
    name: dataAccountExport
    description: One or more  methods to export data out of the service.
    title: data account export
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: dataAccountExport
    owner: Catalogue
    domain_of:
    - ServiceOffering
    range: DataAccountExport
    required: true
  description:
    name: description
    description: A description in natural language.
    title: description
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    alias: description
    owner: Catalogue
    domain_of:
    - DataProduct
    - Resource
    - ServiceOffering
    range: string
    required: false
  keyword:
    name: keyword
    description: Keywords that describe / tag the service.
    title: keyword
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: keyword
    owner: Catalogue
    domain_of:
    - ServiceOffering
    range: string
    required: false
  provisionType:
    name: provisionType
    description: Provision type of the service
    title: provision type
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: provisionType
    owner: Catalogue
    domain_of:
    - ServiceOffering
    range: string
    required: false
  endpoint:
    name: endpoint
    description: Endpoint through which the Service Offering can be accessed.
    title: endpoint
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: endpoint
    owner: Catalogue
    domain_of:
    - ServiceOffering
    range: Endpoint
    required: false
  hostedOn:
    name: hostedOn
    description: List of Resource references where service is hosted and can be instantiated.
      Can refer to availabilty zones, data centers, regions, etc.
    title: hosted on
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: hostedOn
    owner: Catalogue
    domain_of:
    - ServiceOffering
    range: string
    required: false
    any_of:
    - range: Resource
    - range: ServiceOffering
  dependsOn:
    name: dependsOn
    description: A resolvable link to the service offering self-description related
      to the service and that can exist independently of it.
    title: depends on
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: dependsOn
    owner: Catalogue
    domain_of:
    - ServiceOffering
    range: ServiceOffering
    required: false
  aggregationOf:
    name: aggregationOf
    description: A resolvable link to the resources self-description related to the
      service and that can exist independently of it.
    title: aggregation of
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    multivalued: true
    alias: aggregationOf
    owner: Catalogue
    domain_of:
    - DataProduct
    - Resource
    - ServiceOffering
    range: string
    required: false
    any_of:
    - range: Resource
    - range: ServiceOffering
tree_root: true

```
</details>