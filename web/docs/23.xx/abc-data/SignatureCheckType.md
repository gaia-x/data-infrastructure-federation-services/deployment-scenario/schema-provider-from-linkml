# Class: SignatureCheckType



URI: [abc-data:SignatureCheckType](https://$BASE_URL$/abc-data#SignatureCheckType)




```mermaid
 classDiagram
    class SignatureCheckType
      GaiaXEntity <|-- SignatureCheckType
      
      SignatureCheckType : legalValidity
        
      SignatureCheckType : mandatory
        
      SignatureCheckType : participantRole
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **SignatureCheckType**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [participantRole](participantRole.md) | 1..1 <br/> [String](String.md) | Establish a unique way to identify the participant that has to Sign(e | direct |
| [mandatory](mandatory.md) | 1..1 <br/> [String](String.md) | Establish the if a Signature is mandatory or Optional | direct |
| [legalValidity](legalValidity.md) | 1..1 <br/> [String](String.md) | Establish the if the Legal validity check needs to be enforced to the Signatu... | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [DataProductUsageContract](DataProductUsageContract.md) | [signers](signers.md) | range | [SignatureCheckType](SignatureCheckType.md) |
| [DataUsageAgreement](DataUsageAgreement.md) | [signers](signers.md) | range | [SignatureCheckType](SignatureCheckType.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | abc-data:SignatureCheckType |
| native | https://$BASE_URL$/abc-data/:SignatureCheckType |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: SignatureCheckType
from_schema: https://$BASE_URL$/abc-data
is_a: GaiaXEntity
slots:
- participantRole
- mandatory
- legalValidity
slot_usage:
  participantRole:
    name: participantRole
    domain_of:
    - SignatureCheckType
    required: true
  mandatory:
    name: mandatory
    domain_of:
    - SignatureCheckType
    required: true
  legalValidity:
    name: legalValidity
    domain_of:
    - SignatureCheckType
    required: true
class_uri: abc-data:SignatureCheckType

```
</details>

### Induced

<details>
```yaml
name: SignatureCheckType
from_schema: https://$BASE_URL$/abc-data
is_a: GaiaXEntity
slot_usage:
  participantRole:
    name: participantRole
    domain_of:
    - SignatureCheckType
    required: true
  mandatory:
    name: mandatory
    domain_of:
    - SignatureCheckType
    required: true
  legalValidity:
    name: legalValidity
    domain_of:
    - SignatureCheckType
    required: true
attributes:
  participantRole:
    name: participantRole
    description: Establish a unique way to identify the participant that has to Sign(e.g.
      gx:providedBy is identified by Provider ). Possible values are Provider, Consumer,
      Licensor, Producer
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:participantRole
    alias: participantRole
    owner: SignatureCheckType
    domain_of:
    - SignatureCheckType
    range: string
    required: true
  mandatory:
    name: mandatory
    description: Establish the if a Signature is mandatory or Optional. Possible values
      are Yes/No
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:mandatory
    alias: mandatory
    owner: SignatureCheckType
    domain_of:
    - SignatureCheckType
    range: string
    required: true
  legalValidity:
    name: legalValidity
    description: Establish the if the Legal validity check needs to be enforced to
      the Signature. Possible values are Yes/No
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:legalValidity
    alias: legalValidity
    owner: SignatureCheckType
    domain_of:
    - SignatureCheckType
    range: string
    required: true
class_uri: abc-data:SignatureCheckType

```
</details>