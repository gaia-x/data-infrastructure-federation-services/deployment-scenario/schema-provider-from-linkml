# Slot: hostedOn


_List of Resource references where service is hosted and can be instantiated. Can refer to availabilty zones, data centers, regions, etc._



URI: [https://$BASE_URL$/abc-data/:hostedOn](https://$BASE_URL$/abc-data/:hostedOn)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: hostedOn
description: List of Resource references where service is hosted and can be instantiated.
  Can refer to availabilty zones, data centers, regions, etc.
title: hosted on
from_schema: https://$BASE_URL$/abc-data
rank: 1000
multivalued: true
alias: hostedOn
owner: ServiceOffering
domain_of:
- ServiceOffering
range: string
required: false
any_of:
- range: Resource
- range: ServiceOffering

```
</details>