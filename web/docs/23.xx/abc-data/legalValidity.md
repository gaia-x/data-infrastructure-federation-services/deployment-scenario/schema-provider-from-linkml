# Slot: legalValidity


_Establish the if the Legal validity check needs to be enforced to the Signature. Possible values are Yes/No_



URI: [abc-data:legalValidity](https://$BASE_URL$/abc-data#legalValidity)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SignatureCheckType](SignatureCheckType.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: legalValidity
description: Establish the if the Legal validity check needs to be enforced to the
  Signature. Possible values are Yes/No
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:legalValidity
alias: legalValidity
domain_of:
- SignatureCheckType
range: string

```
</details>