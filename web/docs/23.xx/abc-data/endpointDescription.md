# Slot: endpointDescription


_The Description (e.g. openAPI Description) of the endpoint._



URI: [https://$BASE_URL$/abc-data/:endpointDescription](https://$BASE_URL$/abc-data/:endpointDescription)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Endpoint](Endpoint.md) | An endpoint is a mean to access and interact with a service or a resource |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: endpointDescription
description: The Description (e.g. openAPI Description) of the endpoint.
title: endpoint description
from_schema: https://$BASE_URL$/abc-data
rank: 1000
multivalued: true
alias: endpointDescription
owner: Endpoint
domain_of:
- Endpoint
range: string
required: false

```
</details>