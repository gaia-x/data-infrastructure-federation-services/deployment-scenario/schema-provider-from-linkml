# Slot: aggregationOf


_DataSet Content_



URI: [abc-data:aggregationOf](https://$BASE_URL$/abc-data#aggregationOf)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  yes  |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |
[Resource](Resource.md) | A resource that may be aggregated in a Service Offering or exist independentl... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [DataSet](DataSet.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: aggregationOf
description: DataSet Content
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:aggregationOf
multivalued: true
alias: aggregationOf
domain_of:
- DataProduct
- Resource
- ServiceOffering
range: DataSet

```
</details>