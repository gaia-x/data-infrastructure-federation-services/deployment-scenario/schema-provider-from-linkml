# Slot: dataUsageAgreementTrustAnchor


_A resolvable link to the Data Usage Agreement Trust Anchor._



URI: [abc-data:dataUsageAgreementTrustAnchor](https://$BASE_URL$/abc-data#dataUsageAgreementTrustAnchor)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataUsageAgreement](DataUsageAgreement.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: dataUsageAgreementTrustAnchor
description: A resolvable link to the Data Usage Agreement Trust Anchor.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:dataUsageAgreementTrustAnchor
alias: dataUsageAgreementTrustAnchor
domain_of:
- DataUsageAgreement
range: string

```
</details>