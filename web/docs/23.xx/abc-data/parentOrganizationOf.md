# Slot: parentOrganizationOf


_A list of direct participant that this entity is a subOrganization of, if any._



URI: [https://$BASE_URL$/abc-data/:parentOrganizationOf](https://$BASE_URL$/abc-data/:parentOrganizationOf)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Relationship](Relationship.md) | A relationship between two organisations |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: parentOrganizationOf
description: A list of direct participant that this entity is a subOrganization of,
  if any.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
alias: parentOrganizationOf
owner: Relationship
domain_of:
- Relationship
range: LegalPerson

```
</details>