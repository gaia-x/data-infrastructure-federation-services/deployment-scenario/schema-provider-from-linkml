# Slot: exposedThrough


_A resolvable link to the data exchange component that exposes the Data Product._



URI: [abc-data:exposedThrough](https://$BASE_URL$/abc-data#exposedThrough)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataSet](DataSet.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: exposedThrough
description: A resolvable link to the data exchange component that exposes the Data
  Product.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:exposedThrough
alias: exposedThrough
domain_of:
- DataSet
range: string

```
</details>