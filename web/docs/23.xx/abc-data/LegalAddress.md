# Slot: legalAddress


_The full legal address of the organization._



URI: [https://$BASE_URL$/abc-data/:legalAddress](https://$BASE_URL$/abc-data/:legalAddress)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProvider](DataProvider.md) | is equivalent to Gaia-X Provider |  no  |
[DataLicensor](DataLicensor.md) | is equivalent to Gaia-X Licensor |  no  |
[DataProducer](DataProducer.md) | is equivalent to Gaia-X RessourceOwner |  no  |
[DataConsumer](DataConsumer.md) | is equivalent to Gaia-X Consumer |  no  |
[Federator](Federator.md) | is equivalent to Gaia-X Federator |  no  |
[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number |  no  |







## Properties

* Range: [LegalAddress](LegalAddress.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: legalAddress
description: The full legal address of the organization.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
alias: legalAddress
owner: LegalPerson
domain_of:
- LegalPerson
range: LegalAddress
required: true

```
</details>