# Slot: URL


_A resolvable link to document._



URI: [https://$BASE_URL$/abc-data/:URL](https://$BASE_URL$/abc-data/:URL)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[TermsAndConditions](TermsAndConditions.md) | Terms and Conditions applying to a service offering |  no  |







## Properties

* Range: [Uri](Uri.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: URL
description: A resolvable link to document.
title: url
from_schema: https://$BASE_URL$/abc-data
rank: 1000
multivalued: false
alias: URL
owner: TermsAndConditions
domain_of:
- TermsAndConditions
range: uri
required: true

```
</details>