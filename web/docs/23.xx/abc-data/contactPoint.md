# Slot: contactPoint


_Contacts to get more information_



URI: [dcat:contactPoint](http://www.w3.org/ns/dcat#contactPoint)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: contactPoint
description: Contacts to get more information
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: dcat:contactPoint
alias: contactPoint
domain_of:
- DataProduct
range: string

```
</details>