# Slot: signers


_The array identifying all required Participant signature_



URI: [abc-data:signers](https://$BASE_URL$/abc-data#signers)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProductUsageContract](DataProductUsageContract.md) |  |  yes  |
[DataUsageAgreement](DataUsageAgreement.md) |  |  yes  |







## Properties

* Range: [SignatureCheckType](SignatureCheckType.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: signers
description: The array identifying all required Participant signature
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:signers
multivalued: true
alias: signers
domain_of:
- DataProductUsageContract
- DataUsageAgreement
range: SignatureCheckType

```
</details>