# Slot: participantRole


_Establish a unique way to identify the participant that has to Sign(e.g. gx:providedBy is identified by Provider ). Possible values are Provider, Consumer, Licensor, Producer_



URI: [abc-data:participantRole](https://$BASE_URL$/abc-data#participantRole)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SignatureCheckType](SignatureCheckType.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: participantRole
description: Establish a unique way to identify the participant that has to Sign(e.g.
  gx:providedBy is identified by Provider ). Possible values are Provider, Consumer,
  Licensor, Producer
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:participantRole
alias: participantRole
domain_of:
- SignatureCheckType
range: string

```
</details>