# Slot: conformsTo


_An established standard to which the described resource conforms._



URI: [dct:conformsTo](http://purl.org/dc/terms/conformsTo)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: conformsTo
description: An established standard to which the described resource conforms.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: dct:conformsTo
alias: conformsTo
domain_of:
- DataProduct
range: string

```
</details>