# abc-data



URI: https://$BASE_URL$/abc-data

Name: abc-data



## Classes

| Class | Description |
| --- | --- |
| [Address](Address.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[HeadquarterAddress](HeadquarterAddress.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[LegalAddress](LegalAddress.md) | None |
| [DataAccountExport](DataAccountExport.md) | List of methods to export data from your account out of the service. |
| [DataUsageAgreement](DataUsageAgreement.md) | None |
| [Endpoint](Endpoint.md) | An endpoint is a mean to access and interact with a service or a resource. |
| [GaiaXEntity](GaiaXEntity.md) | Root class for Gaia-X entities. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[DataProductUsageContract](DataProductUsageContract.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[DataSet](DataSet.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[DataUsage](DataUsage.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Distribution](Distribution.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Issuer](Issuer.md) | Each issuer shall issue a GaiaXTermsAndCondition verifiable credential. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Participant](Participant.md) | An legal or natural person that is onboarded to Gaia-X and offers, consumes services or operates resources. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[DataConsumer](DataConsumer.md) | is equivalent to Gaia-X Consumer |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[DataLicensor](DataLicensor.md) | is equivalent to Gaia-X Licensor |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[DataProducer](DataProducer.md) | is equivalent to Gaia-X RessourceOwner |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[DataProvider](DataProvider.md) | is equivalent to Gaia-X Provider |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Federator](Federator.md) | is equivalent to Gaia-X Federator |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Resource](Resource.md) | A resource that may be aggregated in a Service Offering or exist independently of it. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal service, a warehouse, a plant. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, filter services and resources. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[DataProduct](DataProduct.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[SignatureCheckType](SignatureCheckType.md) | None |
| [GaiaXTermsAndConditions](GaiaXTermsAndConditions.md) | Gaia-X terms and conditions, each issuer has to aggree. |
| [RegistrationNumber](RegistrationNumber.md) | Country's registration number, which identifies one specific entity. Allowed entries are Local, VatID, lei code, EODI, and EUID. |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[EORI](EORI.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[EUID](EUID.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[LeiCode](LeiCode.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[LocalRegistrationNumber](LocalRegistrationNumber.md) | None |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[VatID](VatID.md) | None |
| [Relationship](Relationship.md) | A relationship between two organisations. |
| [StandardConformity](StandardConformity.md) | Details about standard applied to entities. |
| [TermsAndConditions](TermsAndConditions.md) | Terms and Conditions applying to a service offering. |



## Slots

| Slot | Description |
| --- | --- |
| [accessType](accessType.md) | Type of data support: digital, physical |
| [aggregationOf](aggregationOf.md) | DataSet Content |
| [byteSize](byteSize.md) | Size of the dataset distribution |
| [compressFormat](compressFormat.md) | The compression format of the distribution in which the data is contained in ... |
| [conformsTo](conformsTo.md) | An established standard to which the described resource conforms |
| [consumedBy](consumedBy.md) | A resolvable link to the Data Consumer Delaration |
| [contactPoint](contactPoint.md) | Contacts to get more information |
| [country_name](country_name.md) | Physical location of head quarter in ISO 3166-1 alpha2, alpha-3 or numeric fo... |
| [dataAccountExport](dataAccountExport.md) | One or more  methods to export data out of the service |
| [dataLicensors](dataLicensors.md) | A list of Licensors either as a free form string or participant self-descript... |
| [dataProduct](dataProduct.md) | A resolvable link to the Data Product Description Declaration (after negotiat... |
| [dataProtectionRegime](dataProtectionRegime.md) | One or more data protection regimes |
| [dataUsage](dataUsage.md) | A resolvable link to Data Usage |
| [dataUsageAgreement](dataUsageAgreement.md) | List of authorizations from the data subjects as Natural Person when the data... |
| [dataUsageAgreementTrustAnchor](dataUsageAgreementTrustAnchor.md) | A resolvable link to the Data Usage Agreement Trust Anchor |
| [dependsOn](dependsOn.md) | A resolvable link to the service offering self-description related to the ser... |
| [description](description.md) | Description of the Data Product |
| [distributions](distributions.md) | List of distributions format of the dataset |
| [endpoint](endpoint.md) | Endpoint through which the Service Offering can be accessed |
| [endpointDescription](endpointDescription.md) | The Description (e |
| [endpointURL](endpointURL.md) | The URL of the endpoint where it can be accessed |
| [expirationDateTime](expirationDateTime.md) | Date time in ISO 8601 format after which data is expired and shall be deleted |
| [exposedThrough](exposedThrough.md) | A resolvable link to the data exchange component that exposes the Data Produc... |
| [format](format.md) | Format of the dataset distribution (pdf, csv, …) |
| [formatType](formatType.md) | Type of Media Types (formerly known as MIME types) as defined by the IANA |
| [getVerifiableCredentialsIDs](getVerifiableCredentialsIDs.md) | a route used to synchronize catalogues and retrieve the list of Verifiable Cr... |
| [gps](gps.md) | GPS in ISO 6709:2008/Cor 1:2009 format |
| [hash](hash.md) | To uniquely identify the data contained in the dataset distribution |
| [hashAlgorithm](hashAlgorithm.md) | Hash Algorithm |
| [hasPolicy](hasPolicy.md) | a list of policy expressed using ODRL |
| [headquarterAddress](headquarterAddress.md) | Full physical location of the headquarter of the organization |
| [hostedOn](hostedOn.md) | List of Resource references where service is hosted and can be instantiated |
| [identifier](identifier.md) | Unique uuid4 |
| [issued](issued.md) | Publication date in ISO 8601 format |
| [keyword](keyword.md) | Keywords that describe / tag the service |
| [language](language.md) | Language |
| [legalAddress](legalAddress.md) | The full legal address of the organization |
| [legalValidity](legalValidity.md) | Establish the if the Legal validity check needs to be enforced to the Signatu... |
| [license](license.md) | A list of URIs to license document |
| [licensedBy](licensedBy.md) | A resolvable links to Data Licensors |
| [locality](locality.md) | The v:locality property specifies the locality (e |
| [location](location.md) | List of dataset storage location |
| [locationAddress](locationAddress.md) | A vcard:Address object that contains the physical location in ISO 3166-1 alph... |
| [locationGPS](locationGPS.md) | A list of physical GPS in ISO 6709:2008/Cor 1:2009 format |
| [loggingService](loggingService.md) | Link to the Logging Service |
| [maintainedBy](maintainedBy.md) | Participant maintaining the resource in operational condition and thus have p... |
| [mandatory](mandatory.md) | Establish the if a Signature is mandatory or Optional |
| [manufacturedBy](manufacturedBy.md) | Participant manufacturing the resource |
| [name](name.md) | A human readable name of the data resource |
| [notarizedIn](notarizedIn.md) | A resolvable link to the Notarization service |
| [obsoleteDateTime](obsoleteDateTime.md) | Date time in ISO 8601 format after which Data Product is obsolete |
| [ownedBy](ownedBy.md) | Participant owning the resource |
| [packageFormat](packageFormat.md) | The package format of the distribution in which one or more data files are gr... |
| [parentOrganizationOf](parentOrganizationOf.md) | A list of direct participant that this entity is a subOrganization of, if any |
| [participantRole](participantRole.md) | Establish a unique way to identify the participant that has to Sign(e |
| [policy](policy.md) | One or more policies expressed using a DSL (e |
| [postal_code](postal_code.md) | String of a street-address |
| [producedBy](producedBy.md) | A resolvable link to the Data Producer |
| [providedBy](providedBy.md) | A resolvable link to the participant self-description providing the service |
| [provisionType](provisionType.md) | Provision type of the service |
| [publisher](publisher.md) | Publisher of the standard |
| [registrationNumber](registrationNumber.md) | Country's registration number, which identifies one specific entity |
| [relatedOrganizations](relatedOrganizations.md) | A list of related organization, either as sub or parent organization, if any |
| [requestType](requestType.md) | The mean to request data retrieval: API, email, webform, unregisteredLetter, ... |
| [signers](signers.md) | The array identifying all required Participant signature |
| [standardConformity](standardConformity.md) | Provides information about applied standards |
| [standardReference](standardReference.md) | Provides a link to schemas or details about applied standards |
| [street_address](street_address.md) | The v:street-address property specifies the street address of a postal addres... |
| [subOrganisationOf](subOrganisationOf.md) | A direct participant with a legal mandate on this entity, e |
| [termOfUsage](termOfUsage.md) | A resolvable link to the Term of Usage |
| [termsAndConditions](termsAndConditions.md) | A resolvable link to the Terms and Conditions applying to that service |
| [title](title.md) | Title of the Data Product |
| [URL](URL.md) | A resolvable link to document |
| [value](value.md) | The state issued company number |


## Enumerations

| Enumeration | Description |
| --- | --- |
| [AccessTypeMeans](AccessTypeMeans.md) |  |
| [PersonalDataProtectionRegime](PersonalDataProtectionRegime.md) |  |
| [RequestTypeMeans](RequestTypeMeans.md) |  |


## Types

| Type | Description |
| --- | --- |
| [Boolean](Boolean.md) | A binary (true or false) value |
| [Curie](Curie.md) | a compact URI |
| [Date](Date.md) | a date (year, month and day) in an idealized calendar |
| [DateOrDatetime](DateOrDatetime.md) | Either a date or a datetime |
| [Datetime](Datetime.md) | The combination of a date and time |
| [Decimal](Decimal.md) | A real number with arbitrary precision that conforms to the xsd:decimal speci... |
| [Double](Double.md) | A real number that conforms to the xsd:double specification |
| [Float](Float.md) | A real number that conforms to the xsd:float specification |
| [Integer](Integer.md) | An integer |
| [Jsonpath](Jsonpath.md) | A string encoding a JSON Path |
| [Jsonpointer](Jsonpointer.md) | A string encoding a JSON Pointer |
| [Ncname](Ncname.md) | Prefix part of CURIE |
| [Nodeidentifier](Nodeidentifier.md) | A URI, CURIE or BNODE that represents a node in a model |
| [Objectidentifier](Objectidentifier.md) | A URI or CURIE that represents an object in the model |
| [Sparqlpath](Sparqlpath.md) | A string encoding a SPARQL Property Path |
| [String](String.md) | A character string |
| [Time](Time.md) | A time object represents a (local) time of day, independent of any particular... |
| [Uri](Uri.md) | a complete URI |
| [Uriorcurie](Uriorcurie.md) | a URI or a CURIE |


## Subsets

| Subset | Description |
| --- | --- |
