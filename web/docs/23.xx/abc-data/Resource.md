# Class: Resource


_A resource that may be aggregated in a Service Offering or exist independently of it._





URI: [gx:Resource](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#Resource)




```mermaid
 classDiagram
    class Resource
      GaiaXEntity <|-- Resource
      

      Resource <|-- PhysicalResource
      
      
      Resource : aggregationOf
        
          Resource --|> Resource : aggregationOf
        
      Resource : description
        
      Resource : name
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **Resource**
        * [PhysicalResource](PhysicalResource.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [name](name.md) | 0..1 <br/> [String](String.md) | A human readable name of the data resource | direct |
| [description](description.md) | 0..1 <br/> [String](String.md) | A free text description of the data resource | direct |
| [aggregationOf](aggregationOf.md) | 0..* <br/> [Resource](Resource.md) | Resources related to the resource and that can exist independently of it | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [PhysicalResource](PhysicalResource.md) | [aggregationOf](aggregationOf.md) | range | [Resource](Resource.md) |
| [Resource](Resource.md) | [aggregationOf](aggregationOf.md) | range | [Resource](Resource.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:Resource |
| native | https://$BASE_URL$/abc-data/:Resource |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Resource
description: A resource that may be aggregated in a Service Offering or exist independently
  of it.
from_schema: https://$BASE_URL$/abc-data
is_a: GaiaXEntity
attributes:
  name:
    name: name
    description: A human readable name of the data resource.
    title: name
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: false
  description:
    name: description
    description: A free text description of the data resource.
    title: description
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    range: string
    required: false
  aggregationOf:
    name: aggregationOf
    description: Resources related to the resource and that can exist independently
      of it.
    title: aggregation of
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    multivalued: true
    range: Resource
    required: false
class_uri: gx:Resource

```
</details>

### Induced

<details>
```yaml
name: Resource
description: A resource that may be aggregated in a Service Offering or exist independently
  of it.
from_schema: https://$BASE_URL$/abc-data
is_a: GaiaXEntity
attributes:
  name:
    name: name
    description: A human readable name of the data resource.
    title: name
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: name
    owner: Resource
    domain_of:
    - Resource
    - ServiceOffering
    range: string
    required: false
  description:
    name: description
    description: A free text description of the data resource.
    title: description
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    alias: description
    owner: Resource
    domain_of:
    - DataProduct
    - Resource
    - ServiceOffering
    range: string
    required: false
  aggregationOf:
    name: aggregationOf
    description: Resources related to the resource and that can exist independently
      of it.
    title: aggregation of
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    multivalued: true
    alias: aggregationOf
    owner: Resource
    domain_of:
    - DataProduct
    - Resource
    - ServiceOffering
    range: Resource
    required: false
class_uri: gx:Resource

```
</details>