# Slot: location


_List of dataset storage location_



URI: [abc-data:location](https://$BASE_URL$/abc-data#location)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: location
description: List of dataset storage location
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:location
alias: location
domain_of:
- Distribution
range: string

```
</details>