# Slot: termOfUsage


_A resolvable link to the Term of Usage._



URI: [abc-data:termOfUsage](https://$BASE_URL$/abc-data#termOfUsage)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProductUsageContract](DataProductUsageContract.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: termOfUsage
description: A resolvable link to the Term of Usage.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:termOfUsage
alias: termOfUsage
domain_of:
- DataProductUsageContract
range: string

```
</details>