# Slot: description


_Description of the Data Product_



URI: [dct:description](http://purl.org/dc/terms/description)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |
[Resource](Resource.md) | A resource that may be aggregated in a Service Offering or exist independentl... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: description
description: Description of the Data Product
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: dct:description
alias: description
domain_of:
- DataProduct
- Resource
- ServiceOffering
range: string

```
</details>