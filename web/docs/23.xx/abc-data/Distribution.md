# Class: Distribution



URI: [dcat:Distribution](http://www.w3.org/ns/dcat#Distribution)




```mermaid
 classDiagram
    class Distribution
      GaiaXEntity <|-- Distribution
      
      Distribution : byteSize
        
      Distribution : compressFormat
        
      Distribution : dataLicensors
        
          Distribution --|> DataLicensor : dataLicensors
        
      Distribution : dataUsageAgreement
        
          Distribution --|> DataUsageAgreement : dataUsageAgreement
        
      Distribution : expirationDateTime
        
      Distribution : format
        
      Distribution : hash
        
      Distribution : hashAlgorithm
        
      Distribution : issued
        
      Distribution : language
        
      Distribution : license
        
      Distribution : location
        
      Distribution : packageFormat
        
      Distribution : title
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **Distribution**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [title](title.md) | 1..1 <br/> [String](String.md) | Title of the Data Product | direct |
| [format](format.md) | 1..1 <br/> [String](String.md) | Format of the dataset distribution (pdf, csv, …) | direct |
| [compressFormat](compressFormat.md) | 0..1 <br/> [String](String.md) | The compression format of the distribution in which the data is contained in ... | direct |
| [packageFormat](packageFormat.md) | 0..1 <br/> [String](String.md) | The package format of the distribution in which one or more data files are gr... | direct |
| [byteSize](byteSize.md) | 0..1 <br/> [String](String.md) | Size of the dataset distribution | direct |
| [location](location.md) | 0..1 <br/> [String](String.md) | List of dataset storage location | direct |
| [hash](hash.md) | 0..1 <br/> [String](String.md) | To uniquely identify the data contained in the dataset distribution | direct |
| [hashAlgorithm](hashAlgorithm.md) | 0..1 <br/> [String](String.md) | Hash Algorithm | direct |
| [issued](issued.md) | 0..1 <br/> [Date](Date.md) | Publication date in ISO 8601 format | direct |
| [expirationDateTime](expirationDateTime.md) | 0..1 <br/> [Datetime](Datetime.md) | Date time in ISO 8601 format after which data is expired and shall be deleted | direct |
| [language](language.md) | 0..1 <br/> [String](String.md) | Language | direct |
| [license](license.md) | 0..* <br/> [String](String.md) | A list of URIs to license document | direct |
| [dataLicensors](dataLicensors.md) | 0..1 <br/> [DataLicensor](DataLicensor.md) | A list of Licensors either as a free form string or participant self-descript... | direct |
| [dataUsageAgreement](dataUsageAgreement.md) | 0..* <br/> [DataUsageAgreement](DataUsageAgreement.md) | List of authorizations from the data subjects as Natural Person when the data... | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [DataSet](DataSet.md) | [distributions](distributions.md) | range | [Distribution](Distribution.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | dcat:Distribution |
| native | https://$BASE_URL$/abc-data/:Distribution |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Distribution
from_schema: https://$BASE_URL$/abc-data
is_a: GaiaXEntity
slots:
- title
- format
- compressFormat
- packageFormat
- byteSize
- location
- hash
- hashAlgorithm
- issued
- expirationDateTime
- language
- license
- dataLicensors
- dataUsageAgreement
slot_usage:
  title:
    name: title
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    - StandardConformity
    required: true
  format:
    name: format
    domain_of:
    - Distribution
    required: true
class_uri: dcat:Distribution

```
</details>

### Induced

<details>
```yaml
name: Distribution
from_schema: https://$BASE_URL$/abc-data
is_a: GaiaXEntity
slot_usage:
  title:
    name: title
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    - StandardConformity
    required: true
  format:
    name: format
    domain_of:
    - Distribution
    required: true
attributes:
  title:
    name: title
    description: Title of the Data Product
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:title
    alias: title
    owner: Distribution
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    - StandardConformity
    range: string
    required: true
  format:
    name: format
    description: Format of the dataset distribution (pdf, csv, …)
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:format
    alias: format
    owner: Distribution
    domain_of:
    - Distribution
    range: string
    required: true
  compressFormat:
    name: compressFormat
    description: The compression format of the distribution in which the data is contained
      in a compressed form
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dcat:compressFormat
    alias: compressFormat
    owner: Distribution
    domain_of:
    - Distribution
    range: string
  packageFormat:
    name: packageFormat
    description: The package format of the distribution in which one or more data
      files are grouped together
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dcat:packageFormat
    alias: packageFormat
    owner: Distribution
    domain_of:
    - Distribution
    range: string
  byteSize:
    name: byteSize
    description: Size of the dataset distribution
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dcat:byteSize
    alias: byteSize
    owner: Distribution
    domain_of:
    - Distribution
    range: string
  location:
    name: location
    description: List of dataset storage location
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:location
    alias: location
    owner: Distribution
    domain_of:
    - Distribution
    range: string
  hash:
    name: hash
    description: To uniquely identify the data contained in the dataset distribution
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:hash
    alias: hash
    owner: Distribution
    domain_of:
    - Distribution
    - TermsAndConditions
    range: string
  hashAlgorithm:
    name: hashAlgorithm
    description: Hash Algorithm
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:hashAlgorithm
    alias: hashAlgorithm
    owner: Distribution
    domain_of:
    - Distribution
    range: string
  issued:
    name: issued
    description: Publication date in ISO 8601 format
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:issued
    alias: issued
    owner: Distribution
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    range: date
    pattern: ^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])$
  expirationDateTime:
    name: expirationDateTime
    description: Date time in ISO 8601 format after which data is expired and shall
      be deleted.
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:expirationDateTime
    alias: expirationDateTime
    owner: Distribution
    domain_of:
    - DataSet
    - Distribution
    range: datetime
    pattern: ^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|-0[1-9]|-1\d|-2[0-3]|-00:?(?:0[1-9]|[1-5]\d)|\+[01]\d|\+2[0-3])(?:|:?[0-5]\d)$
  language:
    name: language
    description: Language
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dcat:language
    alias: language
    owner: Distribution
    domain_of:
    - Distribution
    range: string
  license:
    name: license
    description: A list of URIs to license document.
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:license
    multivalued: true
    alias: license
    owner: Distribution
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    range: string
  dataLicensors:
    name: dataLicensors
    description: A list of Licensors either as a free form string or participant self-description.
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:dataLicensors
    alias: dataLicensors
    owner: Distribution
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    range: DataLicensor
  dataUsageAgreement:
    name: dataUsageAgreement
    description: List of authorizations from the data subjects as Natural Person when
      the dataset contains PII, as defined by the Trust Framework
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:dataUsageAgreement
    multivalued: true
    alias: dataUsageAgreement
    owner: Distribution
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    range: DataUsageAgreement
class_uri: dcat:Distribution

```
</details>