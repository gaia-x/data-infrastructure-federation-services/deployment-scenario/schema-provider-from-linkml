# Slot: byteSize


_Size of the dataset distribution_



URI: [dcat:byteSize](http://www.w3.org/ns/dcat#byteSize)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: byteSize
description: Size of the dataset distribution
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: dcat:byteSize
alias: byteSize
domain_of:
- Distribution
range: string

```
</details>