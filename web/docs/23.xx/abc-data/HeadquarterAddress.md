# Slot: headquarterAddress


_Full physical location of the headquarter of the organization._



URI: [https://$BASE_URL$/abc-data/:headquarterAddress](https://$BASE_URL$/abc-data/:headquarterAddress)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProvider](DataProvider.md) | is equivalent to Gaia-X Provider |  no  |
[DataLicensor](DataLicensor.md) | is equivalent to Gaia-X Licensor |  no  |
[DataProducer](DataProducer.md) | is equivalent to Gaia-X RessourceOwner |  no  |
[DataConsumer](DataConsumer.md) | is equivalent to Gaia-X Consumer |  no  |
[Federator](Federator.md) | is equivalent to Gaia-X Federator |  no  |
[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number |  no  |







## Properties

* Range: [HeadquarterAddress](HeadquarterAddress.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: headquarterAddress
description: Full physical location of the headquarter of the organization.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
alias: headquarterAddress
owner: LegalPerson
domain_of:
- LegalPerson
range: HeadquarterAddress
required: true

```
</details>