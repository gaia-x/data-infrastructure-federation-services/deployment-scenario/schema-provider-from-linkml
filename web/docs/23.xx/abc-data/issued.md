# Slot: issued


_Publication date in ISO 8601 format_



URI: [dct:issued](http://purl.org/dc/terms/issued)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |
[DataSet](DataSet.md) |  |  no  |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [Date](Date.md)

* Regex pattern: `^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])$`





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: issued
description: Publication date in ISO 8601 format
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: dct:issued
alias: issued
domain_of:
- DataProduct
- DataSet
- Distribution
range: date
pattern: ^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])$

```
</details>