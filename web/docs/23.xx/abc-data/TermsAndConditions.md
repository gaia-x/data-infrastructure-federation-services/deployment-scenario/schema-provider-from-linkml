# Slot: termsAndConditions


_A resolvable link to the Terms and Conditions applying to that service._



URI: [abc-data:termsAndConditions](https://$BASE_URL$/abc-data#termsAndConditions)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  yes  |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[GaiaXTermsAndConditions](GaiaXTermsAndConditions.md) | Gaia-X terms and conditions, each issuer has to aggree |  no  |
[Issuer](Issuer.md) | Each issuer shall issue a GaiaXTermsAndCondition verifiable credential |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: termsAndConditions
description: A resolvable link to the Terms and Conditions applying to that service.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:termsAndConditions
alias: termsAndConditions
domain_of:
- DataProduct
- GaiaXTermsAndConditions
- Issuer
- ServiceOffering
range: string

```
</details>