# Slot: expirationDateTime


_Date time in ISO 8601 format after which data is expired and shall be deleted._



URI: [abc-data:expirationDateTime](https://$BASE_URL$/abc-data#expirationDateTime)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataSet](DataSet.md) |  |  no  |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [Datetime](Datetime.md)

* Regex pattern: `^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|-0[1-9]|-1\d|-2[0-3]|-00:?(?:0[1-9]|[1-5]\d)|\+[01]\d|\+2[0-3])(?:|:?[0-5]\d)$`





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: expirationDateTime
description: Date time in ISO 8601 format after which data is expired and shall be
  deleted.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:expirationDateTime
alias: expirationDateTime
domain_of:
- DataSet
- Distribution
range: datetime
pattern: ^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|-0[1-9]|-1\d|-2[0-3]|-00:?(?:0[1-9]|[1-5]\d)|\+[01]\d|\+2[0-3])(?:|:?[0-5]\d)$

```
</details>