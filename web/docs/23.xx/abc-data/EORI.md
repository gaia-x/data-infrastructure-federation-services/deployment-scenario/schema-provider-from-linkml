# Class: EORI



URI: [gx:EORI](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#EORI)




```mermaid
 classDiagram
    class EORI
      RegistrationNumber <|-- EORI
      
      EORI : value
        
      
```





## Inheritance
* [RegistrationNumber](RegistrationNumber.md)
    * **EORI**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [value](value.md) | 1..1 <br/> [String](String.md) | The Economic Operators Registration and Identification number (EORI) | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:EORI |
| native | https://$BASE_URL$/abc-data/:EORI |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: EORI
from_schema: https://$BASE_URL$/abc-data
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: The Economic Operators Registration and Identification number (EORI).
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    identifier: true
    range: string
    required: true
class_uri: gx:EORI

```
</details>

### Induced

<details>
```yaml
name: EORI
from_schema: https://$BASE_URL$/abc-data
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: The Economic Operators Registration and Identification number (EORI).
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    identifier: true
    alias: value
    owner: EORI
    domain_of:
    - LocalRegistrationNumber
    - VatID
    - LeiCode
    - EORI
    - EUID
    range: string
    required: true
class_uri: gx:EORI

```
</details>