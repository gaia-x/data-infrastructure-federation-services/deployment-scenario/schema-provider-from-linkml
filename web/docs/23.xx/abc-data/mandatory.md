# Slot: mandatory


_Establish the if a Signature is mandatory or Optional. Possible values are Yes/No_



URI: [abc-data:mandatory](https://$BASE_URL$/abc-data#mandatory)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[SignatureCheckType](SignatureCheckType.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: mandatory
description: Establish the if a Signature is mandatory or Optional. Possible values
  are Yes/No
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:mandatory
alias: mandatory
domain_of:
- SignatureCheckType
range: string

```
</details>