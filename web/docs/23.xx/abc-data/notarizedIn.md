# Slot: notarizedIn


_A resolvable link to the Notarization service_



URI: [https://$BASE_URL$/abc-data/:notarizedIn](https://$BASE_URL$/abc-data/:notarizedIn)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProductUsageContract](DataProductUsageContract.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: notarizedIn
description: A resolvable link to the Notarization service
from_schema: https://$BASE_URL$/abc-data
rank: 1000
alias: notarizedIn
domain_of:
- DataProductUsageContract
range: string

```
</details>