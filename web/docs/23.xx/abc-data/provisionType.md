# Slot: provisionType


_Provision type of the service_



URI: [https://$BASE_URL$/abc-data/:provisionType](https://$BASE_URL$/abc-data/:provisionType)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: provisionType
description: Provision type of the service
title: provision type
from_schema: https://$BASE_URL$/abc-data
rank: 1000
alias: provisionType
owner: ServiceOffering
domain_of:
- ServiceOffering
range: string
required: false

```
</details>