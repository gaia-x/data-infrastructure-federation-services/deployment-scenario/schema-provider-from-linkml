# Slot: consumedBy


_A resolvable link to the Data Consumer Delaration._



URI: [abc-data:consumedBy](https://$BASE_URL$/abc-data#consumedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProductUsageContract](DataProductUsageContract.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: consumedBy
description: A resolvable link to the Data Consumer Delaration.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:consumedBy
alias: consumedBy
domain_of:
- DataProductUsageContract
range: string

```
</details>