# Slot: providedBy


_A resolvable link to the participant self-description providing the service._



URI: [abc-data:providedBy](https://$BASE_URL$/abc-data#providedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  yes  |
[DataProductUsageContract](DataProductUsageContract.md) |  |  yes  |
[DataUsageAgreement](DataUsageAgreement.md) |  |  yes  |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: providedBy
description: A resolvable link to the participant self-description providing the service.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:providedBy
alias: providedBy
domain_of:
- DataProduct
- DataProductUsageContract
- DataUsageAgreement
- ServiceOffering
range: string

```
</details>