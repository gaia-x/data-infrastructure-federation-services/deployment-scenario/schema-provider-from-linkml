# Slot: getVerifiableCredentialsIDs


_a route used to synchronize catalogues and retrieve the list of Verifiable Credentials (issuer, id)._



URI: [https://$BASE_URL$/abc-data/:getVerifiableCredentialsIDs](https://$BASE_URL$/abc-data/:getVerifiableCredentialsIDs)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: getVerifiableCredentialsIDs
description: a route used to synchronize catalogues and retrieve the list of Verifiable
  Credentials (issuer, id).
from_schema: https://$BASE_URL$/abc-data
rank: 1000
multivalued: true
alias: getVerifiableCredentialsIDs
owner: Catalogue
domain_of:
- Catalogue
range: string
required: true
inlined: true
inlined_as_list: true

```
</details>