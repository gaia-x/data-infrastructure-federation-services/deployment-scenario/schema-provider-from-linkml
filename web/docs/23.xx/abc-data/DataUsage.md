# Slot: dataUsage


_A resolvable link to Data Usage._



URI: [abc-data:dataUsage](https://$BASE_URL$/abc-data#dataUsage)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProductUsageContract](DataProductUsageContract.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: dataUsage
description: A resolvable link to Data Usage.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:dataUsage
alias: dataUsage
domain_of:
- DataProductUsageContract
range: string

```
</details>