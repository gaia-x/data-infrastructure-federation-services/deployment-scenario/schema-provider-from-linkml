# Slot: producedBy


_A resolvable link to the Data Producer._



URI: [abc-data:producedBy](https://$BASE_URL$/abc-data#producedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataUsageAgreement](DataUsageAgreement.md) |  |  yes  |







## Properties

* Range: [DataProducer](DataProducer.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: producedBy
description: A resolvable link to the Data Producer.
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: abc-data:producedBy
alias: producedBy
domain_of:
- DataUsageAgreement
range: DataProducer

```
</details>