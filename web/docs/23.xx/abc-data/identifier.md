# Slot: identifier


_Unique uuid4_



URI: [dct:identifier](http://purl.org/dc/terms/identifier)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  yes  |
[DataSet](DataSet.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data




## LinkML Source

<details>
```yaml
name: identifier
description: Unique uuid4
from_schema: https://$BASE_URL$/abc-data
rank: 1000
slot_uri: dct:identifier
alias: identifier
domain_of:
- DataProduct
- DataSet
range: string

```
</details>