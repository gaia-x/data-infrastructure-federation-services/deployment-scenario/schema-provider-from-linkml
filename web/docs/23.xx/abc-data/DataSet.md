# Class: DataSet



URI: [abc-data:DataSet](https://$BASE_URL$/abc-data#DataSet)




```mermaid
 classDiagram
    class DataSet
      GaiaXEntity <|-- DataSet
      
      DataSet : dataLicensors
        
          DataSet --|> DataLicensor : dataLicensors
        
      DataSet : dataUsageAgreement
        
          DataSet --|> DataUsageAgreement : dataUsageAgreement
        
      DataSet : distributions
        
          DataSet --|> Distribution : distributions
        
      DataSet : expirationDateTime
        
      DataSet : exposedThrough
        
      DataSet : identifier
        
      DataSet : issued
        
      DataSet : license
        
      DataSet : title
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **DataSet**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [title](title.md) | 1..1 <br/> [String](String.md) | Title of the Dataset | direct |
| [distributions](distributions.md) | 1..* <br/> [Distribution](Distribution.md) | List of distributions format of the dataset | direct |
| [identifier](identifier.md) | 1..1 <br/> [String](String.md) | Unique uuid4 | direct |
| [issued](issued.md) | 0..1 <br/> [Date](Date.md) | Publication date in ISO 8601 format | direct |
| [expirationDateTime](expirationDateTime.md) | 0..1 <br/> [Datetime](Datetime.md) | Date time in ISO 8601 format after which data is expired and shall be deleted | direct |
| [license](license.md) | 0..* <br/> [String](String.md) | A list of URIs to license document | direct |
| [dataLicensors](dataLicensors.md) | 0..1 <br/> [DataLicensor](DataLicensor.md) | A list of Licensors either as a free form string or participant self-descript... | direct |
| [dataUsageAgreement](dataUsageAgreement.md) | 0..* <br/> [DataUsageAgreement](DataUsageAgreement.md) | List of authorizations from the data subjects as Natural Person when the data... | direct |
| [exposedThrough](exposedThrough.md) | 1..1 <br/> [String](String.md) | A resolvable link to the data exchange component that exposes the Data Produc... | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/abc-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | abc-data:DataSet |
| native | https://$BASE_URL$/abc-data/:DataSet |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: DataSet
from_schema: https://$BASE_URL$/abc-data
is_a: GaiaXEntity
slots:
- title
- distributions
- identifier
- issued
- expirationDateTime
- license
- dataLicensors
- dataUsageAgreement
- exposedThrough
slot_usage:
  title:
    name: title
    description: Title of the Dataset
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    - StandardConformity
    required: true
  distributions:
    name: distributions
    domain_of:
    - DataSet
    required: true
  identifier:
    name: identifier
    identifier: true
    domain_of:
    - DataProduct
    - DataSet
    required: true
  exposedThrough:
    name: exposedThrough
    domain_of:
    - DataSet
    required: true
class_uri: abc-data:DataSet

```
</details>

### Induced

<details>
```yaml
name: DataSet
from_schema: https://$BASE_URL$/abc-data
is_a: GaiaXEntity
slot_usage:
  title:
    name: title
    description: Title of the Dataset
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    - StandardConformity
    required: true
  distributions:
    name: distributions
    domain_of:
    - DataSet
    required: true
  identifier:
    name: identifier
    identifier: true
    domain_of:
    - DataProduct
    - DataSet
    required: true
  exposedThrough:
    name: exposedThrough
    domain_of:
    - DataSet
    required: true
attributes:
  title:
    name: title
    description: Title of the Dataset
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:title
    alias: title
    owner: DataSet
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    - StandardConformity
    range: string
    required: true
  distributions:
    name: distributions
    description: List of distributions format of the dataset
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:distributions
    multivalued: true
    alias: distributions
    owner: DataSet
    domain_of:
    - DataSet
    range: Distribution
    required: true
  identifier:
    name: identifier
    description: Unique uuid4
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:identifier
    identifier: true
    alias: identifier
    owner: DataSet
    domain_of:
    - DataProduct
    - DataSet
    range: string
    required: true
  issued:
    name: issued
    description: Publication date in ISO 8601 format
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:issued
    alias: issued
    owner: DataSet
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    range: date
    pattern: ^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])$
  expirationDateTime:
    name: expirationDateTime
    description: Date time in ISO 8601 format after which data is expired and shall
      be deleted.
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:expirationDateTime
    alias: expirationDateTime
    owner: DataSet
    domain_of:
    - DataSet
    - Distribution
    range: datetime
    pattern: ^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|-0[1-9]|-1\d|-2[0-3]|-00:?(?:0[1-9]|[1-5]\d)|\+[01]\d|\+2[0-3])(?:|:?[0-5]\d)$
  license:
    name: license
    description: A list of URIs to license document.
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: dct:license
    multivalued: true
    alias: license
    owner: DataSet
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    range: string
  dataLicensors:
    name: dataLicensors
    description: A list of Licensors either as a free form string or participant self-description.
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:dataLicensors
    alias: dataLicensors
    owner: DataSet
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    range: DataLicensor
  dataUsageAgreement:
    name: dataUsageAgreement
    description: List of authorizations from the data subjects as Natural Person when
      the dataset contains PII, as defined by the Trust Framework
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:dataUsageAgreement
    multivalued: true
    alias: dataUsageAgreement
    owner: DataSet
    domain_of:
    - DataProduct
    - DataSet
    - Distribution
    range: DataUsageAgreement
  exposedThrough:
    name: exposedThrough
    description: A resolvable link to the data exchange component that exposes the
      Data Product.
    from_schema: https://$BASE_URL$/abc-data
    rank: 1000
    slot_uri: abc-data:exposedThrough
    alias: exposedThrough
    owner: DataSet
    domain_of:
    - DataSet
    range: string
    required: true
class_uri: abc-data:DataSet

```
</details>