# Slot: relatedOrganizations


_A list of related organization, either as sub or parent organization, if any._



URI: [https://$BASE_URL$/aster-data/:relatedOrganizations](https://$BASE_URL$/aster-data/:relatedOrganizations)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProvider](DataProvider.md) | is equivalent to Gaia-X Provider |  no  |
[DataLicensor](DataLicensor.md) | is equivalent to Gaia-X Licensor |  no  |
[DataProducer](DataProducer.md) | is equivalent to Gaia-X RessourceOwner |  no  |
[DataConsumer](DataConsumer.md) | is equivalent to Gaia-X Consumer |  no  |
[Federator](Federator.md) | is equivalent to Gaia-X Federator |  no  |
[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number |  no  |







## Properties

* Range: [Relationship](Relationship.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: relatedOrganizations
description: A list of related organization, either as sub or parent organization,
  if any.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
multivalued: true
alias: relatedOrganizations
owner: LegalPerson
domain_of:
- LegalPerson
range: Relationship

```
</details>