# Slot: dataUsage


_A resolvable link to Data Usage._



URI: [aster-data:dataUsage](https://$BASE_URL$/aster-data#dataUsage)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProductUsageContract](DataProductUsageContract.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: dataUsage
description: A resolvable link to Data Usage.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:dataUsage
alias: dataUsage
domain_of:
- DataProductUsageContract
range: string

```
</details>