# Class: GaiaXEntity


_Root class for Gaia-X entities._




* __NOTE__: this is an abstract class and should not be instantiated directly


URI: [https://$BASE_URL$/aster-data/:GaiaXEntity](https://$BASE_URL$/aster-data/:GaiaXEntity)




```mermaid
 classDiagram
    class GaiaXEntity
      GaiaXEntity <|-- SignatureCheckType
      GaiaXEntity <|-- DataUsage
      GaiaXEntity <|-- DataProductUsageContract
      GaiaXEntity <|-- DataSet
      GaiaXEntity <|-- Distribution
      GaiaXEntity <|-- Issuer
      GaiaXEntity <|-- Participant
      GaiaXEntity <|-- Resource
      GaiaXEntity <|-- ServiceOffering
      
      
```





## Inheritance
* **GaiaXEntity**
    * [SignatureCheckType](SignatureCheckType.md)
    * [DataUsage](DataUsage.md)
    * [DataProductUsageContract](DataProductUsageContract.md)
    * [DataSet](DataSet.md)
    * [Distribution](Distribution.md)
    * [Issuer](Issuer.md)
    * [Participant](Participant.md)
    * [Resource](Resource.md)
    * [ServiceOffering](ServiceOffering.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/aster-data/:GaiaXEntity |
| native | https://$BASE_URL$/aster-data/:GaiaXEntity |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: GaiaXEntity
description: Root class for Gaia-X entities.
from_schema: https://$BASE_URL$/aster-data
abstract: true

```
</details>

### Induced

<details>
```yaml
name: GaiaXEntity
description: Root class for Gaia-X entities.
from_schema: https://$BASE_URL$/aster-data
abstract: true

```
</details>