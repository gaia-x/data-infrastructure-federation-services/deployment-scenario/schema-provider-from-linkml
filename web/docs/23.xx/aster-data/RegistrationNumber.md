# Slot: registrationNumber


_Country's registration number, which identifies one specific entity. Valid formats are local, EUID, EORI, vatID, leiCode._



URI: [https://$BASE_URL$/aster-data/:registrationNumber](https://$BASE_URL$/aster-data/:registrationNumber)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProvider](DataProvider.md) | is equivalent to Gaia-X Provider |  no  |
[DataLicensor](DataLicensor.md) | is equivalent to Gaia-X Licensor |  no  |
[DataProducer](DataProducer.md) | is equivalent to Gaia-X RessourceOwner |  no  |
[DataConsumer](DataConsumer.md) | is equivalent to Gaia-X Consumer |  no  |
[Federator](Federator.md) | is equivalent to Gaia-X Federator |  no  |
[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: registrationNumber
description: Country's registration number, which identifies one specific entity.
  Valid formats are local, EUID, EORI, vatID, leiCode.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
multivalued: true
alias: registrationNumber
owner: LegalPerson
domain_of:
- LegalPerson
range: string
required: true
inlined: true
inlined_as_list: true
any_of:
- range: RegistrationNumber
- range: LocalRegistrationNumber
- range: VatID
- range: LeiCode
- range: EORI
- range: EUID

```
</details>