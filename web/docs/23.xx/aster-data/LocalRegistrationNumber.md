# Class: LocalRegistrationNumber



URI: [https://$BASE_URL$/aster-data/:LocalRegistrationNumber](https://$BASE_URL$/aster-data/:LocalRegistrationNumber)




```mermaid
 classDiagram
    class LocalRegistrationNumber
      RegistrationNumber <|-- LocalRegistrationNumber
      
      LocalRegistrationNumber : value
        
      
```





## Inheritance
* [RegistrationNumber](RegistrationNumber.md)
    * **LocalRegistrationNumber**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [value](value.md) | 1..1 <br/> [String](String.md) | The state issued company number | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/aster-data/:LocalRegistrationNumber |
| native | https://$BASE_URL$/aster-data/:LocalRegistrationNumber |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: LocalRegistrationNumber
from_schema: https://$BASE_URL$/aster-data
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: The state issued company number.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    identifier: true
    range: string
    required: true

```
</details>

### Induced

<details>
```yaml
name: LocalRegistrationNumber
from_schema: https://$BASE_URL$/aster-data
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: The state issued company number.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    identifier: true
    alias: value
    owner: LocalRegistrationNumber
    domain_of:
    - LocalRegistrationNumber
    - VatID
    - LeiCode
    - EORI
    - EUID
    range: string
    required: true

```
</details>