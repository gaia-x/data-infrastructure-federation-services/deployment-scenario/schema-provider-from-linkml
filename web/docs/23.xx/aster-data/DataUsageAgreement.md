# Slot: dataUsageAgreement


_List of authorizations from the data subjects as Natural Person when the dataset contains PII, as defined by the Trust Framework_



URI: [aster-data:dataUsageAgreement](https://$BASE_URL$/aster-data#dataUsageAgreement)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |
[DataSet](DataSet.md) |  |  no  |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [DataUsageAgreement](DataUsageAgreement.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: dataUsageAgreement
description: List of authorizations from the data subjects as Natural Person when
  the dataset contains PII, as defined by the Trust Framework
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:dataUsageAgreement
multivalued: true
alias: dataUsageAgreement
domain_of:
- DataProduct
- DataSet
- Distribution
range: DataUsageAgreement

```
</details>