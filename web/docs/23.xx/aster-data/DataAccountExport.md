# Slot: dataAccountExport


_One or more  methods to export data out of the service._



URI: [https://$BASE_URL$/aster-data/:dataAccountExport](https://$BASE_URL$/aster-data/:dataAccountExport)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [DataAccountExport](DataAccountExport.md)

* Multivalued: True

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: dataAccountExport
description: One or more  methods to export data out of the service.
title: data account export
from_schema: https://$BASE_URL$/aster-data
rank: 1000
multivalued: true
alias: dataAccountExport
owner: ServiceOffering
domain_of:
- ServiceOffering
range: DataAccountExport
required: true

```
</details>