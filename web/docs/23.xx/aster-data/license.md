# Slot: license


_A list of URIs to license document._



URI: [dct:license](http://purl.org/dc/terms/license)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  yes  |
[DataSet](DataSet.md) |  |  no  |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: license
description: A list of URIs to license document.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: dct:license
multivalued: true
alias: license
domain_of:
- DataProduct
- DataSet
- Distribution
range: string

```
</details>