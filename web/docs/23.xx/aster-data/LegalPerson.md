# Class: LegalPerson


_An legal person, who is uniquely identifier by it's registration number._





URI: [https://$BASE_URL$/aster-data/:LegalPerson](https://$BASE_URL$/aster-data/:LegalPerson)




```mermaid
 classDiagram
    class LegalPerson
      Participant <|-- LegalPerson
      

      LegalPerson <|-- DataProvider
      LegalPerson <|-- DataLicensor
      LegalPerson <|-- DataProducer
      LegalPerson <|-- DataConsumer
      LegalPerson <|-- Federator
      
      
      LegalPerson : headquarterAddress
        
          LegalPerson --|> HeadquarterAddress : headquarterAddress
        
      LegalPerson : legalAddress
        
          LegalPerson --|> LegalAddress : legalAddress
        
      LegalPerson : registrationNumber
        
      LegalPerson : relatedOrganizations
        
          LegalPerson --|> Relationship : relatedOrganizations
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * [Participant](Participant.md)
        * **LegalPerson**
            * [DataProvider](DataProvider.md)
            * [DataLicensor](DataLicensor.md)
            * [DataProducer](DataProducer.md)
            * [DataConsumer](DataConsumer.md)
            * [Federator](Federator.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [registrationNumber](registrationNumber.md) | 1..* <br/> [String](String.md) | Country's registration number, which identifies one specific entity | direct |
| [legalAddress](legalAddress.md) | 1..1 <br/> [LegalAddress](LegalAddress.md) | The full legal address of the organization | direct |
| [headquarterAddress](headquarterAddress.md) | 1..1 <br/> [HeadquarterAddress](HeadquarterAddress.md) | Full physical location of the headquarter of the organization | direct |
| [relatedOrganizations](relatedOrganizations.md) | 0..* <br/> [Relationship](Relationship.md) | A list of related organization, either as sub or parent organization, if any | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [Catalogue](Catalogue.md) | [providedBy](providedBy.md) | range | [LegalPerson](LegalPerson.md) |
| [Relationship](Relationship.md) | [parentOrganizationOf](parentOrganizationOf.md) | range | [LegalPerson](LegalPerson.md) |
| [Relationship](Relationship.md) | [subOrganisationOf](subOrganisationOf.md) | range | [LegalPerson](LegalPerson.md) |
| [PhysicalResource](PhysicalResource.md) | [maintainedBy](maintainedBy.md) | range | [LegalPerson](LegalPerson.md) |
| [PhysicalResource](PhysicalResource.md) | [ownedBy](ownedBy.md) | range | [LegalPerson](LegalPerson.md) |
| [PhysicalResource](PhysicalResource.md) | [manufacturedBy](manufacturedBy.md) | range | [LegalPerson](LegalPerson.md) |
| [ServiceOffering](ServiceOffering.md) | [providedBy](providedBy.md) | range | [LegalPerson](LegalPerson.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/aster-data/:LegalPerson |
| native | https://$BASE_URL$/aster-data/:LegalPerson |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: LegalPerson
description: An legal person, who is uniquely identifier by it's registration number.
from_schema: https://$BASE_URL$/aster-data
is_a: Participant
attributes:
  registrationNumber:
    name: registrationNumber
    description: Country's registration number, which identifies one specific entity.
      Valid formats are local, EUID, EORI, vatID, leiCode.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    required: true
    inlined: true
    inlined_as_list: true
    any_of:
    - range: RegistrationNumber
    - range: LocalRegistrationNumber
    - range: VatID
    - range: LeiCode
    - range: EORI
    - range: EUID
  legalAddress:
    name: legalAddress
    description: The full legal address of the organization.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: LegalAddress
    required: true
  headquarterAddress:
    name: headquarterAddress
    description: Full physical location of the headquarter of the organization.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: HeadquarterAddress
    required: true
  relatedOrganizations:
    name: relatedOrganizations
    description: A list of related organization, either as sub or parent organization,
      if any.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    range: Relationship
tree_root: true

```
</details>

### Induced

<details>
```yaml
name: LegalPerson
description: An legal person, who is uniquely identifier by it's registration number.
from_schema: https://$BASE_URL$/aster-data
is_a: Participant
attributes:
  registrationNumber:
    name: registrationNumber
    description: Country's registration number, which identifies one specific entity.
      Valid formats are local, EUID, EORI, vatID, leiCode.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: registrationNumber
    owner: LegalPerson
    domain_of:
    - LegalPerson
    range: string
    required: true
    inlined: true
    inlined_as_list: true
    any_of:
    - range: RegistrationNumber
    - range: LocalRegistrationNumber
    - range: VatID
    - range: LeiCode
    - range: EORI
    - range: EUID
  legalAddress:
    name: legalAddress
    description: The full legal address of the organization.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: legalAddress
    owner: LegalPerson
    domain_of:
    - LegalPerson
    range: LegalAddress
    required: true
  headquarterAddress:
    name: headquarterAddress
    description: Full physical location of the headquarter of the organization.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: headquarterAddress
    owner: LegalPerson
    domain_of:
    - LegalPerson
    range: HeadquarterAddress
    required: true
  relatedOrganizations:
    name: relatedOrganizations
    description: A list of related organization, either as sub or parent organization,
      if any.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: relatedOrganizations
    owner: LegalPerson
    domain_of:
    - LegalPerson
    range: Relationship
tree_root: true

```
</details>