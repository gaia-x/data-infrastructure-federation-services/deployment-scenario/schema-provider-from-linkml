# Slot: producedBy


_A resolvable link to the Data Producer._



URI: [aster-data:producedBy](https://$BASE_URL$/aster-data#producedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataUsageAgreement](DataUsageAgreement.md) |  |  yes  |







## Properties

* Range: [DataProducer](DataProducer.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: producedBy
description: A resolvable link to the Data Producer.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:producedBy
alias: producedBy
domain_of:
- DataUsageAgreement
range: DataProducer

```
</details>