# Slot: publisher


_Publisher of the standard._



URI: [https://$BASE_URL$/aster-data/:publisher](https://$BASE_URL$/aster-data/:publisher)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[StandardConformity](StandardConformity.md) | Details about standard applied to entities |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: publisher
description: Publisher of the standard.
title: publisher
from_schema: https://$BASE_URL$/aster-data
rank: 1000
alias: publisher
owner: StandardConformity
domain_of:
- StandardConformity
range: string
required: false

```
</details>