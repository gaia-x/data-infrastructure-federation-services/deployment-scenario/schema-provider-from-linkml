# Slot: location


_List of dataset storage location_



URI: [aster-data:location](https://$BASE_URL$/aster-data#location)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: location
description: List of dataset storage location
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:location
alias: location
domain_of:
- Distribution
range: string

```
</details>