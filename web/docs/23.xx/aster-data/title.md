# Slot: title


_Title of the Data Product_



URI: [dct:title](http://purl.org/dc/terms/title)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  yes  |
[DataSet](DataSet.md) |  |  yes  |
[Distribution](Distribution.md) |  |  yes  |
[StandardConformity](StandardConformity.md) | Details about standard applied to entities |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: title
description: Title of the Data Product
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: dct:title
alias: title
domain_of:
- DataProduct
- DataSet
- Distribution
- StandardConformity
range: string

```
</details>