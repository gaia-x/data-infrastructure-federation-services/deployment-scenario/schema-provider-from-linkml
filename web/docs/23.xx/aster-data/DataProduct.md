# Slot: dataProduct


_A resolvable link to the Data Product Description Declaration (after negotiation)._



URI: [aster-data:dataProduct](https://$BASE_URL$/aster-data#dataProduct)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProductUsageContract](DataProductUsageContract.md) |  |  yes  |
[DataUsageAgreement](DataUsageAgreement.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: dataProduct
description: A resolvable link to the Data Product Description Declaration (after
  negotiation).
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:dataProduct
alias: dataProduct
domain_of:
- DataProductUsageContract
- DataUsageAgreement
range: string

```
</details>