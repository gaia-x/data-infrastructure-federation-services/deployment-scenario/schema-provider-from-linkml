# Slot: dataLicensors


_A list of Licensors either as a free form string or participant self-description._



URI: [aster-data:dataLicensors](https://$BASE_URL$/aster-data#dataLicensors)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |
[DataSet](DataSet.md) |  |  no  |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [DataLicensor](DataLicensor.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: dataLicensors
description: A list of Licensors either as a free form string or participant self-description.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:dataLicensors
alias: dataLicensors
domain_of:
- DataProduct
- DataSet
- Distribution
range: DataLicensor

```
</details>