# Slot: postal_code


_String of a street-address._



URI: [https://$BASE_URL$/aster-data/:postal_code](https://$BASE_URL$/aster-data/:postal_code)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Address](Address.md) |  |  no  |
[LegalAddress](LegalAddress.md) |  |  no  |
[HeadquarterAddress](HeadquarterAddress.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: postal-code
description: String of a street-address.
title: postal code
from_schema: https://$BASE_URL$/aster-data
rank: 1000
alias: postal_code
owner: Address
domain_of:
- Address
range: string
required: false

```
</details>