# Slot: format


_Format of the dataset distribution (pdf, csv, …)_



URI: [dct:format](http://purl.org/dc/terms/format)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Distribution](Distribution.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: format
description: Format of the dataset distribution (pdf, csv, …)
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: dct:format
alias: format
domain_of:
- Distribution
range: string

```
</details>