# Slot: accessType


_Type of data support: digital, physical._



URI: [https://$BASE_URL$/aster-data/:accessType](https://$BASE_URL$/aster-data/:accessType)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataAccountExport](DataAccountExport.md) | List of methods to export data from your account out of the service |  no  |







## Properties

* Range: [AccessTypeMeans](AccessTypeMeans.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: accessType
description: 'Type of data support: digital, physical.'
title: access type
from_schema: https://$BASE_URL$/aster-data
rank: 1000
alias: accessType
owner: DataAccountExport
domain_of:
- DataAccountExport
range: AccessTypeMeans
required: true

```
</details>