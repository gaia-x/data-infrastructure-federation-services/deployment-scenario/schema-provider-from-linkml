# Slot: hasPolicy


_a list of policy expressed using ODRL_



URI: [odrl:hasPolicy](http://www.w3.org/ns/odrl/2/hasPolicy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: hasPolicy
description: a list of policy expressed using ODRL
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: odrl:hasPolicy
alias: hasPolicy
domain_of:
- DataProduct
range: string

```
</details>