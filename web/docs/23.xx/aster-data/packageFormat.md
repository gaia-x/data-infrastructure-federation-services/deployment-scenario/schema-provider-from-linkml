# Slot: packageFormat


_The package format of the distribution in which one or more data files are grouped together_



URI: [dcat:packageFormat](http://www.w3.org/ns/dcat#packageFormat)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: packageFormat
description: The package format of the distribution in which one or more data files
  are grouped together
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: dcat:packageFormat
alias: packageFormat
domain_of:
- Distribution
range: string

```
</details>