# Slot: licensedBy


_A resolvable links to Data Licensors._



URI: [aster-data:licensedBy](https://$BASE_URL$/aster-data#licensedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataUsageAgreement](DataUsageAgreement.md) |  |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: licensedBy
description: A resolvable links to Data Licensors.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:licensedBy
multivalued: true
alias: licensedBy
domain_of:
- DataUsageAgreement
range: string

```
</details>