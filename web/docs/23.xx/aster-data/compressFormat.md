# Slot: compressFormat


_The compression format of the distribution in which the data is contained in a compressed form_



URI: [dcat:compressFormat](http://www.w3.org/ns/dcat#compressFormat)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: compressFormat
description: The compression format of the distribution in which the data is contained
  in a compressed form
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: dcat:compressFormat
alias: compressFormat
domain_of:
- Distribution
range: string

```
</details>