# Slot: consumedBy


_A resolvable link to the Data Consumer Delaration._



URI: [aster-data:consumedBy](https://$BASE_URL$/aster-data#consumedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProductUsageContract](DataProductUsageContract.md) |  |  yes  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: consumedBy
description: A resolvable link to the Data Consumer Delaration.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:consumedBy
alias: consumedBy
domain_of:
- DataProductUsageContract
range: string

```
</details>