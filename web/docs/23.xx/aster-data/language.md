# Slot: language


_Language_



URI: [dcat:language](http://www.w3.org/ns/dcat#language)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Distribution](Distribution.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: language
description: Language
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: dcat:language
alias: language
domain_of:
- Distribution
range: string

```
</details>