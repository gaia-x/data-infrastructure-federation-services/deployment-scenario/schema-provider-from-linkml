# Class: LeiCode



URI: [https://$BASE_URL$/aster-data/:LeiCode](https://$BASE_URL$/aster-data/:LeiCode)




```mermaid
 classDiagram
    class LeiCode
      RegistrationNumber <|-- LeiCode
      
      LeiCode : value
        
      
```





## Inheritance
* [RegistrationNumber](RegistrationNumber.md)
    * **LeiCode**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [value](value.md) | 1..1 <br/> [String](String.md) | Unique LEI number as defined by https://www | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://$BASE_URL$/aster-data/:LeiCode |
| native | https://$BASE_URL$/aster-data/:LeiCode |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: LeiCode
from_schema: https://$BASE_URL$/aster-data
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: Unique LEI number as defined by https://www.gleif.org.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    slot_uri: schema:leiCode
    identifier: true
    required: true

```
</details>

### Induced

<details>
```yaml
name: LeiCode
from_schema: https://$BASE_URL$/aster-data
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: Unique LEI number as defined by https://www.gleif.org.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    slot_uri: schema:leiCode
    identifier: true
    alias: value
    owner: LeiCode
    domain_of:
    - LocalRegistrationNumber
    - VatID
    - LeiCode
    - EORI
    - EUID
    range: string
    required: true

```
</details>