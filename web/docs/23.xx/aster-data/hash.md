# Slot: hash


_To uniquely identify the data contained in the dataset distribution_



URI: [aster-data:hash](https://$BASE_URL$/aster-data#hash)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Distribution](Distribution.md) |  |  no  |
[TermsAndConditions](TermsAndConditions.md) | Terms and Conditions applying to a service offering |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: hash
description: To uniquely identify the data contained in the dataset distribution
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:hash
alias: hash
domain_of:
- Distribution
- TermsAndConditions
range: string

```
</details>