# Slot: distributions


_List of distributions format of the dataset_



URI: [dct:distributions](http://purl.org/dc/terms/distributions)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataSet](DataSet.md) |  |  yes  |







## Properties

* Range: [Distribution](Distribution.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: distributions
description: List of distributions format of the dataset
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: dct:distributions
multivalued: true
alias: distributions
domain_of:
- DataSet
range: Distribution

```
</details>