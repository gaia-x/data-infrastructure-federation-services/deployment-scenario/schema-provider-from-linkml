# Class: ServiceOffering


_A description of a digital service available for order._





URI: [gx:ServiceOffering](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#ServiceOffering)




```mermaid
 classDiagram
    class ServiceOffering
      GaiaXEntity <|-- ServiceOffering
      

      ServiceOffering <|-- DataProduct
      ServiceOffering <|-- Catalogue
      
      
      ServiceOffering : aggregationOf
        
      ServiceOffering : dataAccountExport
        
          ServiceOffering --|> DataAccountExport : dataAccountExport
        
      ServiceOffering : dataProtectionRegime
        
          ServiceOffering --|> PersonalDataProtectionRegime : dataProtectionRegime
        
      ServiceOffering : dependsOn
        
          ServiceOffering --|> ServiceOffering : dependsOn
        
      ServiceOffering : description
        
      ServiceOffering : endpoint
        
          ServiceOffering --|> Endpoint : endpoint
        
      ServiceOffering : hostedOn
        
      ServiceOffering : keyword
        
      ServiceOffering : name
        
      ServiceOffering : policy
        
      ServiceOffering : providedBy
        
          ServiceOffering --|> LegalPerson : providedBy
        
      ServiceOffering : provisionType
        
      ServiceOffering : termsAndConditions
        
          ServiceOffering --|> TermsAndConditions : termsAndConditions
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **ServiceOffering**
        * [DataProduct](DataProduct.md)
        * [Catalogue](Catalogue.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [providedBy](providedBy.md) | 1..1 <br/> [LegalPerson](LegalPerson.md) | A resolvable link to the participant self-description providing the service | direct |
| [name](name.md) | 1..1 <br/> [String](String.md) | Human readable name of the service offering | direct |
| [termsAndConditions](termsAndConditions.md) | 1..* <br/> [TermsAndConditions](TermsAndConditions.md) | A resolvable link to the Terms and Conditions applying to that service | direct |
| [policy](policy.md) | 1..* <br/> [String](String.md) | One or more policies expressed using a DSL (e | direct |
| [dataProtectionRegime](dataProtectionRegime.md) | 0..* <br/> [PersonalDataProtectionRegime](PersonalDataProtectionRegime.md) | One or more data protection regimes | direct |
| [dataAccountExport](dataAccountExport.md) | 1..* <br/> [DataAccountExport](DataAccountExport.md) | One or more  methods to export data out of the service | direct |
| [description](description.md) | 0..1 <br/> [String](String.md) | A description in natural language | direct |
| [keyword](keyword.md) | 0..* <br/> [String](String.md) | Keywords that describe / tag the service | direct |
| [provisionType](provisionType.md) | 0..1 <br/> [String](String.md) | Provision type of the service | direct |
| [endpoint](endpoint.md) | 0..1 <br/> [Endpoint](Endpoint.md) | Endpoint through which the Service Offering can be accessed | direct |
| [hostedOn](hostedOn.md) | 0..* <br/> [String](String.md) | List of Resource references where service is hosted and can be instantiated | direct |
| [dependsOn](dependsOn.md) | 0..* <br/> [ServiceOffering](ServiceOffering.md) | A resolvable link to the service offering self-description related to the ser... | direct |
| [aggregationOf](aggregationOf.md) | 0..* <br/> [String](String.md) | A resolvable link to the resources self-description related to the service an... | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [DataProduct](DataProduct.md) | [dependsOn](dependsOn.md) | range | [ServiceOffering](ServiceOffering.md) |
| [Catalogue](Catalogue.md) | [dependsOn](dependsOn.md) | range | [ServiceOffering](ServiceOffering.md) |
| [ServiceOffering](ServiceOffering.md) | [dependsOn](dependsOn.md) | range | [ServiceOffering](ServiceOffering.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:ServiceOffering |
| native | https://$BASE_URL$/aster-data/:ServiceOffering |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: ServiceOffering
description: A description of a digital service available for order.
from_schema: https://$BASE_URL$/aster-data
is_a: GaiaXEntity
attributes:
  providedBy:
    name: providedBy
    description: A resolvable link to the participant self-description providing the
      service.
    title: provided by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    range: LegalPerson
    required: true
  name:
    name: name
    description: Human readable name of the service offering.
    title: name
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    range: string
    required: true
  termsAndConditions:
    name: termsAndConditions
    description: A resolvable link to the Terms and Conditions applying to that service.
    title: terms and conditions
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    multivalued: true
    range: TermsAndConditions
    required: true
  policy:
    name: policy
    description: One or more policies expressed using a DSL (e.g., Rego or ODRL).
    title: policy
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    range: string
    required: true
  dataProtectionRegime:
    name: dataProtectionRegime
    description: One or more data protection regimes.
    title: data protection regime
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    range: PersonalDataProtectionRegime
    required: false
  dataAccountExport:
    name: dataAccountExport
    description: One or more  methods to export data out of the service.
    title: data account export
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    range: DataAccountExport
    required: true
  description:
    name: description
    description: A description in natural language.
    title: description
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    range: string
    required: false
  keyword:
    name: keyword
    description: Keywords that describe / tag the service.
    title: keyword
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    range: string
    required: false
  provisionType:
    name: provisionType
    description: Provision type of the service
    title: provision type
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: false
  endpoint:
    name: endpoint
    description: Endpoint through which the Service Offering can be accessed.
    title: endpoint
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: Endpoint
    required: false
  hostedOn:
    name: hostedOn
    description: List of Resource references where service is hosted and can be instantiated.
      Can refer to availabilty zones, data centers, regions, etc.
    title: hosted on
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    required: false
    any_of:
    - range: Resource
    - range: ServiceOffering
  dependsOn:
    name: dependsOn
    description: A resolvable link to the service offering self-description related
      to the service and that can exist independently of it.
    title: depends on
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    range: ServiceOffering
    required: false
  aggregationOf:
    name: aggregationOf
    description: A resolvable link to the resources self-description related to the
      service and that can exist independently of it.
    title: aggregation of
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    multivalued: true
    required: false
    any_of:
    - range: Resource
    - range: ServiceOffering
class_uri: gx:ServiceOffering

```
</details>

### Induced

<details>
```yaml
name: ServiceOffering
description: A description of a digital service available for order.
from_schema: https://$BASE_URL$/aster-data
is_a: GaiaXEntity
attributes:
  providedBy:
    name: providedBy
    description: A resolvable link to the participant self-description providing the
      service.
    title: provided by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    alias: providedBy
    owner: ServiceOffering
    domain_of:
    - DataProduct
    - DataProductUsageContract
    - DataUsageAgreement
    - ServiceOffering
    range: LegalPerson
    required: true
  name:
    name: name
    description: Human readable name of the service offering.
    title: name
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    alias: name
    owner: ServiceOffering
    domain_of:
    - Resource
    - ServiceOffering
    range: string
    required: true
  termsAndConditions:
    name: termsAndConditions
    description: A resolvable link to the Terms and Conditions applying to that service.
    title: terms and conditions
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    multivalued: true
    alias: termsAndConditions
    owner: ServiceOffering
    domain_of:
    - DataProduct
    - GaiaXTermsAndConditions
    - Issuer
    - ServiceOffering
    range: TermsAndConditions
    required: true
  policy:
    name: policy
    description: One or more policies expressed using a DSL (e.g., Rego or ODRL).
    title: policy
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: policy
    owner: ServiceOffering
    domain_of:
    - ServiceOffering
    range: string
    required: true
  dataProtectionRegime:
    name: dataProtectionRegime
    description: One or more data protection regimes.
    title: data protection regime
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: dataProtectionRegime
    owner: ServiceOffering
    domain_of:
    - ServiceOffering
    range: PersonalDataProtectionRegime
    required: false
  dataAccountExport:
    name: dataAccountExport
    description: One or more  methods to export data out of the service.
    title: data account export
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: dataAccountExport
    owner: ServiceOffering
    domain_of:
    - ServiceOffering
    range: DataAccountExport
    required: true
  description:
    name: description
    description: A description in natural language.
    title: description
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    alias: description
    owner: ServiceOffering
    domain_of:
    - DataProduct
    - Resource
    - ServiceOffering
    range: string
    required: false
  keyword:
    name: keyword
    description: Keywords that describe / tag the service.
    title: keyword
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: keyword
    owner: ServiceOffering
    domain_of:
    - ServiceOffering
    range: string
    required: false
  provisionType:
    name: provisionType
    description: Provision type of the service
    title: provision type
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: provisionType
    owner: ServiceOffering
    domain_of:
    - ServiceOffering
    range: string
    required: false
  endpoint:
    name: endpoint
    description: Endpoint through which the Service Offering can be accessed.
    title: endpoint
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: endpoint
    owner: ServiceOffering
    domain_of:
    - ServiceOffering
    range: Endpoint
    required: false
  hostedOn:
    name: hostedOn
    description: List of Resource references where service is hosted and can be instantiated.
      Can refer to availabilty zones, data centers, regions, etc.
    title: hosted on
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: hostedOn
    owner: ServiceOffering
    domain_of:
    - ServiceOffering
    range: string
    required: false
    any_of:
    - range: Resource
    - range: ServiceOffering
  dependsOn:
    name: dependsOn
    description: A resolvable link to the service offering self-description related
      to the service and that can exist independently of it.
    title: depends on
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: dependsOn
    owner: ServiceOffering
    domain_of:
    - ServiceOffering
    range: ServiceOffering
    required: false
  aggregationOf:
    name: aggregationOf
    description: A resolvable link to the resources self-description related to the
      service and that can exist independently of it.
    title: aggregation of
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    multivalued: true
    alias: aggregationOf
    owner: ServiceOffering
    domain_of:
    - DataProduct
    - Resource
    - ServiceOffering
    range: string
    required: false
    any_of:
    - range: Resource
    - range: ServiceOffering
class_uri: gx:ServiceOffering

```
</details>