# Class: DataProductUsageContract



URI: [aster-data:DataProductUsageContract](https://$BASE_URL$/aster-data#DataProductUsageContract)




```mermaid
 classDiagram
    class DataProductUsageContract
      GaiaXEntity <|-- DataProductUsageContract
      
      DataProductUsageContract : consumedBy
        
          DataProductUsageContract --|> DataConsumer : consumedBy
        
      DataProductUsageContract : dataProduct
        
          DataProductUsageContract --|> DataConsumer : dataProduct
        
      DataProductUsageContract : dataUsage
        
          DataProductUsageContract --|> DataUsage : dataUsage
        
      DataProductUsageContract : notarizedIn
        
      DataProductUsageContract : providedBy
        
          DataProductUsageContract --|> DataProvider : providedBy
        
      DataProductUsageContract : signers
        
          DataProductUsageContract --|> SignatureCheckType : signers
        
      DataProductUsageContract : termOfUsage
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **DataProductUsageContract**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [providedBy](providedBy.md) | 1..1 <br/> [DataProvider](DataProvider.md) | A resolvable link to the Data Provider Declaration | direct |
| [consumedBy](consumedBy.md) | 1..1 <br/> [DataConsumer](DataConsumer.md) | A resolvable link to the Data Consumer Delaration | direct |
| [dataProduct](dataProduct.md) | 1..1 <br/> [DataConsumer](DataConsumer.md) | A resolvable link to the Data Product Description Declaration (after negotiat... | direct |
| [signers](signers.md) | 1..* <br/> [SignatureCheckType](SignatureCheckType.md) | The array identifying all required Participant signature | direct |
| [termOfUsage](termOfUsage.md) | 1..1 <br/> [String](String.md) | A resolvable link to the Term of Usage | direct |
| [notarizedIn](notarizedIn.md) | 0..1 <br/> [String](String.md) | A resolvable link to the Notarization service | direct |
| [dataUsage](dataUsage.md) | 1..1 <br/> [DataUsage](DataUsage.md) | A resolvable link to Data Usage | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | aster-data:DataProductUsageContract |
| native | https://$BASE_URL$/aster-data/:DataProductUsageContract |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: DataProductUsageContract
from_schema: https://$BASE_URL$/aster-data
is_a: GaiaXEntity
slots:
- providedBy
- consumedBy
- dataProduct
- signers
- termOfUsage
- notarizedIn
- dataUsage
slot_usage:
  providedBy:
    name: providedBy
    description: A resolvable link to the Data Provider Declaration.
    domain_of:
    - DataProduct
    - DataProductUsageContract
    - DataUsageAgreement
    - ServiceOffering
    range: DataProvider
    required: true
  consumedBy:
    name: consumedBy
    domain_of:
    - DataProductUsageContract
    range: DataConsumer
    required: true
  dataProduct:
    name: dataProduct
    domain_of:
    - DataProductUsageContract
    - DataUsageAgreement
    range: DataConsumer
    required: true
  signers:
    name: signers
    domain_of:
    - DataProductUsageContract
    - DataUsageAgreement
    required: true
  termOfUsage:
    name: termOfUsage
    domain_of:
    - DataProductUsageContract
    required: true
  dataUsage:
    name: dataUsage
    domain_of:
    - DataProductUsageContract
    range: DataUsage
    required: true
class_uri: aster-data:DataProductUsageContract

```
</details>

### Induced

<details>
```yaml
name: DataProductUsageContract
from_schema: https://$BASE_URL$/aster-data
is_a: GaiaXEntity
slot_usage:
  providedBy:
    name: providedBy
    description: A resolvable link to the Data Provider Declaration.
    domain_of:
    - DataProduct
    - DataProductUsageContract
    - DataUsageAgreement
    - ServiceOffering
    range: DataProvider
    required: true
  consumedBy:
    name: consumedBy
    domain_of:
    - DataProductUsageContract
    range: DataConsumer
    required: true
  dataProduct:
    name: dataProduct
    domain_of:
    - DataProductUsageContract
    - DataUsageAgreement
    range: DataConsumer
    required: true
  signers:
    name: signers
    domain_of:
    - DataProductUsageContract
    - DataUsageAgreement
    required: true
  termOfUsage:
    name: termOfUsage
    domain_of:
    - DataProductUsageContract
    required: true
  dataUsage:
    name: dataUsage
    domain_of:
    - DataProductUsageContract
    range: DataUsage
    required: true
attributes:
  providedBy:
    name: providedBy
    description: A resolvable link to the Data Provider Declaration.
    from_schema: https://$BASE_URL$/aster-data
    rank: 1000
    slot_uri: aster-data:providedBy
    alias: providedBy
    owner: DataProductUsageContract
    domain_of:
    - DataProduct
    - DataProductUsageContract
    - DataUsageAgreement
    - ServiceOffering
    range: DataProvider
    required: true
  consumedBy:
    name: consumedBy
    description: A resolvable link to the Data Consumer Delaration.
    from_schema: https://$BASE_URL$/aster-data
    rank: 1000
    slot_uri: aster-data:consumedBy
    alias: consumedBy
    owner: DataProductUsageContract
    domain_of:
    - DataProductUsageContract
    range: DataConsumer
    required: true
  dataProduct:
    name: dataProduct
    description: A resolvable link to the Data Product Description Declaration (after
      negotiation).
    from_schema: https://$BASE_URL$/aster-data
    rank: 1000
    slot_uri: aster-data:dataProduct
    alias: dataProduct
    owner: DataProductUsageContract
    domain_of:
    - DataProductUsageContract
    - DataUsageAgreement
    range: DataConsumer
    required: true
  signers:
    name: signers
    description: The array identifying all required Participant signature
    from_schema: https://$BASE_URL$/aster-data
    rank: 1000
    slot_uri: aster-data:signers
    multivalued: true
    alias: signers
    owner: DataProductUsageContract
    domain_of:
    - DataProductUsageContract
    - DataUsageAgreement
    range: SignatureCheckType
    required: true
  termOfUsage:
    name: termOfUsage
    description: A resolvable link to the Term of Usage.
    from_schema: https://$BASE_URL$/aster-data
    rank: 1000
    slot_uri: aster-data:termOfUsage
    alias: termOfUsage
    owner: DataProductUsageContract
    domain_of:
    - DataProductUsageContract
    range: string
    required: true
  notarizedIn:
    name: notarizedIn
    description: A resolvable link to the Notarization service
    from_schema: https://$BASE_URL$/aster-data
    rank: 1000
    alias: notarizedIn
    owner: DataProductUsageContract
    domain_of:
    - DataProductUsageContract
    range: string
  dataUsage:
    name: dataUsage
    description: A resolvable link to Data Usage.
    from_schema: https://$BASE_URL$/aster-data
    rank: 1000
    slot_uri: aster-data:dataUsage
    alias: dataUsage
    owner: DataProductUsageContract
    domain_of:
    - DataProductUsageContract
    range: DataUsage
    required: true
class_uri: aster-data:DataProductUsageContract

```
</details>