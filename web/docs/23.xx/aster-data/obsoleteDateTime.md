# Slot: obsoleteDateTime


_Date time in ISO 8601 format after which Data Product is obsolete._



URI: [aster-data:obsoleteDateTime](https://$BASE_URL$/aster-data#obsoleteDateTime)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |







## Properties

* Range: [Datetime](Datetime.md)

* Regex pattern: `^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|-0[1-9]|-1\d|-2[0-3]|-00:?(?:0[1-9]|[1-5]\d)|\+[01]\d|\+2[0-3])(?:|:?[0-5]\d)$`





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: obsoleteDateTime
description: Date time in ISO 8601 format after which Data Product is obsolete.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:obsoleteDateTime
alias: obsoleteDateTime
domain_of:
- DataProduct
range: datetime
pattern: ^(?:19|20)\d{2}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|-0[1-9]|-1\d|-2[0-3]|-00:?(?:0[1-9]|[1-5]\d)|\+[01]\d|\+2[0-3])(?:|:?[0-5]\d)$

```
</details>