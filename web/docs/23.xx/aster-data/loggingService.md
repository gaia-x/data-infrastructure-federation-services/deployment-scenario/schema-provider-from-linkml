# Slot: loggingService


_Link to the Logging Service._



URI: [aster-data:loggingService](https://$BASE_URL$/aster-data#loggingService)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataUsage](DataUsage.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: loggingService
description: Link to the Logging Service.
from_schema: https://$BASE_URL$/aster-data
rank: 1000
slot_uri: aster-data:loggingService
alias: loggingService
domain_of:
- DataUsage
range: string

```
</details>