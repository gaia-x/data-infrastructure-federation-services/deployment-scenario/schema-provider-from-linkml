# Slot: endpoint


_Endpoint through which the Service Offering can be accessed._



URI: [https://$BASE_URL$/aster-data/:endpoint](https://$BASE_URL$/aster-data/:endpoint)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataProduct](DataProduct.md) |  |  no  |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [Endpoint](Endpoint.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://$BASE_URL$/aster-data




## LinkML Source

<details>
```yaml
name: endpoint
description: Endpoint through which the Service Offering can be accessed.
title: endpoint
from_schema: https://$BASE_URL$/aster-data
rank: 1000
alias: endpoint
owner: ServiceOffering
domain_of:
- ServiceOffering
range: Endpoint
required: false

```
</details>