> ⚠️ <mark>This documentation is not updated, we keep it to have a better understood of the compliance.</mark> ⚠️
***

# Validating compliance - wip

This page illustrates a proposition of concepts and workflows that need to be reviewed and validated by the Gaia-X technical commitee.

The 'gax-compliance' namespace introduces several types of self-descriptions to manage the compliance of service offerings.

```mermaid
classDiagram


ComplianceAssessmentBody "*" -- "*" ThirdPartyComplianceCertificationScheme
ComplianceAssessmentBody  <|-- Participant: Inheritance
class ComplianceAssessmentBody{
  List~ThirdPartyComplianceCertificationScheme~ canCertifyThirdPartyComplianceCertificationScheme
}


class Participant



ComplianceReferenceManager  <|-- Participant: Inheritance
ComplianceReferenceManager "1" -- "*" ComplianceReference
class ComplianceReferenceManager{
  List~ComplianceReference~ hasComplianceReferences
}

ThirdPartyComplianceCertificationScheme "*" -- "1" ComplianceCriterion
class ThirdPartyComplianceCertificationScheme{
 List~ComplianceAssessmentBody~ hasComplianceAssessmentBodies
 ComplianceReference hasComplianceReference
 List~ComplianceCriterion~ grantsComplianceCriteriaCombination
}

ComplianceReference "1" -- "*" ComplianceCertificationScheme
ComplianceReference "1" -- "*" ThirdPartyComplianceCertificationScheme
class ComplianceReference{
  xsd:anyURI hasReferenceUrl
  xsd:string hasSha256
  xsd:string hasComplianceReferenceTitle
  xsd:string hasDescription
  ComplianceReferenceManager hasComplianceReferenceManager
  xsd:string hasVersion
  xsd:dateTime cRValidFrom
  xsd:dateTime cRValidUntil
}


class ComplianceCriterion{
  xsd:string hasName
  xsd:string hasDescription
  xsd:string hasLevel
  xsd:string hasCategory
  xsd:boolean canBeSelfAssessed
}

ComplianceLabel -- "*" ComplianceCriterion
class ComplianceLabel{
  xsd:string hasName
  xsd:string hasDescription
  xsd:string hasLevel
  List~ComplianceCriterion~ hasRequiredComplianceCriteriaCombination
}


ComplianceCertificationScheme "*" -- "1" ComplianceCriterion
class ComplianceCertificationScheme{
 ComplianceReference hasComplianceReference
 List~ComplianceCriterion~ grantsComplianceCriteriaCombination
}

ComplianceCertificateClaim "*" -- "1" ComplianceCertificationScheme
ComplianceCertificateClaim "*" -- "1" LocatedServiceOffering
class ComplianceCertificateClaim{
 ComplianceCertificationScheme hasComplianceCertificationScheme
 LocatedServiceOffering hasServiceOffering
}

%%ComplianceCertificateCredential <|-- VerifiableCredential: Inheritance
ComplianceCertificateCredential "1" -- "1" ComplianceCertificateClaim
class ComplianceCertificateCredential{
 ComplianceCertificateClaim credentialSubject
 xsd:boolean isValid
}


%%ThirdPartyComplianceCertificateCredential <|-- VerifiableCredential: Inheritance
ThirdPartyComplianceCertificateCredential "1" -- "1" ThirdPartyComplianceCertificateClaim
class ThirdPartyComplianceCertificateCredential{
  ThirdPartyComplianceCertificateClaim credentialSubject
  xsd:boolean isValid
}


ThirdPartyComplianceCertificateClaim "*" -- "1" ThirdPartyComplianceCertificationScheme
ThirdPartyComplianceCertificateClaim "*" -- "1" LocatedServiceOffering
class ThirdPartyComplianceCertificateClaim{
  ThirdPartyComplianceCertificationScheme hasComplianceCertificationScheme
  LocatedServiceOffering hasServiceOffering
  ComplianceAssessmentBody hasComplianceAssessmentBody
}

SelfAssessedComplianceCriteriaClaim "*" -- "1" LocatedServiceOffering
SelfAssessedComplianceCriteriaClaim "*" -- "1" ComplianceCriterion
class SelfAssessedComplianceCriteriaClaim{
  LocatedServiceOffering hasServiceOffering
  List~ComplianceCriterion~ hasDeclaredComplianceCriteriaCombination
}

%% SelfAssessedComplianceCriteriaCredential <|-- VerifiableCredential: Inheritance
SelfAssessedComplianceCriteriaCredential "1" -- "1" SelfAssessedComplianceCriteriaClaim
class SelfAssessedComplianceCriteriaCredential{
  SelfAssessedComplianceCriteriaClaim credentialSubject
  xsd:boolean isValid
}
```



## Self descriptions for compliance

### ComplianceReferenceManager

This actor is a Participant inside a given Gaia-X ecosystem in charge of emitting VCs for ComplianceReference and subclasses of ComplianceCertificationScheme.

Several ComplianceReferenceManager can be involved in the same Gaia-X ecosystem. Yet a ComplianceReferenceManager has authority only on the ComplianceReference instances it has created as well as the ComplianceCertificationScheme referencing the ComplianceReference.

**_RULE_** `issuer` of a `ComplianceCertificationScheme` VerifiableCredential equals `hasComplianceReference`.`hasComplianceReferenceManager`.

Each ComplianceReferenceManager must be validated and approved by the **Gaia-X Trust Framework Service** (ComplianceReferenceManager VerifiableCredential is signed by https://compliance.gaia-x.eu/).

To facilitate the bootstrap of compliance features, the default ComplianceReferenceManager is the **Gaia-X Trust Framework Service** itself.

To facilitate the bootstrap of compliance features, the default ComplianceReferenceManager for a federation is the **federator** itself.

### ComplianceReference

This object represents a set of documentation norms and standards, services must comply with. For example: ISO-27001.

A ComplianceReference is composed of mandatory attributes like: the URI of reference document related to the compliance, the title, description version and validity dates of the reference.

Each ComplianceReference is signed by a ComplianceReferenceManager.
This relationship is indicated through:
* the `hasComplianceReferenceManager` attribute of the ComplianceReference
* the `issuer` attribute of the created ComplianceReference VerifiableCredential

**_RULE_** `hasComplianceReferenceManager` equals `issuer` for ComplianceReference VerifiableCredential

### ComplianceCertificationScheme

This object declares a way to certify a Located Service Offering from a Participant has a ComplianceReference.

According to the importance of a ComplianceReference for a Gaia-X ecosystem and the ability to establish controls by humans, different schemes of certification can be allowed.

At the time of this writing, only two types of schemes are possible. `ComplianceCertificationScheme` for a **self-declared** compliance in the case of a provider is allowed to declared itself he has a compliance certification, and `ThirdPartyComplianceCertificationScheme` for a  **3rd party certification** which involves a **compliance assessment body**. Other types of schemes could be added later.

A `ComplianceCertificationScheme` is associated to a list of `ComplianceCriterion` to indicate the granted criteria.

**_RULE_** `issuer` for ComplianceCertificationScheme VerifiableCredential equals `hasComplianceReference.hasComplianceReferenceManager`

### ComplianceAssessmentBody

Is a role assumed by a Participant inside a given Gaia-X ecosystem.

Functionally, a **compliance assessment body** is trusted to assert Service Offerings provided by Providers have been certified for a given compliance in the case of a `ThirdPartyComplianceCertificationScheme`.

### ThirdPartyComplianceCertificationScheme

This object is a type of certification scheme that allows a list of ComplianceAssessmentBody role to claim a service offering provided by a Provider is certified.

A ThirdPartyComplianceCertificationScheme instance is signed by a ComplianceReferenceManager.

**_RULE_** `issuer` for ThirdPartyComplianceCertificationScheme VerifiableCredential equals `hasComplianceReference.hasComplianceReferenceManager`

### ComplianceCertificateClaim

This object claims a located service offering has been certified for a compliance reference . It is created by a Provider to self-declare it has taken a certification.

It associates a ComplianceCertificationScheme with a located service offering.

**_RULE_** for a Self-Assessed claim: `issuer` of VerifiableCredential equals `hasServiceOffering.providedBy`

### ThirdPartyComplianceCertificateClaim

This object claims a located service offering has been certified for a compliance reference, by a CAB. It is created by a Provider to declare it has taken a certification.

**To be fully useful, the referenced CAB must issue a ThirdPartyComplianceCertificateCredential referencing this claim.** See the ThirdPartyComplianceCertificateCredential section below.

It associates a ComplianceCertificationScheme with a located service offering and a compliance assessment body.

**_RULE_** for a ThirdPartyComplianceCertificateClaim claim: `issuer` of VerifiableCredential equals `hasComplianceAssessmentBody`

### ComplianceCertificateCredential

Is a VerifiableCredential self signed by a Provider in the case of self-declaration of a taken compliance reference.

### ThirdPartyComplianceCertificateCredential

Is a VerifiableCredential signed by a ComplianceAssessmentBody in the case of an audit process.

### ComplianceCriterion

Refers to a criterion defined in the Gaia-X Trust Framework or Labelling Criteria.
The list of ComplianceCriterion is created by Gaia-X.

Some criterion can be self-assessed. For that, a SelfAssessedComplianceCriteriaCredential can be created by a Provider.

### ComplianceLabel

Refers to a label defined in the Gaia-X Labelling Framework.
A label has a level and a several ComplianceCriterion.
The list of ComplianceLabel is created by Gaia-X.

According to the list of ComplianceCriterion granted by ComplianceCertificateCredentials, ThirdPartyComplianceCertificateCredential, or SelfAssessedComplianceCriteriaCredential, a located service offering may get a ComplianceLabel.


## Examples

Here are the assumptions for the examples.

Gaia-X Compliance labels:

* Level 1 - criterion C-1 to C-4 must be granted
  * self assessed allowed for C-1 to C-2
  * self declaration of existing certification for C-3 to C-4
* Level 2 - criterion C-1 to C-4 must be granted
  * self declaration of existing certification for C-1
  * compliance issued by an authorized CAB for C-2 to C-4
* Level 3 - criterion C-1 to C-2 must be granted
  * compliance issued by an authorized CAB for C-1 to C-5

This give us the following Criterion to Label matrix:

| Criterion | Label level 1                            | Label level 2                                                              | Label level 3                             |
|-----------|------------------------------------------|----------------------------------------------------------------------------|-------------------------------------------|
| C-1       | SelfAssessedComplianceCriteriaCredential | ComplianceCertificateCredential, ThirdPartyComplianceCertificateCredential | ThirdPartyComplianceCertificateCredential |
| C-2       | SelfAssessedComplianceCriteriaCredential | ComplianceCertificateCredential                                            | ThirdPartyComplianceCertificateCredential |
| C-3       | ComplianceCertificateCredential          | ThirdPartyComplianceCertificateCredential                                  | ThirdPartyComplianceCertificateCredential | ThirdPartyComplianceCertificateCredential
| C-4       | ComplianceCertificateCredential          | ThirdPartyComplianceCertificateCredential                                  | ThirdPartyComplianceCertificateCredential | ThirdPartyComplianceCertificateCredential
| C-5       |                                          |                                                                            | ThirdPartyComplianceCertificateCredential | ThirdPartyComplianceCertificateCredential


To pass a criterion, Compliance references can be used:
* CISPE Data Protection code of conduct
  * can be self declared to grant criterion C-3 to C-4
  * can be certified to grant criterion C-1 to C-4
* ISO 27001
  * can be certified to grant criterion C-5

Here is the resulting Criterion/Labels/Compliance References:

| Criterion | Used for label level | Can be self assessed | CISPE CoC self declared | CISPE CoC certified by a CAB | ISO 27001 certified by a CAB |
|-----------|----------------------|----------------------|-------------------------|------------------------------|------------------------------|
| C-1       | Level 1              | X                    |                         |                              |
|           | Level 2              |                      | X                       | X                            |
|           | Level 3              |                      |                         | X                            |
| C-2       | Level 1              | X                    |                         |                              |
|           | Level 2              |                      |                         | X                            |
|           | Level 3              |                      |                         | X                            |
| C-3       | Level 1              |                      | X                       |                              |
|           | Level 2              |                      |                         | X                            |
|           | Level 3              |                      |                         | X                            |
| C-4       | Level 1              |                      | X                       |                              |
|           | Level 2              |                      |                         | X                            |
|           | Level 3              |                      |                         | X                            |
| C-5       | Level 1              |                      |                         |                              |
|           | Level 2              |                      |                         |                              |
|           | Level 3              |                      |                         |                              | X                            |


### Example of Third Party compliance certification for ISO-27001

Initial setup

* Gaia-X has created the C-1 to C-10 Compliance Criteria VC as well as the Compliance Labels.
* A Participant CompRefManager is considered as a valid ComplianceReferenceManager by the Gaia-X Trust Framework service. Thus Participant CompRefManager has a ComplianceReferenceManager VC signed by https://compliance.gaia-x.eu/
* Participant ProviderA has been onboarded into the Ecosystem. Thus ProviderA has a Provider VC signed by https://compliance.gaia-x.eu/.
* Participant AUDITOR has been onboarded into the Ecosystem. Thus AUDITOR has a ComplianceAssessmentBody VC signed by https://compliance.gaia-x.eu/.
* Participant ProviderA has registered a service offering name IAAS. This service if located in France. Thus a LocatedServiceOffering VC named IAAS-FR has been signed by https://compliance.gaia-x.eu/ .

A new Compliance ISO-27001 must be supported inside the ecosystem:

* Participant CompRefManager signs a ComplianceReference VC. CompRefManager has identified ISO-27001 grants Gaia-X criterion C-5 .
* Participant CompRefManager evaluates the criticity of ISO-27001 compliance. It decides to rely on a network of compliance assessment bodies to trust assertion of certification compliance. ThirdPartyComplianceCertificationScheme is the only way to justify a service is ISO-27001 certified.
* Participant AUDITOR is considered as a valid compliance assessment body for ISO-27001 by the CompRefManager. Thus CompRefManager signs a ThirdPartyComplianceCertificationScheme VC which indicates Participant AUDITOR is allowed to claim ISO-27001 compliance.

Participant ProviderA wants to claim its service IAAS is ISO-27001 compliant:

* ISO-27001 compliance must be assessed. Those steps are out of Gaia-X.
  * ProviderA contact AUDITOR to asks ISO-27001 assessment for its IAAS service
  * AUDITOR runs assessment
* if AUDITOR considers service IAAS is ISO-27001 compliant
  * it creates and signs a ThirdPartyComplianceCertificateClaim IAAS-ISO-27001 indicating the IAAS service offering, AUDITOR and the ThirdPartyComplianceCertificationScheme
  * it creates and signs a ThirdPartyComplianceCertificateCredential referencing the ThirdPartyComplianceCertificateClaim IAAS-ISO-27001. The credential is sent to ProviderA in order to allow him to use the credential later

### Example of Self Declaration of CISPE CoC

Initial setup

* Gaia-X has created the C-1 to C-10 Compliance Criteria VC as well as the Compliance Labels.
* A Participant CompRefManager is considered as a valid ComplianceReferenceManager by the Gaia-X Trust Framework service. Thus Participant CompRefManager has a ComplianceReferenceManager VC signed by <https://compliance.gaia-x.eu/>
* Participant ProviderA has been onboarded into the Ecosystem. Thus ProviderA has a Provider VC signed by <https://compliance.gaia-x.eu/>.
<https://compliance.gaia-x.eu/>.
* Participant ProviderA has registered a service offering name IAAS. This service if located in France. Thus a LocatedServiceOffering VC named IAAS-FR has been signed by <https://compliance.gaia-x.eu/> .

A new CISPE CoC must be supported inside the ecosystem:

* Participant CompRefManager signs a ComplianceReference VC. CompRefManager has identified a self declared CISPE CoC grants Gaia-X criteria C-1 Level 2, C-3 Level 1, and C-4 Level1.
* Participant CompRefManager creates a ComplianceCertificationScheme to allow self declaration for CISPE CoC.

Participant ProviderA wants to self declare its service IAAS is CISPE CoC compliant:

* it creates and signs a ComplianceCertificateClaim IAAS-CISPE indicating the IAAS service offering and the ComplianceCertificationScheme
* it creates and signs a ComplianceCertificateCredential referencing the ComplianceCertificateClaim IAAS-CISPE. This credential is kept by ProviderA for later usage.

### Example of Self Assessment

Initial setup

* Gaia-X has created the C-1 to C-10 Compliance Criteria VC as well as the Compliance Labels.
* Participant ProviderA has been onboarded into the Ecosystem. Thus ProviderA has a Provider VC signed by <https://compliance.gaia-x.eu/>.
<https://compliance.gaia-x.eu/>.
* Participant ProviderA has registered a service offering name IAAS. This service if located in France. Thus a LocatedServiceOffering VC named IAAS-FR has been signed by <https://compliance.gaia-x.eu/> .

Participant ProviderA wants to self assess criterion C-1 and C-2 for its IAAS service:

* it creates and signs a SelfAssessedComplianceCriteriaClaim IAAS indicating the IAAS service offering.
* it creates and signs a SelfAssessedComplianceCriteriaCredential referencing the SelfAssessedComplianceCriteriaClaim IAAS. This credential is kept by ProviderA for later usage.

### Example of a Gaia-X Level 1 label

Initial setup

* [Example of Third Party compliance certification for ISO-27001]()
* [Example of Self Declaration of CISPE CoC]()
* [Example of Self Assessment]()

ProviderA retrieved its Credential for IAAS service:

* ThirdPartyComplianceCertificateCredential ISO-27001
* ComplianceCertificateCredential IAAS-CISPE
* SelfAssessedComplianceCriteriaCredential IAAS

Then it creates a VP with those credentials plus the VC of its located service offering. It requests the **compliance** service with this VP. The **compliance** service returns a ComplianceLabel:
  * Located Service Offering definition
  * the list of granted criteria
  * the list of applicable Gaia-X labels (Level 1 in our case)
