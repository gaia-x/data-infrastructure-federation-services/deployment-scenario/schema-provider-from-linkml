# Type: Ncname




_Prefix part of CURIE_



URI: [xsd:string](http://www.w3.org/2001/XMLSchema#string)

* [base](https://w3id.org/linkml/base): NCName

* [uri](https://w3id.org/linkml/uri): xsd:string

* [repr](https://w3id.org/linkml/repr): str








## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#



