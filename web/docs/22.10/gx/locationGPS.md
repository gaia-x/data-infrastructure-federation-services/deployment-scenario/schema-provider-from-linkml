# Slot: locationGPS


_A list of physical GPS in ISO 6709:2008/Cor 1:2009 format._



URI: [gx:locationGPS](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#locationGPS)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: locationGPS
description: A list of physical GPS in ISO 6709:2008/Cor 1:2009 format.
title: location GPS
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: locationGPS
owner: PhysicalResource
domain_of:
- PhysicalResource
range: string
required: false

```
</details>