# Slot: termsAndConditions

URI: [gx:termsAndConditions](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#termsAndConditions)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[GaiaXTermsAndConditions](GaiaXTermsAndConditions.md) | Gaia-X terms and conditions, each issuer has to aggree |  no  |
[Issuer](Issuer.md) | Each issuer shall issue a GaiaXTermsAndCondition verifiable credential |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information








## LinkML Source

<details>
```yaml
name: termsAndConditions
alias: termsAndConditions
domain_of:
- GaiaXTermsAndConditions
- Issuer
- ServiceOffering
range: string

```
</details>