# Class: EUID



URI: [gx:EUID](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#EUID)




```mermaid
 classDiagram
    class EUID
      RegistrationNumber <|-- EUID
      
      EUID : value
        
      
```





## Inheritance
* [RegistrationNumber](RegistrationNumber.md)
    * **EUID**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [value](value.md) | 1..1 <br/> [String](String.md) | The European Unique Identifier (EUID) for business located in the European Ec... | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:EUID |
| native | gx:EUID |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: EUID
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: The European Unique Identifier (EUID) for business located in the
      European Economic Area, Iceland, Liechtenstein or Norway and registered in the
      Business Registers Interconnection System (BRIS). This number can be found via
      the EU Business registers portal.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    identifier: true
    range: string
    required: true

```
</details>

### Induced

<details>
```yaml
name: EUID
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
is_a: RegistrationNumber
attributes:
  value:
    name: value
    description: The European Unique Identifier (EUID) for business located in the
      European Economic Area, Iceland, Liechtenstein or Norway and registered in the
      Business Registers Interconnection System (BRIS). This number can be found via
      the EU Business registers portal.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    identifier: true
    alias: value
    owner: EUID
    domain_of:
    - LocalRegistrationNumber
    - VatID
    - LeiCode
    - EORI
    - EUID
    range: string
    required: true

```
</details>