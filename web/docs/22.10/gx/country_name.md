# Slot: country_name


_Physical location of head quarter in ISO 3166-1 alpha2, alpha-3 or numeric format._



URI: [gx:country_name](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#country_name)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Address](Address.md) |  |  no  |
[LegalAddress](LegalAddress.md) |  |  no  |
[HeadquarterAddress](HeadquarterAddress.md) |  |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: country-name
description: Physical location of head quarter in ISO 3166-1 alpha2, alpha-3 or numeric
  format.
title: country name
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: country_name
owner: Address
domain_of:
- Address
range: string
required: true

```
</details>