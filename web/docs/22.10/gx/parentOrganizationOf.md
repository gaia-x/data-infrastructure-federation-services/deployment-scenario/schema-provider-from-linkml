# Slot: parentOrganizationOf


_A list of direct participant that this entity is a subOrganization of, if any._



URI: [gx:parentOrganizationOf](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#parentOrganizationOf)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Relationship](Relationship.md) | A relationship between two organisations |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: parentOrganizationOf
description: A list of direct participant that this entity is a subOrganization of,
  if any.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: parentOrganizationOf
owner: Relationship
domain_of:
- Relationship
range: LegalPerson

```
</details>