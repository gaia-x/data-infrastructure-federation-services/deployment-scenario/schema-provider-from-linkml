# Class: GaiaXEntity


_Root class for Gaia-X entities._




* __NOTE__: this is an abstract class and should not be instantiated directly


URI: [gx:GaiaXEntity](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#GaiaXEntity)




```mermaid
 classDiagram
    class GaiaXEntity
      GaiaXEntity <|-- Issuer
      GaiaXEntity <|-- Participant
      GaiaXEntity <|-- Resource
      GaiaXEntity <|-- ServiceOffering
      
      
```





## Inheritance
* **GaiaXEntity**
    * [Issuer](Issuer.md)
    * [Participant](Participant.md)
    * [Resource](Resource.md)
    * [ServiceOffering](ServiceOffering.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |









## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:GaiaXEntity |
| native | gx:GaiaXEntity |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: GaiaXEntity
description: Root class for Gaia-X entities.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
abstract: true

```
</details>

### Induced

<details>
```yaml
name: GaiaXEntity
description: Root class for Gaia-X entities.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
abstract: true

```
</details>