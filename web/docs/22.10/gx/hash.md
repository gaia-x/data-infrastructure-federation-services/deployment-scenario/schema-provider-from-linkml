# Slot: hash


_sha256 hash of the document._



URI: [gx:hash](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#hash)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[TermsAndConditions](TermsAndConditions.md) | Terms and Conditions applying to a service offering |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: hash
description: sha256 hash of the document.
title: hash
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
multivalued: false
alias: hash
owner: TermsAndConditions
domain_of:
- TermsAndConditions
range: string
required: true

```
</details>