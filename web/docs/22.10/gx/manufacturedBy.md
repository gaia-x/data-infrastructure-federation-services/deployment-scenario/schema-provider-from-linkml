# Slot: manufacturedBy


_Participant manufacturing the resource._



URI: [gx:manufacturedBy](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#manufacturedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: manufacturedBy
description: Participant manufacturing the resource.
title: manufactured by
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: manufacturedBy
owner: PhysicalResource
domain_of:
- PhysicalResource
range: LegalPerson
required: false

```
</details>