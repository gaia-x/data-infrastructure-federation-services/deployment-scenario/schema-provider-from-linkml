# Class: PhysicalResource


_A Physical resource is, but not limited to, a datacenter, a bare-metal service, a warehouse, a plant._





URI: [gx:PhysicalResource](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#PhysicalResource)




```mermaid
 classDiagram
    class PhysicalResource
      Resource <|-- PhysicalResource
      
      PhysicalResource : aggregationOf
        
          PhysicalResource --|> Resource : aggregationOf
        
      PhysicalResource : description
        
      PhysicalResource : locationAddress
        
          PhysicalResource --|> Address : locationAddress
        
      PhysicalResource : locationGPS
        
      PhysicalResource : maintainedBy
        
          PhysicalResource --|> LegalPerson : maintainedBy
        
      PhysicalResource : manufacturedBy
        
          PhysicalResource --|> LegalPerson : manufacturedBy
        
      PhysicalResource : name
        
      PhysicalResource : ownedBy
        
          PhysicalResource --|> LegalPerson : ownedBy
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * [Resource](Resource.md)
        * **PhysicalResource**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [maintainedBy](maintainedBy.md) | 1..1 <br/> [LegalPerson](LegalPerson.md) | Participant maintaining the resource in operational condition and thus have p... | direct |
| [ownedBy](ownedBy.md) | 0..1 <br/> [LegalPerson](LegalPerson.md) | Participant owning the resource | direct |
| [manufacturedBy](manufacturedBy.md) | 0..1 <br/> [LegalPerson](LegalPerson.md) | Participant manufacturing the resource | direct |
| [locationGPS](locationGPS.md) | 0..1 <br/> [String](String.md) | A list of physical GPS in ISO 6709:2008/Cor 1:2009 format | direct |
| [locationAddress](locationAddress.md) | 1..1 <br/> [Address](Address.md) | A vcard:Address object that contains the physical location in ISO 3166-1 alph... | direct |
| [name](name.md) | 0..1 <br/> [String](String.md) | A human readable name of the data resource | [Resource](Resource.md) |
| [description](description.md) | 0..1 <br/> [String](String.md) | A free text description of the data resource | [Resource](Resource.md) |
| [aggregationOf](aggregationOf.md) | 0..* <br/> [Resource](Resource.md) | Resources related to the resource and that can exist independently of it | [Resource](Resource.md) |









## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:PhysicalResource |
| native | gx:PhysicalResource |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: PhysicalResource
description: A Physical resource is, but not limited to, a datacenter, a bare-metal
  service, a warehouse, a plant.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
is_a: Resource
attributes:
  maintainedBy:
    name: maintainedBy
    description: Participant maintaining the resource in operational condition and
      thus have physical access to it.
    title: maintained by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: LegalPerson
    required: true
  ownedBy:
    name: ownedBy
    description: Participant owning the resource.
    title: owned by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: LegalPerson
    required: false
  manufacturedBy:
    name: manufacturedBy
    description: Participant manufacturing the resource.
    title: manufactured by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: LegalPerson
    required: false
  locationGPS:
    name: locationGPS
    description: A list of physical GPS in ISO 6709:2008/Cor 1:2009 format.
    title: location GPS
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: false
  locationAddress:
    name: locationAddress
    description: A vcard:Address object that contains the physical location in ISO
      3166-1 alpha2, alpha-3 or numeric format.
    title: location Address
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: Address
    required: true
class_uri: gx:PhysicalResource

```
</details>

### Induced

<details>
```yaml
name: PhysicalResource
description: A Physical resource is, but not limited to, a datacenter, a bare-metal
  service, a warehouse, a plant.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
is_a: Resource
attributes:
  maintainedBy:
    name: maintainedBy
    description: Participant maintaining the resource in operational condition and
      thus have physical access to it.
    title: maintained by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: maintainedBy
    owner: PhysicalResource
    domain_of:
    - PhysicalResource
    range: LegalPerson
    required: true
  ownedBy:
    name: ownedBy
    description: Participant owning the resource.
    title: owned by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: ownedBy
    owner: PhysicalResource
    domain_of:
    - PhysicalResource
    range: LegalPerson
    required: false
  manufacturedBy:
    name: manufacturedBy
    description: Participant manufacturing the resource.
    title: manufactured by
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: manufacturedBy
    owner: PhysicalResource
    domain_of:
    - PhysicalResource
    range: LegalPerson
    required: false
  locationGPS:
    name: locationGPS
    description: A list of physical GPS in ISO 6709:2008/Cor 1:2009 format.
    title: location GPS
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: locationGPS
    owner: PhysicalResource
    domain_of:
    - PhysicalResource
    range: string
    required: false
  locationAddress:
    name: locationAddress
    description: A vcard:Address object that contains the physical location in ISO
      3166-1 alpha2, alpha-3 or numeric format.
    title: location Address
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: locationAddress
    owner: PhysicalResource
    domain_of:
    - PhysicalResource
    range: Address
    required: true
  name:
    name: name
    description: A human readable name of the data resource.
    title: name
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: name
    owner: PhysicalResource
    domain_of:
    - Resource
    - ServiceOffering
    range: string
    required: false
  description:
    name: description
    description: A free text description of the data resource.
    title: description
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: description
    owner: PhysicalResource
    domain_of:
    - Resource
    - ServiceOffering
    range: string
    required: false
  aggregationOf:
    name: aggregationOf
    description: Resources related to the resource and that can exist independently
      of it.
    title: aggregation of
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    multivalued: true
    alias: aggregationOf
    owner: PhysicalResource
    domain_of:
    - Resource
    - ServiceOffering
    range: Resource
    required: false
class_uri: gx:PhysicalResource

```
</details>