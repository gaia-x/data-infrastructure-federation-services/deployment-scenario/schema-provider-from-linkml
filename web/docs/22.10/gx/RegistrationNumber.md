# Slot: registrationNumber


_Country's registration number, which identifies one specific entity. Valid formats are local, EUID, EORI, vatID, leiCode._



URI: [gx:registrationNumber](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#registrationNumber)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LegalPerson](LegalPerson.md) | An legal person, who is uniquely identifier by it's registration number |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: registrationNumber
description: Country's registration number, which identifies one specific entity.
  Valid formats are local, EUID, EORI, vatID, leiCode.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
multivalued: true
alias: registrationNumber
owner: LegalPerson
domain_of:
- LegalPerson
range: string
required: true
inlined: true
inlined_as_list: true
any_of:
- range: RegistrationNumber
- range: LocalRegistrationNumber
- range: VatID
- range: LeiCode
- range: EORI
- range: EUID

```
</details>