# Slot: provisionType


_Provision type of the service_



URI: [gx:provisionType](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#provisionType)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: provisionType
description: Provision type of the service
title: provision type
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: provisionType
owner: ServiceOffering
domain_of:
- ServiceOffering
range: string
required: false

```
</details>