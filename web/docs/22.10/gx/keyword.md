# Slot: keyword


_Keywords that describe / tag the service._



URI: [gx:keyword](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#keyword)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: keyword
description: Keywords that describe / tag the service.
title: keyword
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
multivalued: true
alias: keyword
owner: ServiceOffering
domain_of:
- ServiceOffering
range: string
required: false

```
</details>