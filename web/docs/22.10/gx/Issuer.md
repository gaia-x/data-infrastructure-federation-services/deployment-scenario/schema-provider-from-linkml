# Class: Issuer


_Each issuer shall issue a GaiaXTermsAndCondition verifiable credential._





URI: [gx:Issuer](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#Issuer)




```mermaid
 classDiagram
    class Issuer
      GaiaXEntity <|-- Issuer
      
      Issuer : termsAndConditions
        
          Issuer --|> GaiaXTermsAndConditions : termsAndConditions
        
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **Issuer**



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [termsAndConditions](termsAndConditions.md) | 1..1 <br/> [GaiaXTermsAndConditions](GaiaXTermsAndConditions.md) | Gaia-X terms and conditions signed by participant | direct |









## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:Issuer |
| native | gx:Issuer |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Issuer
description: Each issuer shall issue a GaiaXTermsAndCondition verifiable credential.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
is_a: GaiaXEntity
attributes:
  termsAndConditions:
    name: termsAndConditions
    description: Gaia-X terms and conditions signed by participant.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    range: GaiaXTermsAndConditions
    required: true

```
</details>

### Induced

<details>
```yaml
name: Issuer
description: Each issuer shall issue a GaiaXTermsAndCondition verifiable credential.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
is_a: GaiaXEntity
attributes:
  termsAndConditions:
    name: termsAndConditions
    description: Gaia-X terms and conditions signed by participant.
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    alias: termsAndConditions
    owner: Issuer
    domain_of:
    - GaiaXTermsAndConditions
    - Issuer
    - ServiceOffering
    range: GaiaXTermsAndConditions
    required: true

```
</details>