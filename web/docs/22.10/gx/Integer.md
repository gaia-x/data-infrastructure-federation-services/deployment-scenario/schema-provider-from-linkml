# Type: Integer




_An integer_



URI: [xsd:integer](http://www.w3.org/2001/XMLSchema#integer)

* [base](https://w3id.org/linkml/base): int

* [uri](https://w3id.org/linkml/uri): xsd:integer









## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#



