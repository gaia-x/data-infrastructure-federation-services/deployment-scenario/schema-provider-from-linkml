# Slot: value

URI: [gx:value](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#value)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[LocalRegistrationNumber](LocalRegistrationNumber.md) |  |  no  |
[VatID](VatID.md) |  |  no  |
[LeiCode](LeiCode.md) |  |  no  |
[EORI](EORI.md) |  |  no  |
[EUID](EUID.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information








## LinkML Source

<details>
```yaml
name: value
alias: value
domain_of:
- LocalRegistrationNumber
- VatID
- LeiCode
- EORI
- EUID
range: string

```
</details>