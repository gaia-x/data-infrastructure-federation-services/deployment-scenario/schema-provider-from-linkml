# Enum: RequestTypeMeans



URI: [RequestTypeMeans](RequestTypeMeans)

## Permissible Values

| Value | Meaning | Description |
| --- | --- | --- |
| API | None |  |
| email | None |  |
| webform | None |  |
| unregisteredLetter | None |  |
| registeredLetter | None |  |
| supportCenter | None |  |




## Slots

| Name | Description |
| ---  | --- |
| [requestType](requestType.md) | The mean to request data retrieval: API, email, webform, unregisteredLetter, ... |






## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: RequestTypeMeans
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
permissible_values:
  API:
    text: API
  email:
    text: email
  webform:
    text: webform
  unregisteredLetter:
    text: unregisteredLetter
  registeredLetter:
    text: registeredLetter
  supportCenter:
    text: supportCenter

```
</details>
