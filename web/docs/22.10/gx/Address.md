# Class: Address



URI: [gx:Address](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#Address)




```mermaid
 classDiagram
    class Address
      Address <|-- LegalAddress
      Address <|-- HeadquarterAddress
      
      Address : country_name
        
      Address : gps
        
      Address : locality
        
      Address : postal_code
        
      Address : street_address
        
      
```





## Inheritance
* **Address**
    * [LegalAddress](LegalAddress.md)
    * [HeadquarterAddress](HeadquarterAddress.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [country_name](country_name.md) | 1..1 <br/> [String](String.md) | Physical location of head quarter in ISO 3166-1 alpha2, alpha-3 or numeric fo... | direct |
| [gps](gps.md) | 0..1 <br/> [String](String.md) | GPS in ISO 6709:2008/Cor 1:2009 format | direct |
| [street_address](street_address.md) | 0..1 <br/> [String](String.md) | The v:street-address property specifies the street address of a postal addres... | direct |
| [postal_code](postal_code.md) | 0..1 <br/> [String](String.md) | String of a street-address | direct |
| [locality](locality.md) | 0..1 <br/> [String](String.md) | The v:locality property specifies the locality (e | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [PhysicalResource](PhysicalResource.md) | [locationAddress](locationAddress.md) | range | [Address](Address.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:Address |
| native | gx:Address |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Address
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
attributes:
  country-name:
    name: country-name
    description: Physical location of head quarter in ISO 3166-1 alpha2, alpha-3 or
      numeric format.
    title: country name
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: true
  gps:
    name: gps
    description: GPS in ISO 6709:2008/Cor 1:2009 format.
    title: gps
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: false
  street-address:
    name: street-address
    description: The v:street-address property specifies the street address of a postal
      address.
    title: street address
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: false
  postal-code:
    name: postal-code
    description: String of a street-address.
    title: postal code
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: false
  locality:
    name: locality
    description: The v:locality property specifies the locality (e.g., city) of a
      postal address.
    title: locality
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    range: string
    required: false
class_uri: gx:Address

```
</details>

### Induced

<details>
```yaml
name: Address
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
attributes:
  country-name:
    name: country-name
    description: Physical location of head quarter in ISO 3166-1 alpha2, alpha-3 or
      numeric format.
    title: country name
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: country_name
    owner: Address
    domain_of:
    - Address
    range: string
    required: true
  gps:
    name: gps
    description: GPS in ISO 6709:2008/Cor 1:2009 format.
    title: gps
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: gps
    owner: Address
    domain_of:
    - Address
    range: string
    required: false
  street-address:
    name: street-address
    description: The v:street-address property specifies the street address of a postal
      address.
    title: street address
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: street_address
    owner: Address
    domain_of:
    - Address
    range: string
    required: false
  postal-code:
    name: postal-code
    description: String of a street-address.
    title: postal code
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: postal_code
    owner: Address
    domain_of:
    - Address
    range: string
    required: false
  locality:
    name: locality
    description: The v:locality property specifies the locality (e.g., city) of a
      postal address.
    title: locality
    from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
    rank: 1000
    alias: locality
    owner: Address
    domain_of:
    - Address
    range: string
    required: false
class_uri: gx:Address

```
</details>