# Slot: description

URI: [gx:description](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#description)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Catalogue](Catalogue.md) | A catalogue service is a subclass of serviceOffering used to browse, search, ... |  no  |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |
[Resource](Resource.md) | A resource that may be aggregated in a Service Offering or exist independentl... |  no  |
[ServiceOffering](ServiceOffering.md) | A description of a digital service available for order |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information








## LinkML Source

<details>
```yaml
name: description
alias: description
domain_of:
- Resource
- ServiceOffering
range: string

```
</details>