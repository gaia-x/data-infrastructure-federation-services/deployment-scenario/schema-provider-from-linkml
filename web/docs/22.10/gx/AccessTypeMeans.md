# Enum: AccessTypeMeans



URI: [AccessTypeMeans](AccessTypeMeans)

## Permissible Values

| Value | Meaning | Description |
| --- | --- | --- |
| digital | None |  |
| physical | None |  |




## Slots

| Name | Description |
| ---  | --- |
| [accessType](accessType.md) | Type of data support: digital, physical |






## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: AccessTypeMeans
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
permissible_values:
  digital:
    text: digital
  physical:
    text: physical

```
</details>
