# Slot: endpointURL


_The URL of the endpoint where it can be accessed._



URI: [gx:endpointURL](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#endpointURL)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Endpoint](Endpoint.md) | An endpoint is a mean to access and interact with a service or a resource |  no  |







## Properties

* Range: [Uri](Uri.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: endpointURL
description: The URL of the endpoint where it can be accessed.
title: endpoint URL
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
multivalued: true
alias: endpointURL
owner: Endpoint
domain_of:
- Endpoint
range: uri
required: false

```
</details>