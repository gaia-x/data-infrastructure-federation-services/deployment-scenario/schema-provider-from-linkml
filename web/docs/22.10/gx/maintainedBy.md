# Slot: maintainedBy


_Participant maintaining the resource in operational condition and thus have physical access to it._



URI: [gx:maintainedBy](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#maintainedBy)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: maintainedBy
description: Participant maintaining the resource in operational condition and thus
  have physical access to it.
title: maintained by
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: maintainedBy
owner: PhysicalResource
domain_of:
- PhysicalResource
range: LegalPerson
required: true

```
</details>