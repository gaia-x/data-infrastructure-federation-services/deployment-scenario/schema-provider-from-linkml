# Slot: locationAddress


_A vcard:Address object that contains the physical location in ISO 3166-1 alpha2, alpha-3 or numeric format._



URI: [gx:locationAddress](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#locationAddress)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[PhysicalResource](PhysicalResource.md) | A Physical resource is, but not limited to, a datacenter, a bare-metal servic... |  no  |







## Properties

* Range: [Address](Address.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: locationAddress
description: A vcard:Address object that contains the physical location in ISO 3166-1
  alpha2, alpha-3 or numeric format.
title: location Address
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: locationAddress
owner: PhysicalResource
domain_of:
- PhysicalResource
range: Address
required: true

```
</details>