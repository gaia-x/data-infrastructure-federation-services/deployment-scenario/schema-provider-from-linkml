# Slot: subOrganisationOf


_A direct participant with a legal mandate on this entity, e.g., as a subsidiary._



URI: [gx:subOrganisationOf](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#subOrganisationOf)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Relationship](Relationship.md) | A relationship between two organisations |  no  |







## Properties

* Range: [LegalPerson](LegalPerson.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: subOrganisationOf
description: A direct participant with a legal mandate on this entity, e.g., as a
  subsidiary.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: subOrganisationOf
owner: Relationship
domain_of:
- Relationship
range: LegalPerson

```
</details>