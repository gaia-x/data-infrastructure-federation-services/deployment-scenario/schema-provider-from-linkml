# Slot: standardConformity


_Provides information about applied standards._



URI: [gx:standardConformity](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#standardConformity)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Endpoint](Endpoint.md) | An endpoint is a mean to access and interact with a service or a resource |  no  |







## Properties

* Range: [StandardConformity](StandardConformity.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: standardConformity
description: Provides information about applied standards.
title: standard conformity
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
multivalued: true
alias: standardConformity
owner: Endpoint
domain_of:
- Endpoint
range: StandardConformity
required: false

```
</details>