# Slot: formatType


_Type of Media Types (formerly known as MIME types) as defined by the IANA._



URI: [gx:formatType](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#formatType)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[DataAccountExport](DataAccountExport.md) | List of methods to export data from your account out of the service |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: formatType
description: Type of Media Types (formerly known as MIME types) as defined by the
  IANA.
title: format type
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: formatType
owner: DataAccountExport
domain_of:
- DataAccountExport
range: string
required: true

```
</details>