# Class: Participant


_An legal or natural person that is onboarded to Gaia-X and offers, consumes services or operates resources._




* __NOTE__: this is an abstract class and should not be instantiated directly


URI: [gx:Participant](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#Participant)




```mermaid
 classDiagram
    class Participant
      GaiaXEntity <|-- Participant
      

      Participant <|-- LegalPerson
      
      
      
```





## Inheritance
* [GaiaXEntity](GaiaXEntity.md)
    * **Participant**
        * [LegalPerson](LegalPerson.md)



## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |









## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#





## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | gx:Participant |
| native | gx:Participant |





## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: Participant
description: An legal or natural person that is onboarded to Gaia-X and offers, consumes
  services or operates resources.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
is_a: GaiaXEntity
abstract: true

```
</details>

### Induced

<details>
```yaml
name: Participant
description: An legal or natural person that is onboarded to Gaia-X and offers, consumes
  services or operates resources.
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
is_a: GaiaXEntity
abstract: true

```
</details>