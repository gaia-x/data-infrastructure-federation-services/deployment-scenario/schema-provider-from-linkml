# Slot: postal_code


_String of a street-address._



URI: [gx:postal_code](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#postal_code)



<!-- no inheritance hierarchy -->




## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
[Address](Address.md) |  |  no  |
[LegalAddress](LegalAddress.md) |  |  no  |
[HeadquarterAddress](HeadquarterAddress.md) |  |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#




## LinkML Source

<details>
```yaml
name: postal-code
description: String of a street-address.
title: postal code
from_schema: https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#
rank: 1000
alias: postal_code
owner: Address
domain_of:
- Address
range: string
required: false

```
</details>