# Ontologies

## Available ontologies

| Ontology                                                  | Description                                     |
|-----------------------------------------------------------|-------------------------------------------------|
| [22.10 gx](22.10/gx/index.md)                             | Ontology gx (Trust-Framework) for 22.10 version |
| [23.xx abc-conformity](23.xx/abc-conformity/index.md)     | Ontology abc-conformity for 23.xx version       |
| [23.xx abc-data](23.xx/abc-data/index.md)                 | Ontology abc-data for 23.xx version             |
| [23.xx aster-conformity](23.xx/aster-conformity/index.md) | Ontology aster-conformity for 23.xx version     |
| [23.xx aster-data](23.xx/aster-data/index.md)             | Ontology aster-data for 23.xx version           |
| [23.xx casco-conformity](23.xx/casco-conformity/index.md) | Ontology casco-conformity for 23.xx version     |
