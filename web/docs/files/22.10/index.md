# 22.10 version

## List of SHACL files

| Name                                                 | Description                             |
|------------------------------------------------------|-----------------------------------------|
| [Trust Framework SHACL](shacl/gx-ontology.shacl.ttl) | Shapes of the Trust Framework ontology. |

## List of context files

| Name                                                        | Description                         |
|-------------------------------------------------------------|-------------------------------------|
| [Trust Framework context](context/gx-ontology.context.json) | Context for data exchange ontology. |

