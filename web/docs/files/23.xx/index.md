# 23.xx version

## List of SHACL files

| Name                                                                                | Description                                 |
|-------------------------------------------------------------------------------------|---------------------------------------------|
| [DataExchange SHACL for abc](shacl/abc-data/data-ontology.shacl.ttl)                | Shapes of the abc-data exchange ontology.   |
| [Conformity SHACL for abc](shacl/abc-conformity/conformity-ontology.shacl.ttl)      | Shapes of the abc-conformity ontology.      |
| [DataExchange SHACL for aster](shacl/aster-data/data-ontology.shacl.ttl)            | Shapes of the aster-data exchange ontology. |
| [Conformity SHACL for aster](shacl/aster-conformity/conformity-ontology.shacl.ttl)  | Shapes of the aster-conformity ontology.    |
| [CASCO SHACL for aster](shacl/casco-conformity/casco-conformity-ontology.shacl.ttl) | Shapes of the casco-conformity ontology.    |

## List of context files

| Name                                                                                            | Description                               |
|-------------------------------------------------------------------------------------------------|-------------------------------------------|
| [DataExchange context for abc](context/abc-data/data-ontology.context.json)                     | Context for abc-data exchange ontology.   |
| [Conformity context for abc](context/abc-conformity/conformity-ontology.context.json)           | Context for abc-conformity ontology.      |
| [DataExchange context for aster](context/aster-data/data-ontology.context.json)                 | Context for aster-data exchange ontology. |
| [Conformity context for aster](context/aster-conformity/conformity-ontology.context.json)       | Context for aster-conformity ontology.    |
| [Conformity context for CASCO](context/casco-conformity/casco-conformity-ontology.context.json) | Context for casco-conformity ontology.    |

