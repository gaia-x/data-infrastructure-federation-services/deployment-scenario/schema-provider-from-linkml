##########
# Builders
##########
FROM python:3.10-slim as build

ENV PIP_DEFAULT_TIMEOUT=100 \
    # Allow statements and log messages to immediately appear
    PYTHONUNBUFFERED=1 \
    # disable a pip version check to reduce run-time & log-spam
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    # cache is useless in docker image, so disable to reduce image size
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.3.2

WORKDIR /app

COPY pyproject.toml poetry.lock ./

RUN pip install --no-cache-dir "poetry==$POETRY_VERSION" \
    && poetry install --no-root --no-ansi --no-interaction \
    && poetry export -f requirements.txt --output requirements.txt --without-hashes

### HTML Generation
FROM python:3.10.11-slim-bullseye as generate-html

WORKDIR /app

COPY --from=build /app/requirements.txt .

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./web web

WORKDIR /app/web

RUN mkdocs build \
  && echo "Web site documentation Ok"

### Final stage
FROM nginx:1.25.1
COPY --from=generate-html /app/web/site /usr/share/nginx/html

