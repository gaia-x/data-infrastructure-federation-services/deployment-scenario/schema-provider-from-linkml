# Scénario de la démo

## Génération des différents output

La commande suivante va permettre

- de remplacer la balise $BASE_URL$ par une url w3id.org
- de genérer les json-schémas
- de générer les fichiers de validation SHACL dans un format turtle
- de générer un context permettant de lier chaque term à un IRI

```shell
make generate-trust-framework
```

Pour ma tambouille, j'ai besoin de générer les modèles en langage Python.

```shell
make generate-python-class
```

C'est en utilisant ces modèles que j'ai pu générer mes différents json object.

## Démo utilisation des SHACL

Je dispose de 2 fichiers :

- data-participant.jsonld : contenu correspond aux données obligatoires d'un participant
- data-participant-extended.jsonld : étend le fichier précédent en ajoutant un context et des données non prévues dans
  SHACL participant

### Valider le data-participant avec SHACL participant

```shell
cd demo
pyshacl -s participant.shacl.ttl -df json-ld -f human data-participant.jsonld
# tout est valide
```

### Valider le data-participant-extended avec SHACL participant

```shell
pyshacl -s participant.shacl.ttl -df json-ld -f human data-participant-extended.jsonld
# Aïe ça ne passe pas car le SHACL est "closed" aux extensions
```

Linkml génère par défaut des SHACL "closed". Je n'ai pas trouvé comment configurer ça, ça veut sans doute dire faire son
propre générateur pour palier à ça.

### On rend le SHACL étendable

```turtle

@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix gx: <https://w3id.org/gaia-x/> .

gx:LegalParticipant
    a         sh:NodeShape ;
    sh:closed false .
```

Et maintenant la validation peut se faire.

```shell
pyshacl -s participant-extendable.shacl.ttl -df json-ld -f human data-participant-extended.jsonld
# C'est ok
```

## Remarque(s)

- ~~J'ai fait sauter les Range car ça me générait des sh:Class dans les SHACL. Et ça ne je ne sais pas comment avoir de
  classe en json-ld. D'ailleurs côté service-charac il n'y pas de sh:Class dans les SHACL.~~
- Par contre le dump de Json depuis classe Python indique un ```@type``` seulement pour l'objet root
- LinkML est bien sûr capable de générer bien d'autres choses.
- Linkml génère par défaut des SHACL "closed". Je n'ai pas trouvé comment configurer ça, ça veut sans doute dire faire
  son propre générateur pour palier à ça.

### Explication

Is there an ELI5 analogy between those fields and OOP ?
- Complex question for an ELI5 😃 I'll try my best:
- sh:targetClass = The class you speak about (e.g., gx:LegalParticipant)
- sh:path = The property you want to validate (e.g., gx:legalPostalAddress)
- sh:class = The class you expect for that property (e.g., schema:PostalAddress)
- sh:node = Refer to nested constraints (e.g., "gx:legalPostalAddress sh:node gx:MyLegalAddressShape", and that shape holds constraints about the format of the address).


## Liens

- [Exemple de description linkml avec personinfo](https://github.com/linkml/linkml/blob/main/examples/PersonSchema/personinfo.yaml)
- Exemple de contexte : https://json-ld.org/contexts/person.jsonld
- [Validateur de schéma](https://validator.schema.org/)
- http://blog.sparna.fr/2022/07/20/clean-json-ld-from-rdf-using-framing/
- http://blog.sparna.fr/2017/01/02/shacl-rdf-shapes-constraint-language-enfin-la-possibilite-de-valider-des-donnees-rdf/
- https://shacl-play.sparna.fr/play/
- https://shacl-playground.zazuko.com/
- https://github.com/RDFLib/pySHACL
- https://perso.liris.cnrs.fr/pierre-antoine.champin/2020/json-ld-tuto/
